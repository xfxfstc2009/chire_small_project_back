USE [master]
GO
/****** Object:  Database [Chire]    Script Date: 2020/1/18 15:17:18 ******/
CREATE DATABASE [Chire]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Chire', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Chire.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Chire_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Chire_log.ldf' , SIZE = 10176KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Chire] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Chire].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Chire] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Chire] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Chire] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Chire] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Chire] SET ARITHABORT OFF 
GO
ALTER DATABASE [Chire] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Chire] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Chire] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Chire] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Chire] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Chire] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Chire] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Chire] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Chire] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Chire] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Chire] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Chire] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Chire] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Chire] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Chire] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Chire] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Chire] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Chire] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Chire] SET RECOVERY FULL 
GO
ALTER DATABASE [Chire] SET  MULTI_USER 
GO
ALTER DATABASE [Chire] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Chire] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Chire] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Chire] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Chire', N'ON'
GO
USE [Chire]
GO
/****** Object:  User [ppw2017]    Script Date: 2020/1/18 15:17:18 ******/
CREATE USER [ppw2017] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [peiyefeng]    Script Date: 2020/1/18 15:17:18 ******/
CREATE USER [peiyefeng] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NT AUTHORITY\SYSTEM]    Script Date: 2020/1/18 15:17:18 ******/
CREATE USER [NT AUTHORITY\SYSTEM] FOR LOGIN [NT AUTHORITY\SYSTEM] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[activity]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[activity](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NULL,
	[des] [nvarchar](max) NULL,
	[datetime] [datetime] NULL,
	[link_id] [nvarchar](50) NULL,
 CONSTRAINT [PK_activity] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[activity_sub]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[activity_sub](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[img] [nvarchar](500) NULL,
	[link_id] [nvarchar](50) NULL,
 CONSTRAINT [PK_activity_sub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_admin] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[balance]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[balance](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[open_id] [nvarchar](50) NULL,
	[balance] [float] NULL,
 CONSTRAINT [PK_balance] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[banner]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[banner](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[direct] [nvarchar](50) NULL,
	[img] [nvarchar](1000) NULL,
	[time] [nvarchar](50) NULL,
	[invalid] [int] NULL,
 CONSTRAINT [PK_banner] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[class]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[class](
	[id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[teacher] [nvarchar](150) NULL,
	[marks] [nvarchar](1000) NULL,
	[status] [nvarchar](50) NULL,
	[classtime] [int] NULL,
	[outclasstime] [int] NULL,
	[uploads] [nvarchar](1000) NULL,
	[week] [nvarchar](50) NULL,
	[begintime] [nvarchar](50) NULL,
	[endtime] [nvarchar](50) NULL,
	[datetime] [datetime] NULL,
	[deletestatus] [int] NULL,
	[ablum] [nvarchar](350) NULL,
	[subject] [nvarchar](50) NULL,
 CONSTRAINT [PK_class] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[class_time]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[class_time](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[week] [nvarchar](50) NULL,
	[begintime] [nvarchar](50) NULL,
	[endtime] [nvarchar](50) NULL,
	[create_time] [datetime] NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_class_time] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[company_shenhe]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[company_shenhe](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[appName] [nvarchar](100) NULL,
	[isShenhe] [int] NULL,
	[appVersion] [nvarchar](100) NULL,
	[lungch_img] [nvarchar](max) NULL,
	[lungch_content] [nvarchar](100) NULL,
	[login_delegate] [nvarchar](150) NULL,
	[show_type] [int] NULL,
	[time] [datetime] NULL,
 CONSTRAINT [PK_company_shenhe] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[contact]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[link] [nvarchar](50) NULL,
	[title] [nvarchar](50) NULL,
	[des] [nvarchar](max) NULL,
	[createtime] [datetime] NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[delegate_info]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[delegate_info](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NULL,
	[info] [nvarchar](150) NULL,
	[url] [nvarchar](1520) NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_delegate_info] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[download]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[download](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NULL,
	[info] [nvarchar](150) NULL,
	[url] [nvarchar](1520) NULL,
	[datetime] [datetime] NULL,
	[ablum] [nvarchar](1520) NULL,
 CONSTRAINT [PK_download] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[event_auto_insert]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[event_auto_insert](
	[id] [nvarchar](50) NULL,
	[beginWeekTime] [int] NULL,
	[endWeekTime] [int] NULL,
	[datetime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[event_calendar]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[event_calendar](
	[smart] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[id] [nvarchar](50) NOT NULL,
	[title] [nvarchar](150) NULL,
	[date] [datetime] NULL,
	[time] [datetime] NULL,
	[dateInterval] [int] NULL,
	[timeInterval] [int] NULL,
	[has_del] [int] NULL,
	[class_id] [nvarchar](50) NULL,
	[hasPush] [int] NULL,
	[type] [int] NULL,
 CONSTRAINT [PK_event_calendar] PRIMARY KEY CLUSTERED 
(
	[smart] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[event_calendar_user]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[event_calendar_user](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[event_id] [nvarchar](50) NULL,
	[userId] [nvarchar](50) NULL,
 CONSTRAINT [PK_event_calendar_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[exam]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exam](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[student_id] [nvarchar](50) NULL,
	[exam_id] [nvarchar](50) NULL,
	[score] [nvarchar](50) NULL,
	[marks] [nvarchar](150) NULL,
	[datetime] [datetime] NULL,
	[linkId] [nvarchar](150) NULL,
 CONSTRAINT [PK_exam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[exam_file_list]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exam_file_list](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[url] [nvarchar](1000) NULL,
	[datetime] [datetime] NULL,
	[file_id] [nvarchar](50) NULL,
 CONSTRAINT [PK_exam_file_list] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[exam_sub]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exam_sub](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[fileUrl] [nvarchar](350) NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_exam_sub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[member_auth]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[member_auth](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[auth_type] [nvarchar](50) NULL,
	[code] [nvarchar](50) NULL,
	[openId] [nvarchar](50) NULL,
	[is_auth] [int] NULL,
	[authtime] [datetime] NULL,
	[datetime] [datetime] NULL,
	[auth_key] [nvarchar](50) NULL,
 CONSTRAINT [PK_teacher_auth] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[motto]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[motto](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[motto] [nvarchar](max) NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_motto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[news_list]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[news_list](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[des] [nvarchar](200) NULL,
	[type] [nvarchar](50) NULL,
	[ablum] [nvarchar](350) NULL,
	[link] [nvarchar](50) NULL,
	[createtime] [datetime] NULL,
 CONSTRAINT [PK_news_list] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[news_type]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[news_type](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[ablum] [nvarchar](max) NULL,
	[createtime] [datetime] NULL,
	[linkId] [nvarchar](100) NULL,
 CONSTRAINT [PK_news_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_list]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_list](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[subject_id] [nvarchar](50) NULL,
	[order_type] [int] NULL,
	[money] [nvarchar](50) NULL,
	[price_menu] [nvarchar](50) NULL,
	[hours] [float] NULL,
	[date_time] [datetime] NULL,
 CONSTRAINT [PK_order_list] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[parent]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[parent](
	[id] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[phone] [nvarchar](50) NULL,
	[binding] [nvarchar](50) NULL,
	[datetime] [datetime] NULL,
	[openid] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pricemenu]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pricemenu](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[infoid] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[price] [nvarchar](50) NULL,
	[createtime] [datetime] NULL,
 CONSTRAINT [PK_pricemenu] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pricemenu_sub]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pricemenu_sub](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_pricemenu_sub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[qualifications]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qualifications](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[img] [nvarchar](100) NULL,
	[mainId] [nvarchar](50) NULL,
 CONSTRAINT [PK_qualifications] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[recharge_history]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[recharge_history](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[user_id] [nvarchar](50) NULL,
	[priceMenu_id] [nvarchar](50) NULL,
	[money] [nvarchar](50) NULL,
	[create_time] [nvarchar](50) NULL,
 CONSTRAINT [PK_recharge_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[school]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[school](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[school] [nvarchar](50) NULL,
	[lat] [nvarchar](50) NULL,
	[lon] [nvarchar](50) NULL,
	[map_img] [nvarchar](350) NULL,
 CONSTRAINT [PK_school] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sign]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sign](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[student_id] [nvarchar](50) NULL,
	[class_id] [nvarchar](50) NULL,
	[sign_type] [int] NULL,
	[date] [datetime] NULL,
	[time] [datetime] NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sign_class]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sign_class](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[date] [datetime] NULL,
	[class_id] [nvarchar](50) NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_sign_class] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[student]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[student](
	[id] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[age] [nvarchar](50) NULL,
	[birthday] [nvarchar](50) NULL,
	[gender] [nvarchar](50) NULL,
	[avatar] [nvarchar](150) NULL,
	[phone] [nvarchar](13) NULL,
	[school] [nvarchar](150) NULL,
	[snumber] [nvarchar](50) NULL,
	[parentnumber] [nvarchar](50) NULL,
	[qq] [nvarchar](50) NULL,
	[wechat] [nvarchar](50) NULL,
	[jointime] [datetime] NULL,
	[mark] [nvarchar](max) NULL,
	[infomation] [nvarchar](max) NULL,
	[openid] [nvarchar](150) NULL,
	[unionid] [nvarchar](150) NULL,
	[classid] [varchar](50) NULL,
	[status] [int] NULL,
	[authStatus] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[studenttemp]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[studenttemp](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[age] [int] NULL,
	[gender] [nvarchar](50) NULL,
	[avatar] [nvarchar](150) NULL,
	[phone] [nvarchar](13) NULL,
	[school] [nvarchar](150) NULL,
	[snumber] [nvarchar](50) NULL,
	[parentnumber] [nvarchar](50) NULL,
	[qq] [nvarchar](50) NULL,
	[wechat] [nvarchar](50) NULL,
	[jointime] [datetime] NULL,
	[mark] [nvarchar](max) NULL,
	[infomation] [nvarchar](max) NULL,
	[openid] [nvarchar](150) NULL,
	[unionid] [nvarchar](150) NULL,
	[classid] [varchar](50) NULL,
	[status] [int] NULL,
	[authStatus] [int] NULL,
 CONSTRAINT [PK_studenttemp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[subject]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subject](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[img] [nvarchar](500) NULL,
	[create_time] [datetime] NULL,
 CONSTRAINT [PK_subject] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[teacher]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[teacher](
	[id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[avatar] [nvarchar](1000) NULL,
	[banner] [nvarchar](1000) NULL,
	[gender] [nvarchar](50) NULL,
	[phone] [nvarchar](50) NULL,
	[school] [nvarchar](50) NULL,
	[diploma] [nvarchar](1000) NULL,
	[remark] [nvarchar](1100) NULL,
	[smart] [nvarchar](50) NULL,
	[access_token] [nvarchar](150) NULL,
	[openid] [nvarchar](150) NULL,
	[unionid] [nvarchar](150) NULL,
	[has_del] [nvarchar](50) NULL,
	[datetime] [datetime] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_teacher] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[teacher_jurisdiction]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[teacher_jurisdiction](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[sign] [nvarchar](50) NULL,
	[show] [int] NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_teacher_jurisdiction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[teacher_photo]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[teacher_photo](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[url] [nvarchar](300) NULL,
	[linkid] [nvarchar](50) NULL,
	[type] [nvarchar](50) NULL,
 CONSTRAINT [PK_teacher_photo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[teacher_seniority]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[teacher_seniority](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NULL,
	[create_time] [datetime] NULL,
	[link_id] [nvarchar](50) NULL,
 CONSTRAINT [PK_teacher_seniority] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[teacher_sub]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[teacher_sub](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[img] [nvarchar](500) NULL,
	[link_id] [nvarchar](50) NULL,
 CONSTRAINT [PK_teacher_sub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[time_order]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[time_order](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[student_id] [nvarchar](50) NULL,
	[class_id] [nvarchar](50) NULL,
	[order_type] [nvarchar](50) NULL,
	[order_time] [float] NULL,
	[detail] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[time] [datetime] NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_time_order] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user_list]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_list](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[userId] [nvarchar](50) NULL,
	[avatar] [nvarchar](350) NULL,
	[nick] [nvarchar](50) NULL,
	[sex] [int] NULL,
	[birthday] [nvarchar](50) NULL,
	[phone] [nvarchar](11) NULL,
	[login_type] [int] NULL,
	[device] [nvarchar](50) NULL,
	[union_id] [nvarchar](150) NULL,
	[open_id] [nvarchar](150) NULL,
	[user_sig] [nvarchar](600) NULL,
	[money] [int] NULL,
	[createtime] [datetime] NULL,
	[im_id] [nvarchar](50) NULL,
	[zhifupay] [nvarchar](50) NULL,
	[user_type] [int] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[wechat_auth_notice_times]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wechat_auth_notice_times](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[openid] [nvarchar](50) NULL,
	[auth_key] [nvarchar](50) NULL,
	[times] [int] NULL,
 CONSTRAINT [PK_wechat_auth_notice_times] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[wechat_location]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wechat_location](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[openid] [nvarchar](100) NULL,
	[lat] [nvarchar](50) NULL,
	[lon] [nvarchar](50) NULL,
 CONSTRAINT [PK_wechat_location] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[wechat_menu]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wechat_menu](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[type] [int] NULL,
	[info] [nvarchar](150) NULL,
	[parent] [nchar](10) NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_wechat_menu] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[wechat_notif]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wechat_notif](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[class_id] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[time] [datetime] NULL,
	[type] [int] NULL,
 CONSTRAINT [PK_wechat_notif] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[wechat_power]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wechat_power](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](50) NULL,
	[info] [nvarchar](max) NULL,
 CONSTRAINT [PK_wechat_power] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[wechatEvent]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wechatEvent](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[info] [nvarchar](max) NULL,
	[name] [nvarchar](max) NULL,
	[openid] [nvarchar](150) NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_wechatEvent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[window_img_setting]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[window_img_setting](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[url] [nvarchar](max) NULL,
	[type] [int] NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_window_img_setting] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[work]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[work](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[workName] [nvarchar](50) NULL,
	[hasPush] [int] NULL,
	[pushType] [int] NULL,
	[pushUser] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[linkId] [nvarchar](50) NULL,
 CONSTRAINT [PK_work] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[work_student]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[work_student](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[student_id] [nvarchar](80) NULL,
	[linkId] [nvarchar](50) NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_work_student] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[work_sub]    Script Date: 2020/1/18 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[work_sub](
	[id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](150) NULL,
	[linkId] [nvarchar](50) NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费课程' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'order_list', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1进2出' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'order_list', @level2type=N'COLUMN',@level2name=N'order_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'order_list', @level2type=N'COLUMN',@level2name=N'money'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'价目表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'order_list', @level2type=N'COLUMN',@level2name=N'price_menu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'order_list', @level2type=N'COLUMN',@level2name=N'hours'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消费时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'order_list', @level2type=N'COLUMN',@level2name=N'date_time'
GO
USE [master]
GO
ALTER DATABASE [Chire] SET  READ_WRITE 
GO
