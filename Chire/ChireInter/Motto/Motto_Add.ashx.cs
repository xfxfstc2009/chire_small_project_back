﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Motto
{
    /// <summary>
    /// Motto_Add 的摘要说明
    /// </summary>
    public class Motto_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string motto = "";
      
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("motto"))
            {
                motto = contextWithBase.Request.Form["motto"];
            }
        
            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into motto(motto) values(@motto)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@motto", SqlDbType.VarChar, 50);
            cmd.Parameters["@motto"].Value = motto;
          

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTMotto_Model out_base_setting = new OUTMotto_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}