﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Motto
{
    public class Motto_Action
    {
        #region 获取箴言
        public string getMotto()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from motto";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> music_ablumList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string motto = dt.Rows[i]["motto"].ToString();
                music_ablumList.Add(motto);
            }
            sqlcon.Close();

            string backInfo = "";
            if (music_ablumList.Count > 0)
            {
                Random ran = new Random();
                int rndNum = ran.Next(music_ablumList.Count);
                backInfo = music_ablumList[rndNum].Length == 0 ? "千里之行始于足下" : music_ablumList[rndNum];
            }
            else {
                backInfo = "千里之行始于足下";
            }
           
           
            return backInfo;
        }
        #endregion
    }
}