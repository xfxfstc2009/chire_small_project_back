﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Task;
using Newtonsoft.Json;

namespace Chire.ChireInter.Motto
{
    /// <summary>
    /// Motto_List 的摘要说明
    /// </summary>
    public class Motto_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        
        }

        #region 验证信息
        public void analysisInfo()
        {

            getAllInfo();
        }
        #endregion

        public void getAllInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from motto";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> music_ablumList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string motto = dt.Rows[i]["motto"].ToString();
                music_ablumList.Add(motto);
            }
            sqlcon.Close();
            successManager(music_ablumList);
        }

        #region 执行方法
        public void successManager(List<string> model)
        {
            OUTMotto_Model out_base_setting = new OUTMotto_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Motto_Model mottoModel = new Motto_Model();
            mottoModel.list = model;
            out_base_setting.data = mottoModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}