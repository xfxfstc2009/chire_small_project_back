﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Motto
{
    public class Motto_Model
    {
        public List<string> list;
    }

    public class OUTMotto_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Motto_Model data { get; set; }
    }
}