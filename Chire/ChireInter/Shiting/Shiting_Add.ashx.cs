﻿using Chire.ChireInter.School;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Shiting
{
    /// <summary>
    /// Shiting_Add 的摘要说明
    /// </summary>
    public class Shiting_Add : IHttpHandler
    {
        HttpContext contextWithBase;
   
        string name = "";
        string levels = "";
        string school = "";
        string phone = "";
        string score = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("levels"))
            {
                levels = contextWithBase.Request.Form["levels"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("school"))
            {
                school = contextWithBase.Request.Form["school"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
            {
                phone = contextWithBase.Request.Form["phone"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("score"))
            {
                score = contextWithBase.Request.Form["score"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("score"))
            {
                score = contextWithBase.Request.Form["score"];
            }
            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
     
            StrInsert = "insert into shiting(name,levels,school,phone,score,datetime,has_huifang) values (@name,@levels,@school,@phone,@score,@datetime,@has_huifang)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            // 1. 日记标题
            cmd.Parameters.Add("@levels", SqlDbType.VarChar, 50);
            cmd.Parameters["@levels"].Value = levels;
            // 2.日记详情
            cmd.Parameters.Add("@school", SqlDbType.VarChar, 50);
            cmd.Parameters["@school"].Value = school;

            cmd.Parameters.Add("@phone", SqlDbType.VarChar, 350);
            cmd.Parameters["@phone"].Value = phone;


            cmd.Parameters.Add("@score", SqlDbType.VarChar, 350);
            cmd.Parameters["@score"].Value = score;

            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 350);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString() ;

            cmd.Parameters.Add("@has_huifang", SqlDbType.VarChar, 350);
            cmd.Parameters["@has_huifang"].Value = "0";

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();

            scuccessedManager();
        }
        #endregion

        #region 成功方法
        public void scuccessedManager()
        {
            OUTSchool_List_Model out_base_setting = new OUTSchool_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "success";

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}