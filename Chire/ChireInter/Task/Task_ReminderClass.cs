﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.Event_calendar;
using Chire.ChireInter.Order;
using Chire.ChireInter.Sign;
using Chire.ChireInter.Students;
using Chire.wechat;

namespace Chire.ChireInter.Task
{
    // 【每天早上，如果有课进行提醒】
    // 逻辑
    // 1. 获取当天的所有事件
    // 2. 找到事件对应的班级
    // 3. 找到班级里面的所有同学
    // 4. 对所有同学进行上课提醒
    // 5. 对班级内的同学的签到状态设置为待签到
    // 6. 对该同学生成一张未消费订单。

    public class Task_ReminderClass
    {
            #region 0.主方法
            public void mainManager()
            {
                // 1. 获取当天的事件
                Event_Action eventAction = new Event_Action();
                Class_Action classAction = new Class_Action();
                Student_Action studentAction = new Student_Action();
                Sign_Action signAction = new Sign_Action();
                List<Event_Model> currentDayeventList = eventAction.getEventList(DateTime.Now.Date.ToString());
                // 2.根据所有的事件获取当前的课程id
                List<Class_Model> classList = new List<Class_Model>();
                for (int i = 0; i < currentDayeventList.Count; i++)
                {
                    Event_Model eventModel = currentDayeventList[i];
                    // 判断今天是否该班级上过课
                    bool hasUseClass = todayHasClass(eventModel.class_id);
                    if (hasUseClass == false)
                    {             // 没上过课的话进行插入
                        // 根据classid 获取当前的班级
                        Class_Model singleClassModel = classAction.getClassInfoWithId(eventModel.class_id);
                        // 1. 获取随机数
                        string randomStr = Constance.Instance.getRandomStr(10);
                        // 插入待上课信息
                        addClassStartRember(eventModel.class_id);
                        // 设置所有的学生为未签到状态
                        signAction.addAllStudentSignInWithClassId(eventModel.class_id, randomStr);
                        // 插入新课程信息
                        signAction.addSignClassInfo(eventModel.class_id, randomStr);
                        // 进行发送信息
                        List<Student_Model> studentList = studentAction.getAllStudentWithClassId(eventModel.class_id);
                        for (int j = 0; j < studentList.Count; j++)
                        {
                            Student_Model studentModel = studentList[j];
                            sendMsgWithClassWillStart(studentModel, singleClassModel, eventModel);
                        }
                        // 将当前是否推送的条目设置为已推送
                        eventAction.wechatSettingSignPush(eventModel.id);
                    }
                }
            }
            #endregion

            #region 3.获取今天星期几
            public string getCurrentDay()
            {
                string[] Day = new string[] { "0", "1", "2", "3", "4", "5", "6" };
                string week = Day[Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"))].ToString();
                return week;
            }
            #endregion

            #region 4.发送信息
            public void sendMsgWithClassWillStart(Student_Model studentModel, Class_Model classInfo,Event_Model eventModel)
            {
                
            // 2. 拼接信息
            SmallProjectModelDataList wechatModelList = new SmallProjectModelDataList();

            // 1. 用户昵称
            SmallProjectModelData name1 = new SmallProjectModelData();
            name1.value = studentModel.name.Length > 0?studentModel.name:"同学";
            // 2. 内容
            SmallProjectModelData data2 = new SmallProjectModelData();
            data2.value = eventModel.date + "-" + eventModel.time;
            // 3. 授权方式
            SmallProjectModelData thing4 = new SmallProjectModelData();
            thing4.value = "今天有课哦，不要忘了。";



            wechatModelList.name1 = name1;
            wechatModelList.thing2 = data2;
            wechatModelList.thing4 = thing4;

            Xiaochengxu_Action xiaochengxuAction = new Xiaochengxu_Action();
            xiaochengxuAction.smallproject_morningclass_tixing(studentModel.openid, wechatModelList);
            }
            #endregion

            #region 5.插入当天的开课记录
            public void addClassStartRember(string classId)
            {
                Order_Action action = new Order_Action();
                action.order_willStartGetList(classId);
            }
            #endregion

            #region 判断今天是否上过课
            public bool todayHasClass(string classid)
            {
                bool has = false;
                SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
                sqlcon.Open();

                string strselect = "select top 1 * from sign_class where class_id = '" + classid + "' and date = '" + DateTime.Now.ToShortDateString() + "'";
                SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
                SqlDataReader dr = sqlcmd.ExecuteReader();


                if (dr.Read())
                {
                    has = true;
                }
                else
                {
                    has = false;
                }
                sqlcon.Close();

                return has;
            }
            #endregion
        }

    
}