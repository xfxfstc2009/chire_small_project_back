﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.Event_calendar;
using Chire.ChireInter.Students;
using Chire.wechat;

namespace Chire.ChireInter.Task
{
    // 逻辑
    // 1. 获取所有的班级
    // 2. 获取下周的开始时间与结束时间
    // 3. 获取每个班级的开课时间换算成时间戳插入到事件列表
    // 4. 将所有牵涉到课程的学生进行写入数据库.
    // 5. 设置该条信息已经插入，防止重复插入。


    #region 每周日进行下周的事件同步添加
    public class Task_ClassWeek_EveryWeek
    {
        // 1. 
        public void mainManager()
        {
            getAllClassManager();
        }

        // 2. 
        public void getAllClassManager()
        {
            Event_Action eventAction = new Event_Action();

            // 1. 获取所有班级
            Class_Action classAction = new Class_Action();
            List<Class_Model> classList = classAction.classGetAllClass();

            // 2. 获取当前周几
            string weekDay = getCurrentDay(DateTime.Now);

            // 获取每周开始日期时间戳
            DateTime weekStartDateTime = Constance.WeekStartTime;
            long currentTimeInt = Constance.Instance.ConvertDateTimeInt(weekStartDateTime);
            // 获取本周结束日时间戳
            DateTime weekEndDateTime = Constance.WeekEndTime;
            long endTimeInt = Constance.Instance.ConvertDateTimeInt(weekEndDateTime);
            // 1. 判断是否插入过数据
            bool hasEventInsert = eventAction.eventWeekHas(currentTimeInt, endTimeInt);
            if (hasEventInsert == false)
            {
                for (int i = 0; i < classList.Count; i++)
                {
                    Class_Model classModel = classList[i];
                    // 获取本课程的开课周期
                    List<Class_Time_Model> classTimeArr = classModel.classtimelist;

                    // 2. 我该课程的开课时间进行循环
                    for (int wt = 0; wt < classTimeArr.Count; wt++)
                    {
                        // 1. 获取当前的timeModel
                        Class_Time_Model singleClassTimeModel = classTimeArr[wt];
                        // 2. 根据当前时间进行插入事件
                        long dayTimeInt = Convert.ToInt32(singleClassTimeModel.week) * 24 * 3600;
                        // 获取每天的时间戳
                        long everyTimeInt = dayTimeInt + currentTimeInt;
                        // 时间戳转为日期
                        DateTime dayTime = Constance.ConvertLongToDateTime(everyTimeInt);

                        //拼接课程时间
                        string dayTimeStr = dayTime.ToString();
                        string[] dayTimeStr1 = dayTimeStr.Split(' ');
                        string dayTimeStr2 = dayTimeStr1[0];

                        string dayTimeStr3 = dayTimeStr2 + " " + singleClassTimeModel.begintime;
                        DateTime dayTime4 = Convert.ToDateTime(dayTimeStr3);
                        string dayTime5 = dayTime4.ToString();

                        // 进行插入事件
                        string eventId = eventAction.addEventManager(classModel.name, dayTime5, classModel.id, EventType.EventTypeClass);

                        // 根据班级获取所有学生
                        Student_Action studentAction = new Student_Action();
                        List<Student_Model> studentList = studentAction.getAllStudentWithClassId(classModel.id);
                        // 根据学生将事件按数据插入到表中
                        for (int s = 0; s < studentList.Count; s++)
                        {
                            Student_Model student = studentList[s];
                            eventAction.addEventWithStudent(student.id, eventId);
                        }
                    }

                }
                // 插入记录数据
                eventAction.eventWeekAdd(currentTimeInt, endTimeInt);
            }
        }

        #region 3.获取今天星期几
        public string getCurrentDay(DateTime time)
        {
            string[] Day = new string[] { "0", "1", "2", "3", "4", "5", "6" };
            string week = Day[Convert.ToInt32(time.DayOfWeek.ToString("d"))].ToString();
            return week;
        }
        #endregion
    }
    #endregion

}