﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.Event_calendar;
using Chire.ChireInter.Motto;
using Chire.ChireInter.Order;
using Chire.ChireInter.Sign;
using Chire.ChireInter.Students;
using Chire.wechat;

namespace Chire.ChireInter.Task
{
    // 每天晚上7点进行事件推送
    public class Task_Event_EveryDay
    {
        #region 每天早上7点提醒
        public class Task_EveryDay
        {
            #region 0.主方法
            public void mainManager()
            {
                // 1. 获取当天的事件
                Event_Action eventAction = new Event_Action();
                Class_Action classAction = new Class_Action();
                Student_Action studentAction = new Student_Action();
                Sign_Action signAction = new Sign_Action();
                List<Event_Model> currentDayeventList = eventAction.getEventList(DateTime.Now.Date.ToString());
                // 2.根据所有的事件获取当前的课程id
                List<Class_Model> classList = new List<Class_Model>();
                for (int i = 0; i < currentDayeventList.Count; i++)
                {
                    Event_Model eventModel = currentDayeventList[i];
                    // 判断今天是否该班级上过课
                    bool hasUseClass = todayHasClass(eventModel.class_id);
                    if (hasUseClass == false)
                    {             // 没上过课的话进行插入
                        // 根据classid 获取当前的班级
                        Class_Model singleClassModel = classAction.getClassInfoWithId(eventModel.class_id);
                        // 1. 获取随机数
                        string randomStr = Constance.Instance.getRandomStr(10);
                        // 插入待上课信息
                        addClassStartRember(eventModel.class_id);
                        // 设置所有的学生为未签到状态
                        signAction.addAllStudentSignInWithClassId(eventModel.class_id, randomStr);
                        // 插入新课程信息
                        signAction.addSignClassInfo(eventModel.class_id, randomStr);
                        // 进行发送信息
                        List<Student_Model> studentList = studentAction.getAllStudentWithClassId(eventModel.class_id);
                        for (int j = 0; j < studentList.Count; j++)
                        {
                            Student_Model studentModel = studentList[j];
                            sendMsgWithClassWillStart(studentModel, singleClassModel);
                        }
                        // 将当前是否推送的条目设置为已推送
                        eventAction.wechatSettingSignPush(eventModel.id);
                    }
                }
            }
            #endregion

            #region 3.获取今天星期几
            public string getCurrentDay()
            {
                string[] Day = new string[] { "0", "1", "2", "3", "4", "5", "6" };
                string week = Day[Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"))].ToString();
                return week;
            }
            #endregion

            #region 4.发送信息
            public void sendMsgWithClassWillStart(Student_Model studentModel, Class_Model classInfo)
            {
                Wechat_AccessToken wechatToken = new Wechat_AccessToken();

                WechatModelData first = new WechatModelData();
                first.value = "【炽热教育】开课提醒";
                first.color = "#FF0000";

                WechatModelData keyword1 = new WechatModelData();
                keyword1.value = classInfo.name;
                keyword1.color = "#173177";

                WechatModelData keyword2 = new WechatModelData();
                keyword2.value = classInfo.begintime + " - " + classInfo.endtime;
                keyword2.color = "#173177";

                WechatModelData keyword3 = new WechatModelData();
                keyword3.value = "万豪洋服";
                keyword3.color = "#173177";

                WechatModelData keyword4 = new WechatModelData();
                keyword4.value = classInfo.teacher.name + classInfo.teacher.phone;
                keyword4.color = "#173177";

                Motto_Action mottoAction = new Motto_Action();
                WechatModelData remark = new WechatModelData();
                remark.value = mottoAction.getMotto();
                remark.color = "#173177";

                WechatModelDataList list = new WechatModelDataList();
                list.first = first;
                list.keyword1 = keyword1;
                list.keyword2 = keyword2;
                list.keyword3 = keyword3;
                list.keyword4 = keyword4;
                list.remark = remark;

                wechatToken.wechatStartClassMsg(studentModel.openid, list);

            }
            #endregion

            #region 5.插入当天的开课记录
            public void addClassStartRember(string classId)
            {
                Order_Action action = new Order_Action();
                action.order_willStartGetList(classId);
            }
            #endregion

            #region 判断今天是否上过课
            public bool todayHasClass(string classid)
            {
                bool has = false;
                SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
                sqlcon.Open();

                string strselect = "select top 1 * from sign_class where class_id = '" + classid + "' and date = '" + DateTime.Now.ToShortDateString() + "'";
                SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
                SqlDataReader dr = sqlcmd.ExecuteReader();


                if (dr.Read())
                {
                    has = true;
                }
                else
                {
                    has = false;
                }
                sqlcon.Close();

                return has;
            }
            #endregion
        }

        #endregion
    }
}