﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Sign;
using Chire.wechat;

namespace Chire.ChireInter.Task
{
    public class Task_BeforeNoSignOut
    {
        //每周六晚上5点半推送
        // 用来判断上周是否没签退的
        public void mainManager() {
            getBeforeSignNoSignStudent();
        }

        #region 1.去课时订单处进行申请上周所有为签到的
        public void getBeforeSignNoSignStudent() {
            // 1. 获取结算日期的开始
            DateTime startDate = DateTime.Now.AddDays(-7);
            // 2. 获取本周的结束
            DateTime endDate = DateTime.Now.AddDays(-1);
            // 3. 获取本周所有的未签退的
            Sign_Action signAction = new Sign_Action();
            List<Sign_Model> signList = signAction.getNoSignInOrSignOutTime(startDate, endDate);
            // 4. 对为签到或者为签退的进行推送
            for (int i = 0; i < signList.Count; i++)
            {
                Sign_Model signModel = signList[i];
                pushManager(signModel);
            }
        }
        #endregion

        #region 推送
        //获取本周的开始日期
        public void pushManager(Sign_Model signModel) { 

            // 2. 拼接信息
            SmallProjectModelDataList wechatModelList = new SmallProjectModelDataList();

            SmallProjectModelData thing5 = new SmallProjectModelData();
            thing5.value = signModel.student.name;

            SmallProjectModelData time3 = new SmallProjectModelData();
            time3.value = signModel.date.ToString();

            SmallProjectModelData thing4 = new SmallProjectModelData();
            thing4.value = "同学,你忘记签到了,如老师调课请忽略。";


            wechatModelList.thing5 = thing5;
            wechatModelList.time3 = time3;
            wechatModelList.thing4 = thing4;
         
            Xiaochengxu_Action xiaochengxuAction = new Xiaochengxu_Action();
            xiaochengxuAction.smallproject_keqiankehou_tixing(signModel.student_id, wechatModelList);

        }

        #endregion
    }
}