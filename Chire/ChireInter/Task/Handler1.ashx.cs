﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Task
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
            // 1. 每周的课表插入到事件
            //Task_ClassWeek_EveryWeek task = new Task_ClassWeek_EveryWeek();
            //task.mainManager();
            // 2.每天早上7点进行订单操作
            Task_ReminderClass task2 = new Task_ReminderClass();
            task2.mainManager();

            // 3.
            //Task_ClassBefore_Tixing task3 = new Task_ClassBefore_Tixing();
            //task3.mainManager();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}