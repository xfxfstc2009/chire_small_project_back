﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Tool
{
    /// <summary>
    /// Tool_Update 的摘要说明
    /// </summary>
    public class Tool_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string img = "";
        int index = 1;
        int important = 1;
        string toolId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;

            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("toolId"))
            {
                toolId = contextWithBase.Request.Form["toolId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("img"))
            {
                img = contextWithBase.Request.Form["img"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("index"))
            {
                index = Convert.ToInt32(contextWithBase.Request.Form["index"]);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("important"))
            {
                important = Convert.ToInt32(contextWithBase.Request.Form["important"]);
            }

            insertToolInfoManager();
        }
        #endregion


        #region 插入科目
        public void insertToolInfoManager()
        {
            Tool_Action toolAction = new Tool_Action();
            toolAction.updateToolManager(toolId, name, img, index, important);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTTool_List_Model out_base_setting = new OUTTool_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}