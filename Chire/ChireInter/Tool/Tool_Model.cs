﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Tool
{
    public class Tool_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string img { get; set; }
        public int index { get; set; }
        public int important { get; set; }

        
    }

    public class OUTTool_Model_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Tool_Model data { get; set; }
    }

    public class Tool_List_Model
    {
        public List<Tool_Model> list;
    }

    public class OUTTool_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Tool_List_Model data { get; set; }
    }
}