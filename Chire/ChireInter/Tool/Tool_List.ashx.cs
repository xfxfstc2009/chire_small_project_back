﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Tool
{
    /// <summary>
    /// Tool_List 的摘要说明
    /// </summary>
    public class Tool_List : IHttpHandler
    {

        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            getAllInfo();
        }
        #endregion

        #region 获取所有科目
        public void getAllInfo()
        {
            Tool_Action subjectAction = new Tool_Action();
            List<Tool_Model> subjectList = subjectAction.getAllTool();
            successManager(subjectList);

        }
        #endregion

        #region 执行方法
        public void successManager(List<Tool_Model> subjectList)
        {
            OUTTool_List_Model out_base_setting = new OUTTool_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Tool_List_Model listModel = new Tool_List_Model();
            listModel.list = subjectList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}