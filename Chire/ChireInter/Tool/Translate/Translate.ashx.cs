﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Fanyi;
using Newtonsoft.Json;

namespace Chire.ChireInter.Tool.Translate
{
    /// <summary>
    /// Translate 的摘要说明
    /// </summary>
    public class Translate : IHttpHandler
    {
        HttpContext contextWithBase;
      
        string fromUserName;
        string info;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("fromUserName"))
            {
                fromUserName = contextWithBase.Request.Form["fromUserName"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("info"))
            {
                info = contextWithBase.Request.Form["info"];
            }


            fanyiManager();
        }
        #endregion

        #region 进行翻译
        public void fanyiManager()
        {
            Fanyi_Manager manager = new Fanyi_Manager();
            string eninfo = manager.fanyi(info);
            successManager(eninfo);
        }
        #endregion

        #region 成功方法
        public void successManager(string fanyiInfo)
        {
            OUTTranslate_Model out_base_setting = new OUTTranslate_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Translate_Model translateModel = new Translate_Model();
            translateModel.info = fanyiInfo;

            out_base_setting.data = translateModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion




     
    }
}