﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Tool.Translate
{
    public class Translate_Model
    {
        public string info { get; set; }
    }


    public class OUTTranslate_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Translate_Model data { get; set; }

    }
}