﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Tool
{
    public class Tool_Action
    {
        public void addToolManager(string name, string img, int index, int important)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into tool_list(name,img,indexs,important) values (@name,@img,@indexs,@important)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            // 1. 日记标题
            cmd.Parameters.Add("@img", SqlDbType.VarChar, 500);
            cmd.Parameters["@img"].Value = img;

            cmd.Parameters.Add("@indexs", SqlDbType.Int);
            cmd.Parameters["@indexs"].Value = index;

            cmd.Parameters.Add("@important", SqlDbType.Int);
            cmd.Parameters["@important"].Value = important;
            
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }

        #region 获取所有工具
        public List<Tool_Model> getAllTool()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from tool_list order by indexs asc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Tool_Model> subjectList = new List<Tool_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string img = dt.Rows[i]["img"].ToString();
                int index = Convert.ToInt32(dt.Rows[i]["indexs"].ToString());
                int important = Convert.ToInt32(dt.Rows[i]["important"].ToString());

                
                Tool_Model subjectModel = new Tool_Model()
                {
                    id = id,
                    name = name,  
                    img = img,
                    index = index,
                    important = important,
                };
                subjectList.Add(subjectModel);
            }
            sqlcon.Close();
            return subjectList;
        }
        #endregion

        #region 获取当前内容信息
        public Tool_Model getCurrentInfoManager(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from tool_list where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();


            Tool_Model teacherModel = new Tool_Model();
            if (dr.Read())
            {
                teacherModel.id = dr["id"].ToString();
                teacherModel.name = dr["name"].ToString();
                teacherModel.img = dr["img"].ToString();
                teacherModel.index = Convert.ToInt32(dr["indexs"].ToString());
                teacherModel.important = Convert.ToInt32(dr["important"].ToString());
             
            }
            sqlcon.Close();
            return teacherModel;
        }
        #endregion


        #region 修改当前工具内容
        public void updateToolManager(string id, string name, string img, int index, int important)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息

            string strSqls = "update tool_list set name = '" + name + "',img='" + img + "' ,indexs = '" + index + "',important = '" + important + "'  where id = '" + id + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

    }
}