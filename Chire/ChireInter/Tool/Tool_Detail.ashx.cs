﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Tool
{
    /// <summary>
    /// Tool_Detail 的摘要说明
    /// </summary>
    public class Tool_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string toolId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;

            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("toolId"))
            {
                toolId = contextWithBase.Request.Form["toolId"];
            }
          
            insertToolInfoManager();
        }
        #endregion


        #region 插入科目
        public void insertToolInfoManager()
        {
            Tool_Action toolAction = new Tool_Action();
            Tool_Model toolModel = toolAction.getCurrentInfoManager(toolId);
            successManager(toolModel);
        }
        #endregion

        #region 成功方法
        public void successManager(Tool_Model toolModel)
        {
            OUTTool_Model_Model out_base_setting = new OUTTool_Model_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = toolModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}