﻿using Chire.ChireInter.Download;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Delegate
{
    /// <summary>
    /// Delegate_Del 的摘要说明
    /// </summary>
    public class Delegate_Del : IHttpHandler
    {

        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }

            deleteDownload();
        }
        #endregion

        #region 删除数据
        public void deleteDownload()
        {
            Delegate_Action downloadAction = new Delegate_Action();
            downloadAction.downloadDeleteManager(id);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OutDownLoad_List out_base_setting = new OutDownLoad_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}