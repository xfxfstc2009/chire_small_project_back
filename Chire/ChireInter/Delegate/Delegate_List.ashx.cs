﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Delegate
{
    /// <summary>
    /// Delegate_List 的摘要说明
    /// </summary>
    public class Delegate_List : IHttpHandler
    {
        HttpContext contextWithBase;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            getAllInfo();
        }
        #endregion

        #region 获取所有下载列表
        public void getAllInfo()
        {
            Delegate_Action delegateAction = new Delegate_Action();
            List<Delegate_Model> downloadList = delegateAction.getAllDownLoadInfo();
            successManager(downloadList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Delegate_Model> model)
        {
            OutDelegate_List_Model out_base_setting = new OutDelegate_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Delegate_List_Model listModel = new Delegate_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion
       
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}