﻿using Chire.ChireInter.Download;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Delegate
{
    public class Delegate_Action
    {

        #region 插入下载信息
        public void downloadAddManager(DownLoad_Request_Model requestModel)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into delegate_info(name,url,datetime,info) values(@name,@url,@datetime,@info)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = requestModel.name;
            // 1. 日记标题
            cmd.Parameters.Add("@url", SqlDbType.VarChar, 1000);
            cmd.Parameters["@url"].Value = requestModel.url;
            // 2.日记详情
            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString(); ;
            // 2.日记详情
            cmd.Parameters.Add("@info", SqlDbType.VarChar, 1000);
            cmd.Parameters["@info"].Value = requestModel.info;
          
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 删除信息
        public void downloadDeleteManager(string id) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from delegate_info where id = '" + id + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 获取所有的内容
        public List<Delegate_Model> getAllDownLoadInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from delegate_info order by id desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Delegate_Model> music_ablumList = new List<Delegate_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string url = dt.Rows[i]["url"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();
                string info = dt.Rows[i]["info"].ToString();

                Delegate_Model downloadModel = new Delegate_Model() { info = info, id = id, name = name, url = url, datetime = datetime };
                music_ablumList.Add(downloadModel);
            }
            sqlcon.Close();
            return music_ablumList;
        }
        #endregion
    }
}