﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Delegate
{
    public class Delegate_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string datetime { get; set; }
        public string info { get; set; }
    }

    public class Delegate_List_Model
    {
        public List<Delegate_Model> list;
    }

    public class OutDelegate_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Delegate_List_Model data { get; set; }
    }
}