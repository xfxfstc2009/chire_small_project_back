﻿using Chire.ChireInter.Class;
using Chire.ChireInter.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Event_calendar
{
    public class Event_Model
    {
        public string id { get; set; }
        public string title { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public long dateInterval { get; set; }
        public long timeInterval { get; set; }
        public int has_del { get; set; }
        public string class_id { get; set; }
        public Class_Model classModel { get; set; }
    }

    public class OUTEvent_Model {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息;
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Event_Model data { get; set; }
    }


    public class Event_List_Model
    {
        public List<Event_Model> list { get; set; }
    }

    public class OUTEvent_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Event_List_Model data { get; set; }
    }



    #region 获取月份的列表
    public class Event_Month_Model {
        public string date { get; set; }
        public int count { get; set; }
    }

    public class Event_Month_List_Model
    {
        public List<Event_Month_Model> list { get; set; }
    }
    public class OUTEvent_Month_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Event_Month_List_Model data { get; set; }
    }
    #endregion

    #region 获取微信事件列表
    public class Event_Wechat_Model
    {
        public string date { get; set; }
        public string title { get; set; }
        public string time { get; set; }

    }

    public class Event_Wechat_List_Model
    {
        public List<Event_Wechat_Model> list { get; set; }

    }

    public class OUTEvent_Wechat_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Event_Wechat_List_Model data { get; set; }

    } 
    #endregion

    #region 获取某事件下面的学生列表
    public class Event_Student
    {
        public string id { get; set; }
        public string event_id { get; set; }
        public string userId { get; set; }
        public Student_Model student { get; set; }

    }

    public class Event_Student_List
    {
        public List<Event_Student> list { get; set; }
 
    }

        public class outEvent_Student_List
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Event_Student_List data { get; set; }
 
    }
    #endregion
}