﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Event_calendar
{
    /// <summary>
    /// 获取每个月下面的事件
    /// </summary>
    public class Event_Moon_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string year = "";
        string moon = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("year"))
            {
                year = contextWithBase.Request.Form["year"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("moon"))
            {
                moon = contextWithBase.Request.Form["moon"];
            }

            getEventMoonList();
        }
        #endregion

        #region 插入数据
        public void getEventMoonList()
        {
            Event_Action eventAction = new Event_Action();
            List<Event_Month_Model> list = eventAction.eventMoonList(year, moon);
            successManager(list);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Event_Month_Model> list)
        {
            OUTEvent_Month_List_Model out_base_setting = new OUTEvent_Month_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Event_Month_List_Model eventList = new Event_Month_List_Model();
            eventList.list = list;
            out_base_setting.data = eventList;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}