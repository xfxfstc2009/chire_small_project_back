﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Event_calendar
{
    /// <summary>
    /// 删除事件 - 用户
    /// </summary>
    public class Event_Del_EventWithUser : IHttpHandler
    {
        HttpContext contextWithBase;
        string eventId = "";
        string userId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("eventId"))
            {
                eventId = contextWithBase.Request.Form["eventId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("userId"))
            {
                userId = contextWithBase.Request.Form["userId"];
            }
            delEventWithEvent();
        }
        #endregion

        #region 插入数据
        public void delEventWithEvent()
        {
            Event_Action eventAction = new Event_Action();
            eventAction.eventDeleteEventWithUserAndEventId(eventId, userId);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTEvent_Wechat_List_Model out_base_setting = new OUTEvent_Wechat_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
     
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}