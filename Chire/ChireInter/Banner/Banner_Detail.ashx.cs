﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Banner
{
    /// <summary>
    /// Banner_Detail 的摘要说明
    /// </summary>
    public class Banner_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
         

            insertIntoActivity();
        }
        #endregion

        private void insertIntoActivity()
        {
            Banner_Action activityAction = new Banner_Action();
            Banner_Model bannerModel = activityAction.getBannerInfoWithId(id);
            successManager(bannerModel);
        }

        #region 成功方法
        public void successManager(Banner_Model bannerModel)
        {
            Out_Banner_Model out_base_setting = new Out_Banner_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = bannerModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}