﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Banner
{
    public class Banner_Model
    {
   
        public string id { get; set; }
        public string name { get; set; }
        public string direct { get; set; }
        public string img { get; set; }
        public string time { get; set; }
        public string invalid { get; set; }
        public string html { get; set; }
    }

    public class Banner_List_Model
    {

        public List<Banner_Model> list;
    }

    public class Out_Banner_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Banner_List_Model data { get; set; }
    }


    public class Out_Banner_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Banner_Model data { get; set; }
    }

}