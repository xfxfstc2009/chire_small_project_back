﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Banner
{
    /// <summary>
    /// Banner_List 的摘要说明
    /// </summary>
    public class Banner_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string invalid = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("invalid"))
            {
                invalid = contextWithBase.Request.Form["invalid"];
            }
            getAllInfo();
        }
        #endregion

        #region 获取所有课程信息
        public void getAllInfo()
        {
            Banner_Action bannerAction = new Banner_Action();
            bool activity = false;
            if (invalid == "" || invalid == "0"){
                activity = false;
            } else {
                activity = true;
            }
            List<Banner_Model> bannerList = bannerAction.getBannerListWithActivity(activity);
            successManager(bannerList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Banner_Model> bannerList)
        {
            Out_Banner_List_Model out_base_setting = new Out_Banner_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Banner_List_Model listModel = new Banner_List_Model();
            listModel.list = bannerList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}