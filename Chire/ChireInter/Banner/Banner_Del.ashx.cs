﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Banner
{
    /// <summary>
    /// Banner_Del 的摘要说明
    /// </summary>
    public class Banner_Del : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        bool invalid = false;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("invalid"))
            {
                invalid = Convert.ToBoolean(contextWithBase.Request.Form["invalid"]);
            }
            mainManager();
        }
        #endregion

        #region 主要操作
        public void mainManager()
        {
            Banner_Action activityAction = new Banner_Action();
            activityAction.deleteBanner(id,invalid);
            scuccessedManager();
        }

        #endregion

        #region 成功方法
        public void scuccessedManager()
        {
            Out_Banner_Model out_base_setting = new Out_Banner_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "success";

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}