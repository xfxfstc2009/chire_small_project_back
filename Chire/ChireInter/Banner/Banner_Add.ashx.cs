﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Banner
{
    /// <summary>
    /// Banner_Add 的摘要说明
    /// </summary>
    public class Banner_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string direct = "";
        string img = "";
        string html = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("direct"))
            {
                direct = contextWithBase.Request.Form["direct"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("img"))
            {
                img = contextWithBase.Request.Form["img"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("html"))
            {
                html = contextWithBase.Request.Form["html"];
                html = HttpUtility.UrlDecode(html);

            }
            insertIntoActivity();
        }
        #endregion

        private void insertIntoActivity()
        {
            Banner_Action activityAction = new Banner_Action();
            activityAction.addBannerManager(name, direct, img, true,html);
            successManager();
        }

        #region 成功方法
        public void successManager()
        {
            Out_Banner_Model out_base_setting = new Out_Banner_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}