﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.wechat;

namespace Chire.ChireInter.Banner
{
    public class Banner_Action
    {
     
        #region 添加banner
        public void addBannerManager(string name,string direct,string img,bool invalid,string html)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into banner(name,direct,img,time,invalid,html) values(@name,@direct,@img,@time,@invalid,@html)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;

            cmd.Parameters.Add("@direct", SqlDbType.VarChar, 1000);
            cmd.Parameters["@direct"].Value = direct;

            cmd.Parameters.Add("@img", SqlDbType.VarChar, 1000);
            cmd.Parameters["@img"].Value = img;

            cmd.Parameters.Add("@time", SqlDbType.VarChar, 50);
            cmd.Parameters["@time"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@invalid", SqlDbType.VarChar, 20);
            cmd.Parameters["@invalid"].Value = (invalid ==true?"1":"0");

            cmd.Parameters.Add("@html", SqlDbType.VarChar, 1000000);
            cmd.Parameters["@html"].Value = html;
            

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 获取bannerlist
        public List<Banner_Model> getBannerListWithActivity(bool activity)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            
            string strselect = "select * from banner where invalid = 1 order by id desc";
            if (activity == true)
            {
                strselect = "select * from banner where invalid = 1 order by id desc";
            }
            else {
                strselect = "select * from banner where invalid = 0 order by id desc";
            }
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Banner_Model> bannerList = new List<Banner_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string direct = dt.Rows[i]["direct"].ToString();
                string img = dt.Rows[i]["img"].ToString();
                string time = dt.Rows[i]["time"].ToString();

                long timeInterval = Constance.Instance.ConvertDateTimeInt(Convert.ToDateTime(time));
                string invalid = dt.Rows[i]["invalid"].ToString();

                Banner_Model bannerModel = new Banner_Model()
                {
                    id = id,
                    name = name,
                    direct = direct,
                    img = img,
                    time = time,
                    invalid = invalid,
                };
                bannerList.Add(bannerModel);
            }
            sqlcon.Close();
            return bannerList;

        
        }
        #endregion

        #region 删除
        public void deleteBanner(string id, bool invalid)
        {
          //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update banner set invalid = '" + (invalid == true?1:0) + "'  where id ='" + id + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 根据id获取banner
        public Banner_Model getBannerInfoWithId(string id)
        {
          SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from banner where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Banner_Model bannerModel = new Banner_Model();
            if (dr.Read())
            {
                bannerModel.id = id;
                bannerModel.name = dr["name"].ToString();
                bannerModel.direct = dr["direct"].ToString();
                bannerModel.img = dr["img"].ToString();
                bannerModel.time = dr["time"].ToString();
                bannerModel.html = dr["html"].ToString();

            }
            sqlcon.Close();
            return bannerModel;
        }
        #endregion



    }
}