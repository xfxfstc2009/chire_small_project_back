﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Chire.ChireInter.Students;
using Chire.ChireInter.Class;

namespace Chire.ChireInter.Sign
{
    /// <summary>
    /// Sign_Class_Detail 的摘要说明
    /// </summary>
    public class Sign_Class_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string linkId = "";                   // 身份id
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证资料
        public void analysisInfo() {
            if (contextWithBase.Request.Form.AllKeys.Contains("linkId"))
            {
                linkId = contextWithBase.Request.Form["linkId"];
            }

            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager() { 
            Sign_Action signAction = new Sign_Action();
            List<Sign_Model> signList = signAction.getSignListWithLinkId(linkId);
            List<Sign_Model> mainSignList = new List<Sign_Model>();
            Student_Action studentAction = new Student_Action();
            Class_Action classAction = new Class_Action();

            for (int i = 0; i < signList.Count; i += 2) {
                Sign_Model sign_singleModel1 = signList[i];
                for (int j = 0; j < signList.Count; j++) {
                    Sign_Model sign_singleModel2 = signList[j];
                    if (sign_singleModel1.student_id.Equals(sign_singleModel2.student_id)
                        && sign_singleModel1.class_id.Equals(sign_singleModel2.class_id)
                        && !sign_singleModel1.sign_type.Equals(sign_singleModel2.sign_type)) {
                            Sign_Model signMainModel = new Sign_Model();
                            signMainModel.id = sign_singleModel1.id;
                            signMainModel.date = sign_singleModel1.date;
                            signMainModel.student_id = sign_singleModel1.student_id;
                            signMainModel.student = studentAction.getStudentInfoWithId(signMainModel.student_id);
                            signMainModel.class_id = sign_singleModel1.class_id;
                            signMainModel.classModel = classAction.getClassInfoWithId(sign_singleModel1.class_id);
                            if (sign_singleModel1.sign_type.Equals("0"))// 未签到
                            {
                                signMainModel.sign_in = "0";
                            }
                            else if (sign_singleModel1.sign_type.Equals("1"))
                            {                                      // 已签到
                                signMainModel.sign_in = "1";
                            }
                            else if (sign_singleModel1.sign_type.Equals("2"))
                            {                                      // 已签到
                                signMainModel.sign_out = "0";
                            }
                            else if (sign_singleModel1.sign_type.Equals("3"))
                            {                                      // 已签到
                                signMainModel.sign_out = "1";
                            }

                            if (sign_singleModel2.sign_type.Equals("0"))// 未签到
                            {
                                signMainModel.sign_in = "0";
                            }
                            else if (sign_singleModel2.sign_type.Equals("1"))
                            {                                      // 已签到
                                signMainModel.sign_in = "1";
                            }
                            else if (sign_singleModel2.sign_type.Equals("2"))
                            {                                      // 已签到
                                signMainModel.sign_out = "0";
                            }
                            else if (sign_singleModel2.sign_type.Equals("3"))
                            {                                      // 已签到
                                signMainModel.sign_out = "1";
                            }

                            signMainModel.sign_in_time = sign_singleModel1.time;
                            signMainModel.sign_out_time = sign_singleModel2.time;

                            mainSignList.Add(signMainModel);
                    }
                }
            }






            successedManager(mainSignList);
        }
        #endregion

        #region 成功方法
        public void successedManager(List<Sign_Model> signList) {
            OUTSign_List_Model out_base_setting = new OUTSign_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口执行成功";
            Sign_List_Model dataModel = new Sign_List_Model();
            dataModel.list = signList;
            out_base_setting.data = dataModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

      
    }
}