﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Sign
{
    /// <summary>
    /// Sign_Class_List 的摘要说明
    /// </summary>
    public class Sign_Class_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string classid = "";                   // 身份id
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("classid"))
            {
                classid = contextWithBase.Request.Form["classid"];
            }
          
            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager()
        {
            Sign_Action signAction = new Sign_Action();
            List<Sign_Class_Model> signClassList = signAction.getSignClassListWithClassId(classid);
            successManager(signClassList);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Sign_Class_Model> signClassList) {
            OUTSign_Class_List_Model out_base_setting = new OUTSign_Class_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口执行成功";
            Sign_Class_List_Model dataModel = new Sign_Class_List_Model();
            dataModel.list = signClassList;
            out_base_setting.data = dataModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}