﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Sign
{
    public class Sign_Model
    {
        public string id { get; set; }
        public string student_id { get; set; }
        public Student_Model student { get; set; }
        public string class_id { get; set; }
        public Class_Model classModel { get; set; }
        public string sign_type { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string sign_in { get; set; }
        public string sign_in_time { get; set; }
        public string sign_out { get; set; }
        public string sign_out_time { get; set; }

    }

    public class Sign_List_Model {
        public List<Sign_Model> list { get; set; }
    }

    public class OUTSign_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Sign_List_Model data { get; set; }
    }

    public class Sign_Status_Model
    {
        public Sign_Model sign_in { get; set; }
        public Sign_Model sign_out { get; set; }
        public Class_Model classModel { get; set; }
        
    }

    public class OUTSign_Status_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Sign_Status_Model data { get; set; }
    }

    public class OUTSign_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Sign_Model data { get; set; }
    }



    public class Sign_Class_Model
    {
        public string id {get;set;}
        public string date{get;set;}
        public string class_id {get;set;}
        public string linkId { get; set; }
        public Class_Model classModel { get; set; }
        public long dateTimeInterval { get; set; }
    }

    public class Sign_Class_List_Model
    {
        public List<Sign_Class_Model> list { get; set; }
 
    }

    public class OUTSign_Class_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Sign_Class_List_Model data { get; set; }

    }

}