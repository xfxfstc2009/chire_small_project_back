﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Identity
{
    /// <summary>
    /// Identity_HasAuthMember 的摘要说明
    /// </summary>
    public class Identity_HasAuthMember : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证参数
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            getHasMember();
        }
        
        #endregion

        #region 获取所有资料
        public void getHasMember() {
            Identity_Action identityAction = new Identity_Action();
            bool isMember = identityAction.hasChireUserMember(code);
            successManager(isMember);
        }
        #endregion

        #region 输出
        public void successManager(bool isMember) {

            OUTIdentity_HasMember_Model out_base_setting = new OUTIdentity_HasMember_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "请求成功";

            Identity_HasMember_Model identityModel = new Identity_HasMember_Model();
            if (isMember == true)
            {
                identityModel.hasMember = 1;
            }
            else {
                identityModel.hasMember = 0;
            }
       

            out_base_setting.data = identityModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}