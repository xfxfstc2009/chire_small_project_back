﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Identity
{
    /// <summary>
    /// Identity_AuthStatus 的摘要说明
    /// </summary>
    public class Identity_AuthStatus : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "0111jkZ21YcAxR1zJSY21n47Z211jkZh";                       // 传入code
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证参数
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            authStatus();
        }
        #endregion

        #region 获取当前认证状态
        public void authStatus() {
            Identity_Action identityAction = new Identity_Action();
            Identity_Model identityModel = identityAction.authMainManager(code);
            successedManager(identityModel);
        }
        #endregion

        #region 返回方法
        public void successedManager(Identity_Model model) {
            OUTIdentity_Model identityModel = new OUTIdentity_Model();
            identityModel.data = model;
            identityModel.errCode = "200";
            identityModel.errMsg = "请求成功";

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), identityModel);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}