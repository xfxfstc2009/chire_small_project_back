﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Parent;
using Chire.ChireInter.Students;
using Chire.ChireInter.Teacher;

namespace Chire.ChireInter.Identity
{
    public class Identity_Model
    {
        public int auth_type{get;set;}
        public string code { get; set; }
        public int is_auth { get; set; }
        public string openId { get; set; }
        public string authtime { get; set; }
        public string datetime { get; set; }
        public Student_Model student { get; set; }
        public Teacher_Model teacher { get; set; }
        public Parent_Model parent { get; set; }
        public string errorStudent { get; set; }
        public string errorTeacher { get; set; }
    }

    public class OUTIdentity_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Identity_Model data { get; set; }
    }


    public class Identity_HasMember_Model
    {
        public int hasMember { get; set; }
    }
    public class OUTIdentity_HasMember_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Identity_HasMember_Model data { get; set; }
    }

    // 绑定次数
    public class Identity_BindingTimes_Model
    {
        public int times { get; set; }
    }

    public class OUTIdentity_BindingTimes_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Identity_BindingTimes_Model data { get; set; }
    }
}