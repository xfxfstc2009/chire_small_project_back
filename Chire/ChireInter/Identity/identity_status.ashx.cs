﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Wechat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Identity
{
    /// <summary>
    /// identity_status 的摘要说明
    /// </summary>
    public class identity_status : IHttpHandler
    {
        HttpContext contextWithBase;
        public string code = "071tfUAN1ksVc91g6LAN1KW5BN1tfUAD";

        // 临时调用
        public int authType;
        public string openId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("authType"))
            {
                authType = Convert.ToInt32(contextWithBase.Request.Form["authType"]);
            }

            
            // 主方法
            mainManager();
        }
        #endregion

        #region main
        public void mainManager() {
            // 1. 获取我的openid
            Wechat_Action wechatAction = new Wechat_Action();
            openId = wechatAction.getOpenIdWithCode(code);
            // 2. 

            Identity_Action identityAction = new Identity_Action();

            Identity_Model identityModel;
            if (authType == -1)
            {
                identityModel = identityAction.getAuthType(openId);
            }
            else {
                identityModel = identityAction.getAuthType(openId);
            }
            
            successManager("200", "success", identityModel);
        }
        #endregion

        #region 1.插入验证信息
        public Identity_Model YanzhengInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from member_auth where openId = '" + openId + "' and auth = '" + authType + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Identity_Model info = new Identity_Model();
            if (dr.Read())
            {
                info.code = dr["code"].ToString();
                info.is_auth = Convert.ToInt32(dr["is_auth"].ToString());
            }
            sqlcon.Close();
            return info;
        }

        #endregion

        #region 执行方法
        public void successManager(string errcode, string err, Identity_Model identyModel)
        {
            OUTIdentity_Model out_base_setting = new OUTIdentity_Model();
            out_base_setting.errCode = errcode;
            out_base_setting.errMsg = err;

            out_base_setting.data = identyModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}