﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window.Contact
{
    /// <summary>
    /// Contace_List 的摘要说明
    /// </summary>
    public class Contace_List : IHttpHandler
    {
        HttpContext contextWithBase;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            getAllInfo();
        }
        #endregion

        public void getAllInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from contact";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Contace_Model> music_ablumList = new List<Contace_Model>();
            for (int i = 0; i < rows; i++)
            {

                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string address = dt.Rows[i]["address"].ToString();
                string link = dt.Rows[i]["link"].ToString();
                string title = dt.Rows[i]["title"].ToString();
                string des = dt.Rows[i]["des"].ToString();
                string createtime = dt.Rows[i]["createtime"].ToString();

                Contace_Model downloadModel = new Contace_Model() { 
                    id = id,name = name,address = address,link = link,
                    title = title,des = des,createtime = createtime,
                };
                music_ablumList.Add(downloadModel);
            }
            sqlcon.Close();
            successManager(music_ablumList);
        }

        #region 执行方法
        public void successManager(List<Contace_Model> model)
        {
            OutContace_Model out_base_setting = new OutContace_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Contace_List_Model listModel = new Contace_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}