﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window.Contact
{
    /// <summary>
    /// Window_Contact 的摘要说明
    /// </summary>
    public class Window_Contact : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "1";
        string address = "2";
        string link = "3";
        string title = "4";
        string des = "5";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("address"))
            {
                address = contextWithBase.Request.Form["address"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("link"))
            {
                link = contextWithBase.Request.Form["link"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("title"))
            {
                title = contextWithBase.Request.Form["title"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("des"))
            {
                des = contextWithBase.Request.Form["des"];
            }
            addInfoToServer();
        }
        #endregion

        #region 2.添加内容到数据库
        public void addInfoToServer()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into contact(name,address,link,title,des,createtime) values(@name,@address,@link,@title,@des,@createtime)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 名字
            cmd.Parameters.Add("@name", SqlDbType.NVarChar);
            cmd.Parameters["@name"].Value = name;
            // 地址
            cmd.Parameters.Add("@address", SqlDbType.NVarChar);
            cmd.Parameters["@address"].Value = address;

            // 联系方式
            cmd.Parameters.Add("@link", SqlDbType.NVarChar);
            cmd.Parameters["@link"].Value = link;

            // 联系方式
            cmd.Parameters.Add("@title", SqlDbType.NVarChar);
            cmd.Parameters["@title"].Value = title;

            // 联系方式
            cmd.Parameters.Add("@des", SqlDbType.NVarChar);
            cmd.Parameters["@des"].Value = des;

            cmd.Parameters.Add("@createtime", SqlDbType.VarChar, 50);
            cmd.Parameters["@createtime"].Value = DateTime.Now.ToString();

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 4.成功方法
        public void successManager()
        {
            OutContace_Model out_base_setting = new OutContace_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }

        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}