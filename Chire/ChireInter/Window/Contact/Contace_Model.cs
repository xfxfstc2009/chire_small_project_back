﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire
{
	public class Contace_Model
	{
        public string id {get;set;}
         public string name {get;set;}
         public string address {get;set;}
         public string link {get;set;}
         public string title {get;set;}
         public string des {get;set;}
         public string createtime {get;set;}
	}

    public class Contace_List_Model
    {
        public List<Contace_Model> list { get; set; }
    }

    public class OutContace_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Contace_List_Model data { get; set; }
    }
}