﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Exam;
using Chire.ChireInter.Students;
using Chire.ChireInter.Teacher;
using Chire.ChireInter.Window.Home;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window
{
    /// <summary>
    /// Window_Home 的摘要说明
    /// </summary>
    public class Window_Home : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            getHomeInfo();
        }
        #endregion


        #region 获取首页信息
        public void getHomeInfo() { 
            // 1. 获取所有学生
            Student_Action studentAction = new Student_Action();
            List<Student_Model> studentList = studentAction.getAllStudent("1", "");

            // 2. 获取平均分数
            Exam_Action examAction = new Exam_Action();
            double score = examAction.getScoreAvg();

            // 3. 获取授课课程
            Teacher_Action teacherAction = new Teacher_Action();
            List<Teacher_Model> teacherList = teacherAction.getAllTeacher();


            Home_Model homeModel = new Home_Model();
            homeModel.studentCount = studentList.Count;
            homeModel.scoreCount = score;
            homeModel.teacherCount = teacherList.Count;
            homeModel.classCount = 100;
            successManager(homeModel);
        }
        #endregion

        #region 执行方法
        public void successManager(Home_Model homeModel)
        {
            OUTHome_Model out_base_setting = new OUTHome_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = homeModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}