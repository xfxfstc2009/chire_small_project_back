﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Window.Home
{
    public class Home_Model
    {
        public int studentCount{get;set;}
        public double scoreCount{get;set;}
        public int classCount { get; set; }
        public int teacherCount { get; set; }

    }

    public class OUTHome_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Home_Model data { get; set; }
    }
}