﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Window.View
{
    public class Window_Root_Img
    {
        public string pc_home_bg { get; set; }
        public string pc_home_bottom_bg { get; set; }
        public string pc_class_bg { get; set; }
        public string pc_class_bottom_bg { get; set; }
        public string pc_contact_bg { get; set; }
        public string pc_contact_bottom_bg { get; set; }
        public string pc_pricing_bg { get; set; }
        public string pc_pricing_bottom_bg { get; set; }
  
    }


    public class OutWindow_Root_Img
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Window_Root_Img data { get; set; }
    }


    public class Window_List_Single {
        public string id { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public string datetime { get; set; }
    }
    public class Window_List
    {
        public List<Window_List_Single> list { get; set; }
    }

    public class OUTWindow_List {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Window_List data { get; set; }
    }

}