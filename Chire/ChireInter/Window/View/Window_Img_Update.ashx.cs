﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window.View
{
    /// <summary>
    /// Window_Img_Update 的摘要说明
    /// </summary>
    public class Window_Img_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string title = "";
        string url = "";
     
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("title"))
            {
                title = contextWithBase.Request.Form["title"];
            }
       
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }


            updateMainManager();
        }
        #endregion

        #region 修改方法
        public void updateMainManager() {
            Window_Action windowAction = new Window_Action();
            windowAction.windowChangeImg(title, url);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OutWindow_Root_Img out_base_setting = new OutWindow_Root_Img();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}