﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window.View
{
    /// <summary>
    /// Window_Img_List 的摘要说明
    /// </summary>
    public class Window_Img_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

         #region 验证信息
        public void analysisInfo()
        {
            getListManager();
        }
        #endregion

        #region 获取列表方法
        public void getListManager() {
            Window_Action windowAction = new Window_Action();
            List<Window_List_Single> list = windowAction.windowGetAllImgList();
            successManager(list);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Window_List_Single> list)
        {
            OUTWindow_List out_base_setting = new OUTWindow_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Window_List windowList = new Window_List();
            windowList.list = list;
            out_base_setting.data = windowList;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}