﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window.View
{
    /// <summary>
    /// Window_View_Img 的摘要说明
    /// </summary>
    public class Window_View_Img : IHttpHandler
    {
        HttpContext contextWithBase;
        string type = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 执行操作
        public void analysisInfo() {

            if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }

            mainManager();
       
        }
        #endregion

        #region 主方法
        public void mainManager() {
            Window_Action windowAction = new Window_Action();
            List<Window_List_Single> list = windowAction.windowGetAllImgList();
            Window_Root_Img rootImg = new Window_Root_Img();
            for (int i = 0; i < list.Count; i++) {
                Window_List_Single model = list[i];
                if (model.title.Equals("pc_home_bg") == true) {     // 首页背景
                    rootImg.pc_home_bg = Constance.imgBaseUrl +"/"+ model.url;
                }
                else if (model.title.Equals("pc_home_bottom_bg") == true)       // 首页底部
                {     // 首页底部背景
                    rootImg.pc_home_bottom_bg = Constance.imgBaseUrl + "/" + model.url;
                }
                else if (model.title.Equals("pc_class_bg") == true)
                {     // 课程背景
                    rootImg.pc_class_bg = Constance.imgBaseUrl + "/" + model.url;
                }
                else if (model.title.Equals("pc_class_bottom_bg") == true)
                {     // 课程底部背景
                    rootImg.pc_class_bottom_bg = Constance.imgBaseUrl + "/" + model.url;
                }
                else if (model.title.Equals("pc_contact_bg") == true)
                {     // 课程底部背景
                    rootImg.pc_contact_bg = Constance.imgBaseUrl + "/" + model.url;
                }
                else if (model.title.Equals("pc_contact_bottom_bg") == true)
                {     // 课程底部背景
                    rootImg.pc_contact_bottom_bg = Constance.imgBaseUrl + "/" + model.url;
                }
                else if (model.title.Equals("pc_pricing_bg") == true)
                {     // 课程底部背景
                    rootImg.pc_pricing_bg = Constance.imgBaseUrl + "/" + model.url;
                }
                else if (model.title.Equals("pc_pricing_bottom_bg") == true)
                {     // 课程底部背景
                    rootImg.pc_pricing_bottom_bg = Constance.imgBaseUrl + "/" + model.url;
                }  
            }

            successManager(rootImg);
         
        }
        #endregion

        #region 执行方法
        public void successManager(Window_Root_Img rootImg)
        {
            OutWindow_Root_Img out_base_setting = new OutWindow_Root_Img();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
        
            out_base_setting.data = rootImg;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
        
    }
}