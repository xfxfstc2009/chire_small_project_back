﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Window.View
{
    /// <summary>
    /// Window_Img_Add 的摘要说明
    /// </summary>
    public class Window_Img_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string title = "1";
        string name = "2";
        string url = "3";
        string type = "4";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("title"))
            {
                title = contextWithBase.Request.Form["title"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }

            insertInfo();
        }
        #endregion

        #region 插入信息
        public void insertInfo()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into window_img_setting(title,name,url,type,datetime) values(@title,@name,@url,@type,@datetime)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@title", SqlDbType.VarChar, 50);
            cmd.Parameters["@title"].Value = title;

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 1000);
            cmd.Parameters["@name"].Value = name;
            // 2.日记详情
            cmd.Parameters.Add("@url", SqlDbType.VarChar, 1000);
            cmd.Parameters["@url"].Value = url;

            cmd.Parameters.Add("@type", SqlDbType.VarChar, 1000);
            cmd.Parameters["@type"].Value = type;

            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString();


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OutWindow_Root_Img out_base_setting = new OutWindow_Root_Img();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}