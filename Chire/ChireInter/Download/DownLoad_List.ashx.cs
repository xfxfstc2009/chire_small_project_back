﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Download
{
    /// <summary>
    /// DownLoad_List1 的摘要说明
    /// </summary>
    public class DownLoad_List1 : IHttpHandler
    {
        HttpContext contextWithBase;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
   
            getAllInfo();
        }
        #endregion

        #region 获取所有下载列表
        public void getAllInfo()
        {
            DownLoad_Action downloadAction = new DownLoad_Action();
            List<DownLoad_Single_Model> downloadList = downloadAction.getAllDownLoadInfo();
            successManager(downloadList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<DownLoad_Single_Model> model)
        {
            OutDownLoad_List out_base_setting = new OutDownLoad_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            DownLoad_List listModel = new DownLoad_List();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
  
    }
}