﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Download
{
    public class DownLoad_Request_Model {
        public string name { get; set; }
        public string info { get; set; }
        public string url { get; set; }
        public string ablum { get; set; }
    }

     public class DownLoad_Single_Model
    {
         public string id { get; set; }
         public string name { get; set; }
         public string url { get; set; }
         public string datetime { get; set; }
         public string info { get; set; }
         public string ablum { get; set; }
         
    }

    public class DownLoad_List
    {
        public List<DownLoad_Single_Model> list;
    }


    public class OutDownLoad_List
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public DownLoad_List data { get; set; }
    }
}