﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Download
{
    /// <summary>
    /// DownLoad_Add 的摘要说明
    /// </summary>
    public class DownLoad_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string info = "";
        string url = "";
        string ablum = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("info"))
            {
                info = contextWithBase.Request.Form["info"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("ablum"))
            {
                ablum = contextWithBase.Request.Form["ablum"];
            }

            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            DownLoad_Request_Model requestModel = new DownLoad_Request_Model();
            requestModel.name = name;
            requestModel.url = url;
            requestModel.info = info;
            requestModel.ablum = ablum;
            DownLoad_Action downLoadAction = new DownLoad_Action();
            downLoadAction.downloadAddManager(requestModel);

            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OutDownLoad_List out_base_setting = new OutDownLoad_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}