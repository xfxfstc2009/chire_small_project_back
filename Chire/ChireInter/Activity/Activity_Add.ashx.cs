﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Activity
{
    /// <summary>
    /// Activity_Add 的摘要说明
    /// </summary>
    public class Activity_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string title = "";
        string htmls = "";
        string imgs = "";       

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();


        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("title"))
            {
                title = contextWithBase.Request.Form["title"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("html"))
            {
                htmls = contextWithBase.Request.Form["html"];
                htmls =HttpUtility.UrlDecode(htmls);
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("imgs"))
            {
                imgs = contextWithBase.Request.Form["imgs"];
            }
   
            insertIntoActivity();
        }
        #endregion

        private void insertIntoActivity() {
            Activity_Action activityAction = new Activity_Action();
            activityAction.insertActivity(title, htmls, imgs);
            successManager();
        }

        #region 成功方法
        public void successManager()
        {
            OUTActivity_List_Model out_base_setting = new OUTActivity_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}