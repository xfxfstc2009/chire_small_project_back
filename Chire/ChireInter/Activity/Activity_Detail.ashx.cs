﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Activity
{
    /// <summary>
    /// Activity_Detail 的摘要说明
    /// </summary>
    public class Activity_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }

            getInfoWithActivity();
        }
        #endregion

        #region 获取数据
        public void getInfoWithActivity()
        {
            Activity_Action activityAction = new Activity_Action();
            Activity_Model activityModel = activityAction.getactivityDetailWithId(id);

            successManager(activityModel);
        }
        #endregion


        #region 成功方法
        public void successManager(Activity_Model activityModel)
        {
            OUTActivity_Model out_base_setting = new OUTActivity_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = activityModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}