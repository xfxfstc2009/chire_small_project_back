﻿using Chire.wechat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Activity
{
    public class Activity_Action
    {
        #region 获取所有活动
        public List<Activity_Model> getAllActivityListManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from activity order by id desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;
      
            List<Activity_Model> activityList = new List<Activity_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string title = dt.Rows[i]["title"].ToString();
                string des = dt.Rows[i]["des"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();

                long timeInterval = Constance.Instance.ConvertDateTimeInt(Convert.ToDateTime(datetime));

                string link_id = dt.Rows[i]["link_id"].ToString();
                List<string> imgList = getActivityImgList(link_id);
                Activity_Model activityModel = new Activity_Model()
                {
                    id = link_id,
                    title = title,
                    des = des,
                    timeInter = timeInterval,
                    imgList = imgList,
                };
                activityList.Add(activityModel);
            }
            sqlcon.Close();
            return activityList;
        }
        #endregion

        #region 获取活动内的图片
        private List<string> getActivityImgList(string linkId) {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from activity_sub where link_id = '"+linkId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> activityList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string img = dt.Rows[i]["img"].ToString();
              
                activityList.Add(img);
            }
            sqlcon.Close();
            return activityList;
        }
        #endregion

        #region 插入活动
        public void insertActivity(string title,string des,string imgs){
            string linkId = Constance.Instance.getRandomStr(10);

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into activity(title,des,datetime,link_id) values(@title,@des,@datetime,@link_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@title", SqlDbType.VarChar, 50);
            cmd.Parameters["@title"].Value = title;

            cmd.Parameters.Add("@des", SqlDbType.VarChar, 10000);
            cmd.Parameters["@des"].Value = des;

            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 50);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@link_id", SqlDbType.VarChar, 20);
            cmd.Parameters["@link_id"].Value = linkId;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();

            // 插入图片
            string[] imgsList = imgs.Split(',');
            List<string> imgsCountList = imgsList.ToList<string>();
            for (int i = 0; i < imgsCountList.Count; i++)
            {
                string info = imgsCountList[i];
                activityInsertImgList(info, linkId);
            }
        }
        #endregion

        #region 插入多张图片
        private void activityInsertImgList(string img,string linkId) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into activity_sub(img,link_id) values(@img,@link_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@img", SqlDbType.VarChar, 250);
            cmd.Parameters["@img"].Value = img;

            cmd.Parameters.Add("@link_id", SqlDbType.VarChar, 100);
            cmd.Parameters["@link_id"].Value = linkId;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 删除所有活动
        public void deleteActivityWithId(string id) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from activity where link_id = '" + id + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 获取活动信息
        public Activity_Model getactivityDetailWithId(string avtivityId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from activity where link_id = '" + avtivityId + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Activity_Model activtyModel = new Activity_Model();
            if (dr.Read())
            {
             
                activtyModel.id = avtivityId;
                activtyModel.title = dr["title"].ToString();
                activtyModel.des = dr["des"].ToString();
                activtyModel.datetime = dr["datetime"].ToString();
                activtyModel.timeInter = Constance.Instance.ConvertDateTimeInt(Convert.ToDateTime(activtyModel.datetime));
                string linkId = dr["link_id"].ToString();
                activtyModel.imgList = getActivityImgList(linkId);

            }
            sqlcon.Close();
            return activtyModel;
        }
        #endregion
    }
}