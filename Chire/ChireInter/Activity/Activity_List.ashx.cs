﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Activity
{
    /// <summary>
    /// Activity_List 的摘要说明
    /// </summary>
    public class Activity_List : IHttpHandler
    {
        HttpContext contextWithBase;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            getAllInfo();
        }
        #endregion

        #region 获取所有课程信息
        public void getAllInfo()
        {
            Activity_Action activityAction = new Activity_Action();
            List<Activity_Model> activityList = activityAction.getAllActivityListManager();
            successManager(activityList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Activity_Model> activityList)
        {
            OUTActivity_List_Model out_base_setting = new OUTActivity_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Activity_List_Model listModel = new Activity_List_Model();
            listModel.list = activityList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}