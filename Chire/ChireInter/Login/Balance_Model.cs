﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Login
{
    public class Balance_Model
    {
        public int balance { get; set; }                      // 余额
        // temp
        public bool has { get; set; }                      // 是否创建

    }

    public class OutBalance_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Balance_Model data { get; set; }
    }

}