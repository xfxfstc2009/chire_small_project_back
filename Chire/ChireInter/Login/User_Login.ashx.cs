﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Wechat.Model;
using Chire.wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Login
{
    /// <summary>
    /// User_Login 的摘要说明
    /// </summary>
    public class User_Login : IHttpHandler
    {
        HttpContext contextWithBase;
        string avatar = "";             // 头像
        string nick = "";               // 名称
        int sex = 1;                    // 性别 1男 2女
        string birthday = "";           // 生日
        string phone = "";              // 手机号码
        string device = "";             // 设备号
        string union_id = "";           // 唯一号
        string open_id = "";            // 唯一号
        string spCode = "";             // 小程序使用的code临时使用

        Chire.ChireInter.Login.Login_Action.LoginType login_type = Chire.ChireInter.Login.Login_Action.LoginType.loginType_phone;


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;

            analysisInfo();
        

        }

        #region 验证信息
        public void analysisInfo()
        {
            // 头像
            if (contextWithBase.Request.Form.AllKeys.Contains("avatar"))
            {
                avatar = contextWithBase.Request.Form["avatar"];
                if (avatar.Length <= 0)
                {
                    avatar = "http://bee-tv.oss-cn-hangzhou.aliyuncs.com/Main/noneavatar.png";
                }
            }

            // 昵称
            if (contextWithBase.Request.Form.AllKeys.Contains("nick"))
            {
                nick = contextWithBase.Request.Form["nick"];
            }
            // 性别
            if (contextWithBase.Request.Form.AllKeys.Contains("sex"))
            {
                sex = Convert.ToInt32(contextWithBase.Request.Form["sex"]);
            }
            // 年龄        
            if (contextWithBase.Request.Form.AllKeys.Contains("birthday"))
            {
                birthday = contextWithBase.Request.Form["birthday"];
            }
            // 手机号
            if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
            {
                phone = contextWithBase.Request.Form["phone"];
            }
            // 登录类型
            if (contextWithBase.Request.Form.AllKeys.Contains("login_type"))
            {
                login_type = (Chire.ChireInter.Login.Login_Action.LoginType)Convert.ToInt32(contextWithBase.Request.Form["login_type"]);
            }
            // 设备号
            if (contextWithBase.Request.Form.AllKeys.Contains("device"))
            {
                device = contextWithBase.Request.Form["device"];
            }
            // 唯一号
            if (contextWithBase.Request.Form.AllKeys.Contains("union_id"))
            {
                union_id = contextWithBase.Request.Form["union_id"];
            }

            // 唯一号
            if (contextWithBase.Request.Form.AllKeys.Contains("open_id"))
            {
                open_id = contextWithBase.Request.Form["open_id"];
            }

            // 小程序使用的code
            if (contextWithBase.Request.Form.AllKeys.Contains("spCode"))
            {
                spCode = contextWithBase.Request.Form["spCode"];
            }
        
            //  执行业务逻辑
            actionLoginManager();
        }
        #endregion

        #region 执行方法
        public void actionLoginManager()
        {
            // 1. 微信小程序进行登陆
            string openId = "";
            if (spCode.Length > 0)
            {
                Wechat_AccessToken wechatAccessToken = new Wechat_AccessToken();
                Wechat_SmallProject_Model mainInfo = wechatAccessToken.getcode2SessionManager(spCode);
                openId = mainInfo.openid;
            }
            else {
                openId = open_id;
            }

            // 2. 登陆方法
            Login_Action loginAction = new Login_Action();
  
            User_Model userModel = loginAction.mainLoginManager(User_Type.User_TypeNormal, nick, sex, birthday,
                phone, login_type, union_id, openId, avatar);

            successManager(userModel);
        }
        #endregion

        #region 执行方法
        public void successManager(User_Model userModel)
        {
            OutUser_Model out_base_setting = new OutUser_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";

            out_base_setting.data = userModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}