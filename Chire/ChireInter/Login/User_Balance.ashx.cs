﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Login
{
    /// <summary>
    /// User_Balance 的摘要说明
    /// </summary>
    public class User_Balance : IHttpHandler
    {
        HttpContext contextWithBase;
        string openid = "";             // openid
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }

         #region 验证信息
        public void analysisInfo()
        {
            // openid
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            mainManager();

        }
          #endregion


        #region 主方法
        public void mainManager() {
            Balance_Model balanceModel = new Balance_Model();

            // action 
            Balance_Model tempBalanceModel = searchBalanceInfoManager();
            if (tempBalanceModel.has)
            {
                // 1. 进行同步数据
                tongbuMangaer();
                // 2. 进行抛出
                successManager(tempBalanceModel);
            }
            else
            {
                insertInfoManager();
            }
        }
        #endregion

        #region 搜索balance表查看balance
        public Balance_Model searchBalanceInfoManager()
        {
            Balance_Model balanceModel = new Balance_Model();
            balanceModel.has = false;
            balanceModel.balance = 0;
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "";
            if (openid.Length > 0)
            {

                strselect = "select top 1 * from balance where open_id = '" + openid + "'";
            }
           
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            if (dr.Read())
            {
                balanceModel.balance = Convert.ToInt32(dr["balance"].ToString()); ;
                balanceModel.has = true;
            }
            sqlcon.Close();
            return balanceModel;
        }
        #endregion

        #region 没有的话插入
        public void insertInfoManager() {
            Balance_Model balanceModel = new Balance_Model();
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into balance(open_id,balance) values(@open_id,@balance)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@open_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@open_id"].Value = openid;
        

            cmd.Parameters.Add("@balance", SqlDbType.VarChar, 350);
            cmd.Parameters["@balance"].Value = 0;
            balanceModel.balance = 0; ;
            balanceModel.has = false;
        
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager(balanceModel);
        }
        
        #endregion


        #region 同步该学生的账户
        public void tongbuMangaer() { 
         //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = " update  user_list set [money]  = (select balance from balance where open_id = '" + openid + "') where open_id = '" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
       
        }
        #endregion

        #region 成功方法
        public void successManager(Balance_Model balanceModel)
        {
            OutBalance_Model out_base_setting = new OutBalance_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = balanceModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}