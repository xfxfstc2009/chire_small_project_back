﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Login
{
    public class User_Action
    {
        #region 获取所有学生
        public List<User_Model> getAllUser()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();


            string strselect = "select * from user_list where has_renzhen = '1'";
           

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<User_Model> music_ablumList = new List<User_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string open_id = dt.Rows[i]["open_id"].ToString();
                
                User_Model downloadModel = new User_Model()
                {
                    id = id,
                    open_id = open_id,
                 
                };
                music_ablumList.Add(downloadModel);
            }
            sqlcon.Close();
            return music_ablumList;

        }
        #endregion
    }
}