﻿using Chire.ChireInter.Teacher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Login
{
    public class User_Model
    {
        public string id { get; set; }                      // id 
        public string avatar { get; set; }                  // 头像
        public string nick { get; set; }                    // 昵称
        public int sex { get; set; }                        // 性别
        public string birthday { get; set; }                // 年龄
        public string phone { get; set; }                   // 手机
        public int login_type { get; set; }                 // 登录类型
        public string device { get; set; }                  // 设备号
        public string union_id { get; set; }                // qq登录的唯一号
        public string user_sig { get; set; }                // 用户sig
        public int money { get; set; }                      // 钱
        public string im_id { get; set; }                   // im_id
        public string open_id { get; set; }                 // 微信open_id
        public string zhifupay { get; set; }                // 支付宝
        public User_Type user_type { get; set; }            // 用户类型

        public Teacher_Model teacherInfo { get; set; }      // 老师信息
        public Student_Model studentInfo { get; set; }      // 学生信息
        public string has_renzhen { get; set; }                 // 是否认证
    }

    public enum User_Type
    {
        User_TypeNormal = 0,             // 普通人
        User_TypeTeacher = 1,            // 老师
        User_TypeStudent = 2,            // 学生
        User_TypeParent = 3,             // 家长
        User_TypeAdmin = 4,             // 管理员
      
    }
    public class User_List_Model {
        public List<User_Model> list { get; set; }                // 支付宝
        public int page { get; set; }
        public int size { get; set; }
        public int total { get; set; }
       
    }

    public class OutUser_Server_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public User_List_Model data { get; set; }
    }



    public class OutUser_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public User_Model data { get; set; }
    }
}