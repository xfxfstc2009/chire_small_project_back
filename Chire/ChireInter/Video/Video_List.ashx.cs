﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Video
{
    /// <summary>
    /// Video_List 的摘要说明
    /// </summary>
    public class Video_List : IHttpHandler
    {

        HttpContext contextWithBase;
        int page = 1;
        int size = 10;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            if (contextWithBase.Request.Form.AllKeys.Contains("page"))
            {
                page = Convert.ToInt32(contextWithBase.Request.Form["page"]);
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("size"))
            {
                size = Convert.ToInt32(contextWithBase.Request.Form["size"]);
            }

            getAllInfo();
        }
        #endregion

        #region 获取所有科目
        public void getAllInfo()
        {
            Video_Action videoAction = new Video_Action();
            List<Video_Model> subjectList = videoAction.getAllTool(page,size);
            successManager(subjectList);

        }
        #endregion

        #region 执行方法
        public void successManager(List<Video_Model> subjectList)
        {
            OUTVideo_List_Model out_base_setting = new OUTVideo_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Video_List_Model listModel = new Video_List_Model();
            listModel.list = subjectList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}