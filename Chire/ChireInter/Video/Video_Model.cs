﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Video
{

    public class Video_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string ablum { get; set; }
        public string video { get; set; }
        public string author { get; set; }
        public string price { get; set; }
        public string create_time { get; set; }
    }

    public class Video_List_Model
    {
        public List<Video_Model> list;
    }

    public class OUTVideo_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Video_List_Model data { get; set; }
    }


    public class OUTVideo_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Video_Model data { get; set; }
    }
}