﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Video
{
    public class Video_Action
    {
        #region 获取所有工具
        public List<Video_Model> getAllTool(int page,int size)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            int pageWithIndex = ((page - 1) * size) + 1;
            int maxPageIndex = pageWithIndex + size - 1;
            string strselect = "select a.* from(select row_number() over (order by id desc) as rn,* from video) a where  rn between '" + pageWithIndex + "' and '" + maxPageIndex + "'";

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Video_Model> subjectList = new List<Video_Model>();
            for (int i = 0; i < rows; i++)
            {
                 
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string ablum = dt.Rows[i]["ablum"].ToString();
                string video = dt.Rows[i]["video"].ToString();
                string author = dt.Rows[i]["author"].ToString();
                string price = dt.Rows[i]["price"].ToString();
                string create_time = dt.Rows[i]["create_time"].ToString();
          

                Video_Model subjectModel = new Video_Model()
                {
                    id = id,
                    name = name,
                    ablum = ablum,
                    video = video,
                    author = author,

                    price = price,

                    create_time = create_time,
                };
                subjectList.Add(subjectModel);
            }
            sqlcon.Close();
            return subjectList;
        }
        #endregion

    }
}