﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_Update 的摘要说明
    /// </summary>
    public class Teacher_Update : IHttpHandler
    {
        Teacher_Model mainTeacherModel = new Teacher_Model();
        HttpContext contextWithBase;
        string openid = "";                 // openId
        string avatar = "";                 // 1. 头像
        string name = "";                   // 2. 真实姓名
        string gender = "";                 // 3. 性别
        string birthday = "";               // 4. 生日
        string phone = "";                  // 5. 手机号
        string school = "";                 // 6. 毕业院校
        string diploma = "";                // 7. 学历
        string remark = "";                 // 8. 签名
        string lifephotos = "";             // 9. 生活照
        string certphotos = "";             // 10.证书
        List<string> lifephotoArr;
        List<string> certphotoArr;


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("avatar"))
            {
                avatar = contextWithBase.Request.Form["avatar"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("gender"))
            {
                gender = contextWithBase.Request.Form["gender"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("birthday"))
            {
                birthday = contextWithBase.Request.Form["birthday"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
            {
                phone = contextWithBase.Request.Form["phone"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("school"))
            {
                school = contextWithBase.Request.Form["school"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("diploma"))
            {
                diploma = contextWithBase.Request.Form["diploma"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("remark"))
            {
                remark = contextWithBase.Request.Form["remark"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("lifephotos"))
            {
                lifephotos = contextWithBase.Request.Form["lifephotos"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("certphotos"))
            {
                certphotos = contextWithBase.Request.Form["certphotos"];
            }

       
            hasTeacherManager();
        }
        #endregion

        #region 判断是否有老师存在
        private void hasTeacherManager()
        {
            Teacher_Action teacherAction = new Teacher_Action();
            Teacher_Model teacherModel = teacherAction.getTeacherWithId(openid);
            if (teacherModel.id != null)
            {   // 表示有老师
                teacherAction.updateTeacherInfo(name, avatar, gender, phone, school, diploma, remark, lifephotos, certphotos, openid);
            }
            successManager();
        }
        #endregion



        #region 成功方法
        public void successManager()
        {
            OUTTeacher_Model out_base_setting = new OUTTeacher_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}