﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Login;
using Chire.ChireInter.Students;
using Newtonsoft.Json;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_Add 的摘要说明
    /// </summary>
    public class Teacher_Add : IHttpHandler
    {
        Teacher_Model mainTeacherModel = new Teacher_Model();
        HttpContext contextWithBase;
        string openid = "";                 // openId
        string avatar = "";                 // 1. 头像
        string name = "";                   // 2. 真实姓名
        string gender = "";                 // 3. 性别
        string birthday = "";               // 4. 生日
        string phone = "";                  // 5. 手机号
        string school = "";                 // 6. 毕业院校
        string diploma = "";                // 7. 学历
        string remark = "";                 // 8. 签名
        string lifephotos = "";             // 9. 生活照
        string certphotos = "";             // 10.证书
        List<string> lifephotoArr;
        List<string> certphotoArr;


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("avatar"))
            {
                avatar = contextWithBase.Request.Form["avatar"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("gender"))
            {
                gender = contextWithBase.Request.Form["gender"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("birthday"))
            {
                birthday = contextWithBase.Request.Form["birthday"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
            {
                phone = contextWithBase.Request.Form["phone"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("school"))
            {
                school = contextWithBase.Request.Form["school"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("diploma"))
            {
                diploma = contextWithBase.Request.Form["diploma"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("remark"))
            {
                remark = contextWithBase.Request.Form["remark"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("lifephotos"))
            {
                lifephotos = contextWithBase.Request.Form["lifephotos"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("certphotos"))
            {
                certphotos = contextWithBase.Request.Form["certphotos"];
            }

            hasTeacherManager();
        }
        #endregion

        #region 判断是否有老师存在
        private void hasTeacherManager() {
            Teacher_Action teacherAction = new Teacher_Action();
            Teacher_Model teacherModel = teacherAction.getTeacherWithId(openid);
            if (teacherModel.id!= null)
            {   // 表示有老师

                mainTeacherModel = teacherModel;
                successManager();
            }
            else {
                // 1. 插入老师照片
                insertTeacherPhoto();
                // 2. 插入老师信息
                insertTeacherInfo();
            }
        }
        #endregion


        #region 插入老师信息
        public void insertTeacherInfo()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into teacher(birthday,banner,id,name,avatar,gender,phone,school,diploma,remark,openid,authStatus,datetime,has_del) values(@birthday,@banner,@id,@name,@avatar,@gender,@phone,@school,@diploma,@remark,@openid,@authStatus,@datetime,@has_del)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@id", SqlDbType.VarChar, 50);
            cmd.Parameters["@id"].Value = openid;
            mainTeacherModel.id = openid;

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            mainTeacherModel.name = name;

            cmd.Parameters.Add("@birthday", SqlDbType.VarChar, 50);
            cmd.Parameters["@birthday"].Value = birthday;
            mainTeacherModel.birthday = birthday;

            cmd.Parameters.Add("@avatar", SqlDbType.VarChar, 1000);
            cmd.Parameters["@avatar"].Value = avatar;
            mainTeacherModel.avatar = avatar;

            cmd.Parameters.Add("@gender", SqlDbType.VarChar, 1000);
            cmd.Parameters["@gender"].Value = gender;
            mainTeacherModel.gender = gender;

            cmd.Parameters.Add("@phone", SqlDbType.VarChar, 1000);
            cmd.Parameters["@phone"].Value = phone;
            mainTeacherModel.phone = phone;

            cmd.Parameters.Add("@school", SqlDbType.VarChar, 1000);
            cmd.Parameters["@school"].Value = school;
            mainTeacherModel.school = school;

            cmd.Parameters.Add("@diploma", SqlDbType.VarChar, 1000);
            cmd.Parameters["@diploma"].Value = diploma;
            mainTeacherModel.diploma = diploma;

            cmd.Parameters.Add("@remark", SqlDbType.VarChar, 1000);
            cmd.Parameters["@remark"].Value = remark;
            mainTeacherModel.remark = remark;

            cmd.Parameters.Add("@openid", SqlDbType.VarChar, 1000);
            cmd.Parameters["@openid"].Value = openid;
            mainTeacherModel.openid = openid;

            cmd.Parameters.Add("@authStatus", SqlDbType.Int);
            cmd.Parameters["@authStatus"].Value = 0;
            mainTeacherModel.status = 0;

            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@has_del", SqlDbType.VarChar, 1000);
            cmd.Parameters["@has_del"].Value = 0;
            mainTeacherModel.has_del = 0;

            cmd.Parameters.Add("@banner", SqlDbType.VarChar, 1000);

            string bannerTemp = "";
            if (lifephotoArr.Count > 0) {
                bannerTemp = lifephotoArr[0];
            }
            cmd.Parameters["@banner"].Value = bannerTemp; 

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            updateUserType();
            successManager();
        }
        #endregion

        #region 插入老师图片
        private void insertTeacherPhoto(){
            Teacher_Action teacherAction = new Teacher_Action();
            if (lifephotos.Length > 0)
            {
                string[] tempArr = lifephotos.Split(',');
                lifephotoArr = new List<string>(tempArr);
                mainTeacherModel.lifephotoArr = lifephotoArr;
                teacherAction.addTeacherPhoto(openid, lifephotos);
            }
            if (certphotos.Length > 0) {
                string[] tempArr = certphotos.Split(',');
                certphotoArr = new List<string>(tempArr);
                mainTeacherModel.certPhotoArr = certphotoArr;
                teacherAction.addTeacherCertImg(openid, lifephotos);
            }
   

        }
        #endregion

        #region 修改用户状态为老师
        private void updateUserType()
        {
            Login_Action login_Action = new Login_Action();
            login_Action.updateUserInfoWithopenId(openid, "user_type", "1");

        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTTeacher_Model out_base_setting = new OUTTeacher_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = mainTeacherModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}