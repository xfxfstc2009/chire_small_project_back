﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_Apply 的摘要说明
    /// </summary>
    public class Teacher_Apply : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "e44495a3bdc2a844d9f3c9bb93ab9269";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

          #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
         
           
            applyManager();
        }
        #endregion

        #region 进行申请
        public void applyManager()
        {
            Teacher_Action teacherAction = new Teacher_Action();
            bool hasSuccessed = teacherAction.applyToTeacher(id);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTTeacher_Model out_base_setting = new OUTTeacher_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion



       
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }      

      
    }
}