﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_Edit 的摘要说明
    /// </summary>
    public class Teacher_Edit : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        string name = "";
        string avatar = "";
        string gender = "";
        string phone = "";
        string school = "";
        string diploma = "";
        string remark = "";
        string birthday = "";
        int status = -1;                // 表示是否认证老师
        string banner = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("gender"))
            {
                gender = contextWithBase.Request.Form["gender"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("birthday"))
            {
                birthday = contextWithBase.Request.Form["birthday"];
            }
            
            if (contextWithBase.Request.Form.AllKeys.Contains("avatar"))
            {
                avatar = contextWithBase.Request.Form["avatar"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
            {
                phone = contextWithBase.Request.Form["phone"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("school"))
            {
                school = contextWithBase.Request.Form["school"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("diploma"))
            {
                diploma = contextWithBase.Request.Form["diploma"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("remark"))
            {
                remark = contextWithBase.Request.Form["remark"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("banner"))
            {
                banner = contextWithBase.Request.Form["banner"];
            }
            updateInfoManager();
        }
        #endregion


        #region 修改信息
        public void updateInfoManager (){
        
            Teacher_Action teacherAction = new Teacher_Action();
            if (name.Length > 0){
                teacherAction.updateTeacherInfo(id, "name", name);
            } else if (avatar.Length > 0){
                teacherAction.updateTeacherInfo(id, "avatar", avatar);
            } else if (gender.Length > 0){
                teacherAction.updateTeacherInfo(id, "gender", gender);
            } else if (phone.Length > 0){
                teacherAction.updateTeacherInfo(id, "phone", phone);
            } else if (school.Length > 0){
                teacherAction.updateTeacherInfo(id, "school", school);
            } else if (diploma.Length > 0){
                teacherAction.updateTeacherInfo(id, "diploma", diploma);
            } else if (remark.Length > 0){
                teacherAction.updateTeacherInfo(id, "remark", remark);
            }
            else if (birthday.Length > 0) {
                teacherAction.updateTeacherInfo(id, "birthday", birthday);
            }
            else if (banner.Length > 0) {
                teacherAction.updateTeacherInfo(id, "banner", banner);
            }
            successManager();
        }
        
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTTeacher_Model out_base_setting = new OUTTeacher_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
     
    }
}