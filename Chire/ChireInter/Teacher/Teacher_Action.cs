﻿using Chire.ChireInter.Login;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.wechat;

namespace Chire.ChireInter.Teacher
{


    public class Teacher_Action
    {
        #region 根据老师id获取老师信息
        public Teacher_Model getTeacherWithId(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from teacher where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Teacher_Model teacherModel = new Teacher_Model();
            if (dr.Read())
            {
                teacherModel.id = dr["id"].ToString();
                teacherModel.name = dr["name"].ToString();
                teacherModel.banner = dr["banner"].ToString();
                teacherModel.birthday = dr["birthday"].ToString();
                teacherModel.avatar = dr["avatar"].ToString();
                teacherModel.gender = dr["gender"].ToString();
                teacherModel.phone = dr["phone"].ToString();
                teacherModel.school = dr["school"].ToString();
                teacherModel.diploma = dr["diploma"].ToString();
                teacherModel.remark = dr["remark"].ToString();
                teacherModel.has_del = Convert.ToInt32(dr["has_del"].ToString());
                int statusInt = Convert.ToInt32(dr["authStatus"].ToString());
                Chire.ChireInter.Login.Login_Action.User_Status status = (Chire.ChireInter.Login.Login_Action.User_Status)statusInt;

                teacherModel.status = status;

                teacherModel.lifephotoArr = getTeacherLifePhotoList(id);        // 生活照片
                teacherModel.certPhotoArr = getTeacherImgs(id);                 // 获取资历图片
                teacherModel.seniorityArr = getTeacherSeniority(id);            // 资历

            }
            sqlcon.Close();
            return teacherModel;
        }
        #endregion

        #region 根据老师openId获取老师信息
        public Teacher_Model getTeacherWithopenId(string openId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from teacher where openid = '" + openId + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Teacher_Model teacherModel = new Teacher_Model();
            if (dr.Read())
            {
                teacherModel.id = dr["id"].ToString();
                teacherModel.name = dr["name"].ToString();
                teacherModel.banner = dr["banner"].ToString();
                teacherModel.avatar = dr["avatar"].ToString();
                teacherModel.gender = dr["gender"].ToString();
                teacherModel.phone = dr["phone"].ToString();
                teacherModel.school = dr["school"].ToString();
                teacherModel.diploma = dr["diploma"].ToString();
                teacherModel.remark = dr["remark"].ToString();
                teacherModel.has_del = Convert.ToInt32(dr["has_del"].ToString());
                int statusInt = Convert.ToInt32(dr["authStatus"].ToString());
                Chire.ChireInter.Login.Login_Action.User_Status status = (Chire.ChireInter.Login.Login_Action.User_Status)statusInt;

                teacherModel.status = status;
                teacherModel.lifephotoArr = getTeacherLifePhotoList(openId);        // 生活照片
                teacherModel.certPhotoArr = getTeacherImgs(openId);                 // 获取资历图片
                teacherModel.seniorityArr = getTeacherSeniority(openId);            // 资历
     
            }
            sqlcon.Close();
            return teacherModel;
        }
        #endregion

        #region 获取所有老师
        public List<Teacher_Model> getAllTeacher()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher where has_del = '0'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;


            List<Teacher_Model> teacherList = new List<Teacher_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string avatar = dt.Rows[i]["avatar"].ToString();
                string gender = dt.Rows[i]["gender"].ToString();
                string banner = dt.Rows[i]["banner"].ToString();
                string phone = dt.Rows[i]["phone"].ToString();
                string school = dt.Rows[i]["school"].ToString();
                string diploma = dt.Rows[i]["diploma"].ToString();
                string remark = dt.Rows[i]["remark"].ToString();
                string study_level = dt.Rows[i]["study_level"].ToString();
                
                int statusEnumInt = Convert.ToInt32(dt.Rows[i]["authStatus"].ToString());
                Chire.ChireInter.Login.Login_Action.User_Status status = (Chire.ChireInter.Login.Login_Action.User_Status)statusEnumInt;
                Teacher_Model downloadModel = new Teacher_Model()
                {
                    id = id,
                    name = name,
                    gender = gender,
                    phone = phone,
                    avatar = avatar,
                    banner = banner,
                    school = school,
                    diploma = diploma,
                    remark = remark,
                    status = status,
                    study_level= study_level,
                };
                teacherList.Add(downloadModel);
            }
            sqlcon.Close();
            return teacherList;
        }
        #endregion

        #region 获取所有的管理员
        public List<Teacher_Model> getAllAdminManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher where smart = '1'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Teacher_Model> teacherList = new List<Teacher_Model>();
            for (int i = 0; i < rows; i++)
            {
                string openid = dt.Rows[i]["openid"].ToString();
                string name = dt.Rows[i]["name"].ToString();

                Teacher_Model teacherModel = new Teacher_Model()
                {
                    openid = openid,
                    name = name,
                };
                teacherList.Add(teacherModel);
            }
            sqlcon.Close();
            return teacherList;
        }
        #endregion

        #region 删除老师
        public void deleteTeacher(string id)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update teacher set has_del = '1'  where id =" + id + "";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 修改老师信息
        public void updateTeacherInfo(string teacherId, string key, string value)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update user_list set " + key + " = '" + value + "'  where userId ='" + teacherId + "'";

            if (key.Equals("school") || key.Equals("remark") || key.Equals("banner"))
            {
                strSqls = "update teacher set " + key + " = '" + value + "'  where id ='" + teacherId + "'";
            }
            else if (key.Equals("name")) {
                strSqls = "update teacher set " + key + " = '" + value + "'  where id ='" + teacherId + "'";
                Login_Action loginAction = new Login_Action();
                loginAction.updateUserInfo(teacherId, "nick", value);
            }
            else if (key.Equals("phone"))
            {
                strSqls = "update teacher set " + key + " = '" + value + "'  where id ='" + teacherId + "'";
                Login_Action loginAction = new Login_Action();
                loginAction.updateUserInfo(teacherId, "phone", value);
            }
            else if (key.Equals("birthday"))
            {
                Login_Action loginAction = new Login_Action();
                loginAction.updateUserInfo(teacherId, "birthday", value);
            }
                
            else if (key.Equals("gender"))
            {
                string gender = "";
                if (value.Equals("1"))
                {
                    gender = "男";
                }
                else {
                    gender = "女";
                }
                strSqls = "update teacher set " + key + " = '" + gender + "'  where id ='" + teacherId + "'";
                Login_Action loginAction = new Login_Action();
                loginAction.updateUserInfo(teacherId, "sex", value);
            }
            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 修改老师信息
        public void updateTeacherInfo(string name, string avatar, string gender, string phone, string school, string diploma, string remark,string lifephotos,string certphotos,string openid)
        {
            string banner = "";
              if (lifephotos.Length > 0)
            {
                string[] tempArr = lifephotos.Split(',');
                List<string> lifephotoArr = new List<string>(tempArr);
                banner = lifephotoArr[0];
                deleteTeacherLifePotho(openid);
                addTeacherPhoto(openid, lifephotos);
            }
            if (certphotos.Length > 0)
            {
                string[] tempArr = certphotos.Split(',');
                List<string> certphotoArr = new List<string>(tempArr);
                deleteTeacherCertPotho(openid);
                addTeacherCertImg(openid,certphotos);
            }
           

            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update teacher set name = '" + name + "',avatar = '" + avatar + "',banner = '" + banner + "',gender = '" + gender + "',phone = '" + phone + "',school = '" + school + "',diploma='" + diploma + "',remark = '" + remark + "' where openid = '" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 申请成为老师
        public bool applyToTeacher(string userId)
        {
            bool hasSuccessed = false;
            // 1. 通过用户id查找到用户信息
            Login_Action loginAction = new Login_Action();
            User_Model userModel = loginAction.getUserInfoManagerWithId(userId);

            // 2. 判断是否有老师存在
            Teacher_Model teacherModel = getTeacherWithId(userId);
            if (teacherModel.id == null)
            {
                // 2. 插入老师信息
                insertTeacherManager(userModel.id,
                    userModel.nick,
                    userModel.avatar,
                    userModel.sex,
                    userModel.phone,
                    "",
                    "",
                    "",
                    Chire.ChireInter.Login.Login_Action.User_Status.user_status_shenhe,
                    DateTime.Now.ToString(),
                    "0");
                hasSuccessed = true;
            }
            return hasSuccessed;
        }

        #region 插入老师信息
        public void insertTeacherManager(string id, string name, string avatar, int gender, string phone, string school, string diploma, string remark, Chire.ChireInter.Login.Login_Action.User_Status status, string datetime, string has_del)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into teacher(id,name,avatar,gender,phone,school,diploma,remark,datetime,has_del,authStatus) values(@id,@name,@avatar,@gender,@phone,@school,@diploma,@remark,@datetime,@has_del,@authStatus)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            User_Model userModel = new User_Model();

            cmd.Parameters.Add("@id", SqlDbType.NVarChar);
            cmd.Parameters["@id"].Value = id;

            cmd.Parameters.Add("@name", SqlDbType.NVarChar);
            cmd.Parameters["@name"].Value = name;

            // 昵称
            cmd.Parameters.Add("@avatar", SqlDbType.NVarChar);
            cmd.Parameters["@avatar"].Value = avatar;
            // 性别
            cmd.Parameters.Add("@gender", SqlDbType.NVarChar);
            cmd.Parameters["@gender"].Value = gender == 1 ? "男" : "女";
            // 年龄
            cmd.Parameters.Add("@phone", SqlDbType.NVarChar);
            cmd.Parameters["@phone"].Value = phone;

            // 手机号
            cmd.Parameters.Add("@school", SqlDbType.NVarChar);
            cmd.Parameters["@school"].Value = school;
            userModel.phone = phone;
            // 登录类型
            cmd.Parameters.Add("@diploma", SqlDbType.NVarChar);
            cmd.Parameters["@diploma"].Value = diploma;
            // 设备号
            cmd.Parameters.Add("@remark", SqlDbType.NVarChar);
            cmd.Parameters["@remark"].Value = remark;
            // 唯一号
            cmd.Parameters.Add("@datetime", SqlDbType.NVarChar);
            cmd.Parameters["@datetime"].Value = datetime;
            // 头像
            cmd.Parameters.Add("@has_del", SqlDbType.NVarChar);
            cmd.Parameters["@has_del"].Value = has_del;
            userModel.avatar = avatar;

            cmd.Parameters.Add("@authStatus", SqlDbType.Int);
            cmd.Parameters["@authStatus"].Value = status;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion


        #endregion

        #region 添加老师资历
        public void addTeacherSeniority(string info, string id)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into teacher_seniority(name,create_time,link_id) values(@name,@create_time,@link_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.NVarChar);
            cmd.Parameters["@name"].Value = info;

            cmd.Parameters.Add("@create_time", SqlDbType.NVarChar);
            cmd.Parameters["@create_time"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@link_id", SqlDbType.NVarChar);
            cmd.Parameters["@link_id"].Value = id;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 根据老师id 获取老师资历
        public List<string> getTeacherSeniority(string id) {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher_seniority where link_id = '"+id+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> teacherList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string name = dt.Rows[i]["name"].ToString();
              

                teacherList.Add(name);
            }
            sqlcon.Close();
            return teacherList;
        }
        #endregion

        #region 根据老师id 获取老师资历图片
        public List<string> getTeacherImgs(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher_sub where link_id = '" + id + "'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> teacherList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string img = dt.Rows[i]["img"].ToString();

                teacherList.Add(img);
            }
            sqlcon.Close();
            return teacherList;
        }
        #endregion

        #region 根据资历信息删除
        public void delTeacherSeniorityWithInfo(string info)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from teacher_seniority where name = '" + info + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 删除图片
        public void teacherDelImg(string id, string img) { 
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from teacher_sub where img = '" + img + "' and link_id ='"+id+"'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close(); 
        }
        #endregion

        #region 添加图片
        public void teacherAddImg(string id, string img) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into teacher_sub(img,link_id) values(@img,@link_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@img", SqlDbType.NVarChar);
            cmd.Parameters["@img"].Value = img;

            cmd.Parameters.Add("@link_id", SqlDbType.NVarChar);
            cmd.Parameters["@link_id"].Value = id;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion


        #region 添加老师生活照
        public void addTeacherPhoto(string openId, string photos) {
            // 1. 根据传入的photos 转换为数组
            string[] photoArr = photos.Split(',');
            string sqlSQL = "insert into teacher_photo(url,linkid) values";
            for (int i = 0; i < photoArr.Length; i++) { 
                string singleUrl = photoArr[i];
                if (i == photoArr.Length - 1)
                {
                    sqlSQL = sqlSQL + "(" + "'" +  singleUrl +"'" + "," +"'" + openId +"'"+ ")";
                }
                else {
                    sqlSQL = sqlSQL + "(" + "'" + singleUrl + "'" + "," + "'" + openId + "'" + "),";
                }
            }

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
         
            SqlCommand cmd = new SqlCommand(sqlSQL, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 删除老师生活照
        public void deleteTeacherLifePotho(string openId) { 
            
                  // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from teacher_photo where linkid = '"+openId+"'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close(); 
        }
        #endregion

        #region 获取老师的照片
        public List<string> getTeacherLifePhotoList(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher_photo where linkid = '" + id + "'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> teacherList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string img = dt.Rows[i]["url"].ToString();

                teacherList.Add(img);
            }
            sqlcon.Close();
            return teacherList;
        }
        #endregion

        #region 添加老师资历照
        public void addTeacherCertImg(string openId, string photos)
        {
            // 1. 根据传入的photos 转换为数组
            string[] photoArr = photos.Split(',');
            string sqlSQL = "insert into teacher_sub(img,link_id) values";
            for (int i = 0; i < photoArr.Length; i++)
            {
                string singleUrl = photoArr[i];
                if (i == photoArr.Length - 1)
                {
                    sqlSQL = sqlSQL + "(" + "'" + singleUrl + "'" + "," + "'" + openId + "'" + ")";
                }
                else
                {
                    sqlSQL = sqlSQL + "(" + "'" + singleUrl + "'" + "," + "'" + openId + "'" + "),";
                }
            }

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            SqlCommand cmd = new SqlCommand(sqlSQL, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 删除老师资历照
        public void deleteTeacherCertPotho(string openId)
        {

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from teacher_sub where link_id = '" + openId + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

    }
}