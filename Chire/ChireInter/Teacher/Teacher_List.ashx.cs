﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Teacher
{
    /// <summary>
    /// Teacher_List 的摘要说明
    /// </summary>
    public class Teacher_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            getAllInfo();
        }
        #endregion

        #region 获取所有的老师信息
        public void getAllInfo()
        {
            Teacher_Action teacherAction = new Teacher_Action();
            List<Teacher_Model> teacherList = teacherAction.getAllTeacher();
            successManager(teacherList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Teacher_Model> model)
        {
            OUTTeacher_List_Model out_base_setting = new OUTTeacher_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Teacher_List_Model listModel = new Teacher_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}