﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher.Jurisdiction
{
    /// <summary>
    /// Jurisdiction_Init 的摘要说明
    /// </summary>
    public class Jurisdiction_Init : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Jurisdiction_Action action = new Jurisdiction_Action();
            action.initTeacherJurisdiction("bf09d53f65addbd9100382bb78974a28");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}