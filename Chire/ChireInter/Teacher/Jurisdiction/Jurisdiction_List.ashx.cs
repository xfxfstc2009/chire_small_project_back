﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher.Jurisdiction
{
    /// <summary>
    /// Jurisdiction_List 的摘要说明
    /// </summary>
    public class Jurisdiction_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string user_id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }
        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("user_id"))
            {
                user_id = contextWithBase.Request.Form["user_id"];
            }


            searchTeacherJurisdiction();
        }
        #endregion

        #region 查询老师权限表
        public void searchTeacherJurisdiction()
        {
            Jurisdiction_Action action = new Jurisdiction_Action();
            List<Jurisdiction_Model> list = action.getJurisdictionList(user_id);
            successManager(list);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Jurisdiction_Model> list)
        {
            OUTJurisdiction_List_Model out_base_setting = new OUTJurisdiction_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Jurisdiction_List_Model listModel =  new Jurisdiction_List_Model();
            listModel.list = list;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}