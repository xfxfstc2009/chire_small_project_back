﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher.Jurisdiction
{
    /// <summary>
    /// Jurisdiction_Update 的摘要说明
    /// </summary>
    public class Jurisdiction_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string user_id = "";
        string sign = "";
        int show = 0;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }
        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("user_id"))
            {
                user_id = contextWithBase.Request.Form["user_id"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("sign"))
            {
                sign = contextWithBase.Request.Form["sign"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("show"))
            {
                show = Convert.ToInt32( contextWithBase.Request.Form["show"]);
            }
            updateTeacherJurisdiction();
        }
        #endregion

        #region 修改老师权限表
        public void updateTeacherJurisdiction()
        {
            Jurisdiction_Action action = new Jurisdiction_Action();
            action.updateTeacherJurisdiction(sign, show, user_id);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTJurisdiction_List_Model out_base_setting = new OUTJurisdiction_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}