﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher.Jurisdiction
{
    public class Jurisdiction_Model
    {
        public string sign { get; set; }
        public int show { get; set; }
        public string linkId { get; set; }
    }

    public class Jurisdiction_List_Model {
        public List<Jurisdiction_Model> list { get; set; }
    }

    public class OUTJurisdiction_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Jurisdiction_List_Model data { get; set; }
    }

}