﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Teacher
{
    public class Teacher_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string avatar { get; set; }
        public string birthday { get; set; }
        public string banner { get; set; }
        public string gender { get; set; }
        public string phone { get; set; }
        public string school { get; set; }
        public string diploma { get; set; }
        public string remark { get; set; }
        public string openid { get; set; }
        public string unionid { get; set; }
        public int has_del { get; set; }
        public string study_level { get; set; }
        
        public Chire.ChireInter.Login.Login_Action.User_Status status { get; set; }
        public List<string> certPhotoArr { get; set; }                  
        public List<string> lifephotoArr { get; set; }
        public List<string> seniorityArr { get; set; }
    
    }

    public class Teacher_List_Model
    {
        public List<Teacher_Model> list;
    }

    public class OUTTeacher_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Teacher_List_Model data { get; set; }
    }


    public class OUTTeacher_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Teacher_Model data { get; set; }
    }
}