﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.wechat;

namespace Chire.ChireInter.PriceMenu
{
    public class PriceMenu_Action
    {
        #region  添加价格菜单
        // 1. 添加价格菜单
        public void addPriceMenuInfoManager(string name, string price, string subInfo) {
            addPriceMenuInfoManager("", name, price, subInfo);
        }


        public void addPriceMenuInfoManager(string linkId, string name, string price, string subInfo)
        {
            // 1.创建一个id
            string infoid = "";
            if (linkId.Length > 0)
            {
                infoid = linkId;
            }
            else {
                infoid = Constance.Instance.getRandomStr(11);
            }

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into pricemenu(name,price,createtime,infoid) values(@name,@price,@createtime,@infoid)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;

            cmd.Parameters.Add("@price", SqlDbType.VarChar, 10);
            cmd.Parameters["@price"].Value = price;

            cmd.Parameters.Add("@createtime", SqlDbType.VarChar, 30);
            cmd.Parameters["@createtime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@infoid", SqlDbType.VarChar, 50);
            cmd.Parameters["@infoid"].Value = infoid;


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();


            string[] contentInfoList = subInfo.Split(',');

            for (int i = 0; i < contentInfoList.Length; i++)
            {
                string info = contentInfoList[i];
                addPriceMenuDes(info, infoid);
            }
        }

        private void addPriceMenuDes(string des, string lnkId)
        {

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into pricemenu_sub(name,linkId) values(@name,@linkId)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = des;

            cmd.Parameters.Add("@linkId", SqlDbType.VarChar, 20);
            cmd.Parameters["@linkId"].Value = lnkId;


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }


        #endregion


        #region 获取价格菜单
        // 2. 获取价格菜单
        public List<PriceMenu_Model> getPriceMenuListManager()
        {

            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from pricemenu";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<PriceMenu_Model> priceMenuList = new List<PriceMenu_Model>();
            for (int i = 0; i < rows; i++)
            {

                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string price = dt.Rows[i]["price"].ToString();
                string infoid = dt.Rows[i]["infoid"].ToString();
                List<string> infoList = getPriceDesListManager(infoid);

                PriceMenu_Model parentModel = new PriceMenu_Model()
                {
                    id = infoid,
                    name = name,
                    price = price,
                    des = infoList

                };
                priceMenuList.Add(parentModel);
            }
            sqlcon.Close();
            return priceMenuList;
        }

        private List<string> getPriceDesListManager(string infoid)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from pricemenu_sub where linkId = '" + infoid + "'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> priceMenuDesList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string name = dt.Rows[i]["name"].ToString();

                priceMenuDesList.Add(name);
            }
            sqlcon.Close();
            return priceMenuDesList;
        }
        #endregion
      
        // 3. 根据id 获取价格菜单
        public PriceMenu_Model getPriceMenuWithId(string priceMenuId)
        {
            PriceMenu_Model priceMenuModel = new PriceMenu_Model();

            if (priceMenuId.Length <= 0) {
                return priceMenuModel;
            }

            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from pricemenu where infoid = '" + priceMenuId + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            if (dr.Read())
            {
                string infoId = dr["infoid"].ToString();

                priceMenuModel.id = infoId;
                priceMenuModel.name = dr["name"].ToString();
                priceMenuModel.price = dr["price"].ToString();
                priceMenuModel.des = getPriceMenuItemListWithInfoId(infoId);
            }
            sqlcon.Close();
            return priceMenuModel;
        }

        public List<string> getPriceMenuItemListWithInfoId(string infoId) {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from pricemenu_sub where linkId = '"+infoId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> infoList = new List<string>();
            for (int i = 0; i < rows; i++)
            {

                string name = dt.Rows[i]["name"].ToString();
                infoList.Add(name);
            }
            sqlcon.Close();
            return infoList;
        }


        #region 修改菜单
        public void updatePriceMenuWithId(string priceMenuId, string name, string price, string subInfo) { 
            // 1. 删除id下面的菜单
            delPriceMenuInfo(priceMenuId);
            // 2. 添加
            addPriceMenuInfoManager(priceMenuId, name, price, subInfo);
        }

        public void delPriceMenuInfo(string priceMenuId) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from pricemenu where infoid = '" + priceMenuId + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            delPriceMenuSubInfo(priceMenuId);
        }

        public void delPriceMenuSubInfo(string priceMenuId)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from pricemenu_sub where linkId = '" + priceMenuId + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion
    }
}