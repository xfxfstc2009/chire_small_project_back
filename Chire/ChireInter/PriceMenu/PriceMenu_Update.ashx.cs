﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.PriceMenu
{
    /// <summary>
    /// PriceMenu_Update 的摘要说明
    /// </summary>
    public class PriceMenu_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        string name = "";             // 课程名字
        string price = "";            // 课程价格
        string des = "";              // 详情
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }
        #region 验证信息
        public void analysisInfo()
        {

            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("price"))
            {
                price = contextWithBase.Request.Form["price"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("des"))
            {
                des = contextWithBase.Request.Form["des"];
            }
            insertInfoManager();
        }
        #endregion

        public void insertInfoManager()
        {
            PriceMenu_Action menuAction = new PriceMenu_Action();
            menuAction.updatePriceMenuWithId(id,name, price, des);
            successManager();
        }


        #region 成功方法
        public void successManager()
        {
            Out_Parent_List_Model out_base_setting = new Out_Parent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}