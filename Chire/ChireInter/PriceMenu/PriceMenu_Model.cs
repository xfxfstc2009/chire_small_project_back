﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.PriceMenu
{

    public class PriceMenu_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public List<string> des { get; set; }

    }

    public class Out_PriceMenu_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public PriceMenu_Model data { get; set; }
    }

    public class PriceMenu_List_Model
    {
        public List<PriceMenu_Model> list { get; set; }

    }

    public class Out_Parent_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public PriceMenu_List_Model data { get; set; }
    }
}