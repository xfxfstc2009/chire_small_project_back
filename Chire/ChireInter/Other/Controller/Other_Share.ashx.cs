﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using BeeTV_1.Classes.Other.Model;
using Newtonsoft.Json;
using System.IO;

namespace BeeTV_1.Classes.Other.Controller
{
    /// <summary>
    /// Other_Share 的摘要说明
    /// </summary>
    public class Other_Share : IHttpHandler
    {
        HttpContext contextWithBase;
        string userId;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("userId"))
            {
                userId = contextWithBase.Request.Form["userId"];
            }
          
            searchCurrentType();
        }
        #endregion

        public enum Share_Type
        {
            Share_TypeAPP = 1,                  // 手机登录
        }
         

        #region 获取版本
        public void searchCurrentType()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string strselect = "select top 1 * from other_share where share_type = " + (int)Share_Type.Share_TypeAPP + " ORDER BY id DESC ";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Share_SingleModel shareModel = new Share_SingleModel();

            if (dr.Read())
            {
                string id = dr["id"].ToString();
                string title = dr["title"].ToString();
                string des = dr["des"].ToString();
                string imgs = dr["imgs"].ToString();
                string url = dr["url"].ToString();

                shareModel.id = id;
                shareModel.title = title;
                shareModel.desc = des;
                shareModel.imgs = imgs;
                shareModel.url = url;

            }  else {
                shareModel.id = "";
                shareModel.title = ""; ;
                shareModel.desc = ""; ;
                shareModel.imgs = ""; ;
                shareModel.url = ""; ;
            }
            sqlcon.Close();
            successManager(shareModel);
        }
        #endregion


        #region 执行方法
        public void successManager(Share_SingleModel shenheModel)
        {
            OutShare_SingleModel out_base_setting = new OutShare_SingleModel();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = shenheModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}