﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using BeeTV.Classes.Other.Model;
using System.Data;
using Newtonsoft.Json;
using System.IO;

namespace BeeTV_1.Classes.Other.Controller
{
    /// <summary>
    /// Other_Shenhe_Server_Add 的摘要说明
    /// </summary>
    public class Other_Shenhe_Server_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        int isShenhe = 0;
        string appName = "";
        string appVersion = "";
        string lungch_img = "";
        string lungch_content = "";
        string login_delegate = "";
        int show_type = 0;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            if (contextWithBase.Request.Form.AllKeys.Contains("isShenhe"))
            {
                isShenhe = Convert.ToInt32(contextWithBase.Request.Form["isShenhe"]);
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("appName"))
            {
                appName = contextWithBase.Request.Form["appName"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("appVersion"))
            {
                appVersion = contextWithBase.Request.Form["appVersion"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("lungch_img"))
            {
                lungch_img = contextWithBase.Request.Form["lungch_img"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("lungch_content"))
            {
                lungch_content = contextWithBase.Request.Form["lungch_content"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("login_delegate"))
            {
                login_delegate = contextWithBase.Request.Form["login_delegate"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("show_type"))
            {
                show_type = Convert.ToInt32(contextWithBase.Request.Form["show_type"]);
            }
            insertShare();
        }

        #region 执行数据操作
        public void insertShare()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into company_shenhe(appName,isShenhe,appVersion,lungch_img,lungch_content,login_delegate,show_type,time) values(@appName,@isShenhe,@appVersion,@lungch_img,@lungch_content,@login_delegate,@show_type,@time)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            ShenheModel shareModel = new ShenheModel();
            // 昵称
            cmd.Parameters.Add("@appName", SqlDbType.NVarChar);
            cmd.Parameters["@appName"].Value = appName;
            shareModel.appName = appName;

            // 性别
            cmd.Parameters.Add("@isShenhe", SqlDbType.Int);
            cmd.Parameters["@isShenhe"].Value = isShenhe;
            shareModel.isShenhe = isShenhe;
            // 年龄
            cmd.Parameters.Add("@appVersion", SqlDbType.NVarChar);
            cmd.Parameters["@appVersion"].Value = appVersion;
            shareModel.appVersion = appVersion;

            cmd.Parameters.Add("@lungch_img", SqlDbType.NVarChar);
            cmd.Parameters["@lungch_img"].Value = lungch_img;
            shareModel.lungch_img = lungch_img;

            cmd.Parameters.Add("@lungch_content", SqlDbType.NVarChar);
            cmd.Parameters["@lungch_content"].Value = lungch_content;
            shareModel.lungch_img = lungch_content;

            cmd.Parameters.Add("@login_delegate", SqlDbType.NVarChar);
            cmd.Parameters["@login_delegate"].Value = login_delegate;
            shareModel.login_delegate = login_delegate;

            cmd.Parameters.Add("@show_type", SqlDbType.Int);
            cmd.Parameters["@show_type"].Value = show_type;
            shareModel.show_type = show_type;
            
            // 时间
            cmd.Parameters.Add("@time", SqlDbType.VarChar, 50);
            cmd.Parameters["@time"].Value = DateTime.Now.ToString();
           

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            // [去执行成功方法]
            shareManager(shareModel);
        }
        #endregion

        #region 执行方法
        public void shareManager(ShenheModel shareModel)
        {
            OutShenheModel out_base_setting = new OutShenheModel();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";


            out_base_setting.data = shareModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}