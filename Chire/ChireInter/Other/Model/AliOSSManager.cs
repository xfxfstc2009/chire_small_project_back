﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aliyun.OSS;
using System.IO;
using Chire.wechat;

namespace BeeTV_1.Classes.Other.Model
{
    public class AliOSSManager
    {
        private static AliOSSManager _instance;
        public static AliOSSManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AliOSSManager();
                }
                return _instance;
            }
        }

        public enum ImageUsingType
        {
            ImageUsingTypeBanner = 1,                   /**< Banner 首页的banner*/
            ImageUsingTypeUser = 2,                     /**< User/Avatar 用户头像*/
            ImageUsingTypeUserShow = 3,                 /**< User/Show 用户相册*/
            ImageUsingTypeObO = 4,                      /**< Live/OneByOne 一对一视频*/
            ImageUsingTypeLive = 5,                     /**< Live/Live 直播*/
            ImageUsingTypeGift = 6,                     /**< Live/Gift 礼物*/
            ImageUsingTypeActivity = 7,                 /**< Activity/ 活动页面*/
            ImageUsingTypeShare = 8,                    /**< Share/ 分享页面*/
            ImageUsingTypeLaungch = 9,                  /**< Laungch/ 引导图*/
            ImageUsingTypeMessage = 10,                 /**< Message/ 消息传递的图片*/
            ImageUsingTypeTV = 11,                      /**< TV*/
            ImageUsingTypeProduct = 12,                 /**< Product/商品*/
            ImageUsingTypeBuild = 13,                   /**< 建筑物*/
            ImageUsingTypePlayer = 14,                  /**< 视频*/
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="fileName">文件名：/images/demo.jpg</param>
        /// <param name="fileStream"></param>
        public void Upload(string fileName, Stream fileStream)
        {

            OssClient ossClient = new OssClient(Constance.AliOSSEndPoint, Constance.AliOSSaccess, Constance.AliOSSaccessSecret);
            ObjectMetadata metadata = new ObjectMetadata();
            //根据文件名设置ContentType
            metadata.ContentType = GetContentType(fileName);
            string key = fileName;
            fileStream.Seek(0, SeekOrigin.Begin);
            PutObjectResult result = ossClient.PutObject(Constance.AliOSSBucketName, key, fileStream, metadata);
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <param name="dir"></param>
        /// <returns>返回存储后文件路径</returns>
        public string Upload(string fileName, Stream fileStream, string dir)
        {

            OssClient ossClient = new OssClient(Constance.AliOSSEndPoint, Constance.AliOSSaccess, Constance.AliOSSaccessSecret);
            ObjectMetadata metadata = new ObjectMetadata();
            //根据文件名设置ContentType
            metadata.ContentType = GetContentType(fileName);

            string fileext = fileName.Split('.')[1];
            fileName = Guid.NewGuid() + "." + fileext;

            string key = dir + fileName;
            fileStream.Seek(0, SeekOrigin.Begin);
            PutObjectResult result = ossClient.PutObject(Constance.AliOSSBucketName, key, fileStream, metadata);
            return dir + fileName;
        }

        private string GetContentType(string fileName)
        {
            string[] fileNameArray = fileName.Split('.');
            string contentType = string.Empty;
            if (fileNameArray.Length >= 2)
            {
                switch (fileNameArray[1])
                {
                    case "gif":
                        contentType = "image/gif";
                        break;
                    case "png":
                        contentType = "image/png";
                        break;
                    case "jpg":
                        contentType = "image/jpg";
                        break;
                    case "jpeg":
                        contentType = "image/jpeg";
                        break;
                    default:
                        break;
                }
            }
            return contentType;
        }

        public string GetUpLoadPath(ImageUsingType type)
        {
            string path = ""; //站点目录+上传目录
            string infoPath = "";
            if (type == ImageUsingType.ImageUsingTypeBanner)
            {
                infoPath = "Banner";
            }
            else if (type == ImageUsingType.ImageUsingTypeUser)
            {
                infoPath = "User/Avatar";
            }
            else if (type == ImageUsingType.ImageUsingTypeUserShow)
            {
                infoPath = "User/Show";
            }
            else if (type == ImageUsingType.ImageUsingTypeObO)
            {
                infoPath = "Live/OneByOne";
            }
            else if (type == ImageUsingType.ImageUsingTypeLive)
            {
                infoPath = "Live/Live";
            }
            else if (type == ImageUsingType.ImageUsingTypeGift)
            {
                infoPath = "Live/Gift";
            }
            else if (type == ImageUsingType.ImageUsingTypeActivity)
            {
                infoPath = "Activity";
            }
            else if (type == ImageUsingType.ImageUsingTypeShare)
            {
                infoPath = "Share";
            }
            else if (type == ImageUsingType.ImageUsingTypeLaungch)
            {
                infoPath = "Laungch";
            }
            else if (type == ImageUsingType.ImageUsingTypeMessage)
            {
                infoPath = "Message";
            }
            else if (type == ImageUsingType.ImageUsingTypeTV)
            {
                infoPath = "TV";
            }
            else if (type == ImageUsingType.ImageUsingTypeProduct)
            {
                infoPath = "Product";
            }
            else if (type == ImageUsingType.ImageUsingTypeBuild)
            {
                infoPath = "Build";
            }
            else if (type == ImageUsingType.ImageUsingTypePlayer)
            {
                infoPath = "Player";
            }



            path += infoPath + "/" + DateTime.Now.ToString("yyyyMM") + "/" + DateTime.Now.ToString("dd");

            return path + "/";
        }
    }
}