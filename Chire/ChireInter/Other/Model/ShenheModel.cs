﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeeTV.Classes.Other.Model
{
    public class ShenheModel
    {
        public string id { get; set; }
        public string appName { get; set; }
        public string appVersion { get; set; }
        public string lungch_img { get; set; }
        public string lungch_content { get; set; }
        public int isShenhe { get; set; }
        public int show_type { get; set; }
        public string login_delegate { get; set; }              // 代理协议
    }

    public class OutShenheModel
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public ShenheModel data { get; set; }
    }

    public class ShenheListModel
    {
        public List<ShenheModel> list { get; set; }
    }

    public class OUTShenheListModel
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public ShenheListModel data { get; set; }
    }

}