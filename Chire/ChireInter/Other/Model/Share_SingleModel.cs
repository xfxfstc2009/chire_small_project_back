﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeeTV_1.Classes.Other.Model
{
    public class Share_SingleModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string imgs { get; set; }
        public string url { get; set; }
    }

    public class OutShare_SingleModel
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Share_SingleModel data { get; set; }
    }
}