﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_Update 的摘要说明
    /// </summary>
    public class News_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string newsId = "";
        string name = "";
        string html = "";
        string type = "";
        string ablum = "";
        string link = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {

            if (contextWithBase.Request.Form.AllKeys.Contains("newsId"))
            {
                newsId = contextWithBase.Request.Form["newsId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("html"))
            {
                html = contextWithBase.Request.Form["html"];
                html = HttpUtility.UrlDecode(html);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("ablum"))
            {
                ablum = contextWithBase.Request.Form["ablum"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("link"))
            {
                link = contextWithBase.Request.Form["link"];
            }


            insertIntoNews();
        }
        #endregion

        #region 插入数据
        public void insertIntoNews()
        {
            News_Action newsAction = new News_Action();
            newsAction.updateNewsDetailInfoManager(newsId, name, html, type, ablum, link);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            Out_News_Type_List_Model out_base_setting = new Out_News_Type_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}