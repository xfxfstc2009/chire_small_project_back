﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_Type_Update 的摘要说明
    /// </summary>
    public class News_Type_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string ablum = "";
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("ablum"))
            {
                ablum = contextWithBase.Request.Form["ablum"];
            }
              if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
        
            insertIntoNews();
        }
        #endregion

        #region 插入数据
        public void insertIntoNews()
        {
            News_Action newsAction = new News_Action();
            newsAction.updateNewsTypeInfoManager(id, ablum, name);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            Out_News_Type_List_Model out_base_setting = new Out_News_Type_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}