﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.News
{
    public class News_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string des { get; set; }
        public string type { get; set; }
        public string ablum { get; set; }
        public string link { get; set; }
        public string createtime { get; set; }
    }
    public class Out_News_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public News_Model data { get; set; }
    }

    public class News_List_Model
    {
        public List<News_Model> list { get; set; }
    }

    public class Out_News_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public News_List_Model data { get; set; }
    }

    //消息类型列表

    public class News_Type_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string createtime { get; set; }
        public string linkId { get; set; }
        public List<string> ablum { get; set; }
        
    }

    public class News_Type_List_Model
    {
        public List<News_Type_Model> list { get; set; }
    }

    public class Out_News_Type_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public News_Type_List_Model data { get; set; }
    }

    public class Out_News_Type_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public News_Type_Model data { get; set; }
    }
}