﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.wechat;

namespace Chire.ChireInter.News
{
    public class News_Action
    {
        #region 获取消息类型列表
        // 1. 获取消息类型列表
        public List<News_Type_Model> getNewsTypeList()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from news_type";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<News_Type_Model> newstypeList = new List<News_Type_Model>();
            for (int i = 0; i < rows; i++)
            {
                News_Type_Model newsTypeModel = new News_Type_Model();
                newsTypeModel.id = dt.Rows[i]["id"].ToString();
                newsTypeModel.name = dt.Rows[i]["name"].ToString();
                newsTypeModel.createtime = dt.Rows[i]["createtime"].ToString();
                newsTypeModel.linkId = dt.Rows[i]["linkId"].ToString();
                string ablum = dt.Rows[i]["ablum"].ToString();
                string[] ablumList = ablum.Split(',');
                List<string> list1 = new List<string>(ablumList);

                newsTypeModel.ablum = list1;
                
                newstypeList.Add(newsTypeModel);
            }
            sqlcon.Close();


            return newstypeList;
        }

        #endregion

        #region 添加消息类型
        // 2. 添加消息类型
        public void addNewsInfoManager(string info, string ablum)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into news_type(name,createtime,ablum,linkId) values(@name,@createtime,@ablum,@linkId)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 350);
            cmd.Parameters["@name"].Value = info;
            cmd.Parameters.Add("@createtime", SqlDbType.VarChar, 50);
            cmd.Parameters["@createtime"].Value = DateTime.Now.ToString();
            cmd.Parameters.Add("@linkId", SqlDbType.VarChar, 350);
            cmd.Parameters["@linkId"].Value = Constance.Instance.getRandomStr(11);
            cmd.Parameters.Add("@ablum", SqlDbType.VarChar, 350);
            cmd.Parameters["@ablum"].Value = ablum;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }

        #endregion

        #region 根据id 获取消息类型详情
        public News_Type_Model getNewTypeDetail(string id)
        {
            News_Type_Model newsTypeModel = new News_Type_Model();

            if (id.Length <= 0)
            {
                return newsTypeModel;
            }

            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from news_type where linkId = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            if (dr.Read())
            {
                newsTypeModel.id = dr["id"].ToString();;
                newsTypeModel.name = dr["name"].ToString();
                newsTypeModel.createtime = dr["createtime"].ToString();
                newsTypeModel.linkId = dr["createtime"].ToString();
                string ablum = dr["ablum"].ToString();
                string[] ablumList = ablum.Split(',');
                List<string> list1 = new List<string>(ablumList);

                newsTypeModel.ablum = list1;
            }
            sqlcon.Close();
            return newsTypeModel;
        }
        #endregion

        #region 根据id 删除消息类型
        public void deleteNewsTypeWithId(string id) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from news_type where linkId = '" + id + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 根据id 删除消息
        public void deleteNewsWithId(string id)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from news_list where id = '" + id + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 修改消息
        public void updateNewsTypeInfoManager(string id ,string ablum,string name) {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update news_type set ablum = '" + ablum +"',name = '"+name+"'  where linkId ='" + id + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 获取消息
        public List<News_Model> getNewsList(string type, int page, int size)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            int pageWithIndex = ((page - 1) * size) + 1;
            int maxPageIndex = pageWithIndex + size - 1;
            string strselect = "select a.* from(select row_number() over (order by id desc) as rn,* from news_list) a where type ='" + type + "' and rn between '" + pageWithIndex + "' and '" + maxPageIndex + "'";

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<News_Model> newsList = new List<News_Model>();
            for (int i = 0; i < rows; i++)
            {

                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string des = dt.Rows[i]["des"].ToString();
                string typeInfo = dt.Rows[i]["type"].ToString();
                string ablum = dt.Rows[i]["ablum"].ToString();
                string link = dt.Rows[i]["link"].ToString();
                string datetime = dt.Rows[i]["createtime"].ToString();
                News_Model newsModel = new News_Model()
                {
                    id = id,
                    name = name,
                    des = des,
                    type = typeInfo,
                    ablum = ablum,
                    link = link,
                    createtime = datetime
                };
                newsList.Add(newsModel);
            }
            sqlcon.Close();
            return newsList;
        }

        #endregion

        #region 根据id获取消息详情
        public News_Model getNewsDetail(string id)
        {
            
          SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from news_list where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            News_Model examFileModel = new News_Model();
           
            if (dr.Read())
            {
                examFileModel.id = dr["id"].ToString();
                examFileModel.name = dr["name"].ToString();
                examFileModel.des = dr["des"].ToString();
                examFileModel.type = dr["type"].ToString();
                examFileModel.ablum = dr["ablum"].ToString();
                examFileModel.link = dr["link"].ToString();
                examFileModel.createtime = dr["createtime"].ToString();

            }
            sqlcon.Close();
            return examFileModel;
        }
        #endregion

        #region 修改消息详情
    
        public void updateNewsDetailInfoManager(string id,string name,string des,string type, string ablum,string link )
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update news_list set name = '"+name+"',des='"+des+"',type='"+type+"',ablum = '"+ablum+"',link = '"+link+"'  where id ='" + id + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion
    }
}