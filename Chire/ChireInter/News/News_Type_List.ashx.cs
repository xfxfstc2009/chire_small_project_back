﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_Type_List 的摘要说明
    /// </summary>
    public class News_Type_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            getNewsTypeList();
        }
        #endregion

        #region 插入数据
        public void getNewsTypeList()
        {
            News_Action newsAction = new News_Action();
            List<News_Type_Model>newsTypeList = newsAction.getNewsTypeList();
            successManager(newsTypeList);
        }
        #endregion

        #region 成功方法
        public void successManager(List<News_Type_Model> newsTypeList)
        {
            Out_News_Type_List_Model out_base_setting = new Out_News_Type_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            News_Type_List_Model newsTypeListModel = new News_Type_List_Model();
            newsTypeListModel.list = newsTypeList;
            out_base_setting.data = newsTypeListModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}