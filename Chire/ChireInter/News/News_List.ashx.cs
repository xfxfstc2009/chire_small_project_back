﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_List 的摘要说明
    /// </summary>
    public class News_List : IHttpHandler
    {

        HttpContext contextWithBase;
        int page = 1;
        int size = 20;
        string newstype = "KVKjiGM5wFK";                   // 消息类型 1日语 2 其他协议 3绘画
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("page"))
            {
                page = Convert.ToInt32(contextWithBase.Request.Form["page"]);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("size"))
            {
                size = Convert.ToInt32(contextWithBase.Request.Form["size"]);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("newstype"))
            {
                newstype = contextWithBase.Request.Form["newstype"];
            }

            getNewsList();
        }
        #endregion

        #region 获取消息信息
        public void getNewsList() {
            News_Action newsAction = new News_Action();
            List<News_Model>newsList = newsAction.getNewsList(newstype, page, size);
            successManager(newsList);
        }
        #endregion

        #region 成功方法
        public void successManager(List<News_Model> newsList)
        {
            Out_News_List_Model out_base_setting = new Out_News_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            News_List_Model newsTypeListModel = new News_List_Model();
            newsTypeListModel.list = newsList;
            out_base_setting.data = newsTypeListModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}