﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_Detail 的摘要说明
    /// </summary>
    public class News_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string newsId = "";
       
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("newsId"))
            {
                newsId = contextWithBase.Request.Form["newsId"];
            }
  

            insertIntoNews();
        }
        #endregion

        #region 插入数据
        public void insertIntoNews()
        {
            News_Action newsAction = new News_Action();
            News_Model newsModel = newsAction.getNewsDetail(newsId);
            successManager(newsModel);
        }
        #endregion

        #region 成功方法
        public void successManager(News_Model newsModel)
        {

            Out_News_Model out_base_setting = new Out_News_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = newsModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}