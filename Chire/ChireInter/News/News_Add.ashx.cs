﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Login;
using Chire.wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.News
{
    /// <summary>
    /// News_Add 的摘要说明
    /// </summary>
    public class News_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string des = "";
        string type = "";
        string ablum = "";
        string link = "";
        int haspush = 0;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("html"))
            {
                des = contextWithBase.Request.Form["html"];
                des = HttpUtility.UrlDecode(des);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("ablum"))
            {
                ablum = contextWithBase.Request.Form["ablum"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("link"))
            {
                link = contextWithBase.Request.Form["link"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("haspush"))
            {
                haspush = Convert.ToInt32(contextWithBase.Request.Form["haspush"]);
            }
            insertIntoNews();
        }
        #endregion

        #region 插入数据
        public void insertIntoNews()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into news_list(name,des,type,ablum,link,createtime,push) values(@name,@des,@type,@ablum,@link,@createtime,@push)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            cmd.Parameters.Add("@des", SqlDbType.VarChar, 350);
            cmd.Parameters["@des"].Value = des;
            cmd.Parameters.Add("@type", SqlDbType.VarChar, 50);
            cmd.Parameters["@type"].Value = type;
            cmd.Parameters.Add("@ablum", SqlDbType.VarChar, 350);
            cmd.Parameters["@ablum"].Value = ablum;
            cmd.Parameters.Add("@link", SqlDbType.VarChar, 350);
            cmd.Parameters["@link"].Value = link;
            cmd.Parameters.Add("@createtime", SqlDbType.VarChar, 350);
            cmd.Parameters["@createtime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@push", SqlDbType.Int);
            cmd.Parameters["@push"].Value = haspush;


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            xiaoxituisong();
            Out_News_Model out_base_setting = new Out_News_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
            
        }
        #endregion

        #region 小程序推送
        public void xiaoxituisong()
        {
            if (haspush == 1)
            {                     //  表示进行推送
                // 2. 拼接信息
                SmallProjectModelDataList wechatModelList = new SmallProjectModelDataList();

                // 1. 用户昵称
                SmallProjectModelData thing1 = new SmallProjectModelData();
                thing1.value = "您有一条消息";
                     // 2. 内容
                SmallProjectModelData time2 = new SmallProjectModelData();
                time2.value = DateTime.Now.ToString();

                // 2. 内容
                SmallProjectModelData thing4 = new SmallProjectModelData();
                thing4.value = name;

                wechatModelList.thing1 = thing1;
                wechatModelList.time2 = time2;
                wechatModelList.thing4 = thing4;


                Xiaochengxu_Action xiaochengxuAction = new Xiaochengxu_Action();

                // 1. 给所有用户进行推送
                User_Action userAction = new User_Action();
                List<User_Model> userModelList = userAction.getAllUser();
                for (int i = 0; i < userModelList.Count; i++) {
                    User_Model userModel = userModelList[i];
                    xiaochengxuAction.smallproject_push_msg(userModel.open_id, wechatModelList);
                }
                  
            }
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}