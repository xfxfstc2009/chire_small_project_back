﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Download;
using Newtonsoft.Json;
using Chire.wechat;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_File_Add 的摘要说明
    /// </summary>
    public class Exam_File_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string url = "";
        string fileId = "";
        string beizhu = "";
        string classId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("beizhu"))
            {
                beizhu = contextWithBase.Request.Form["beizhu"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classId"))
            {
                classId = contextWithBase.Request.Form["classId"];
            }


            fileId = Constance.Instance.getRandomStr(18);


            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into exam_file_list(name,datetime,file_id,beizhu,class_id) values(@name,@datetime,@file_id,@beizhu,@class_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
    
            // 2.日记详情
            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@file_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@file_id"].Value = fileId;

            cmd.Parameters.Add("@beizhu", SqlDbType.VarChar, 1000);
            cmd.Parameters["@beizhu"].Value = beizhu;

            cmd.Parameters.Add("@class_id", SqlDbType.VarChar, 30);
            cmd.Parameters["@class_id"].Value = classId;
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();


            // 2. 添加url
            if (url.Length > 0) {
                string[] workStringArr = url.Split(',');
                List<string> workList = workStringArr.ToList<string>();
                for (int i = 0; i < workList.Count; i++)
                {
                    string info = workList[i];
                    addUrlInfoManamer(info, fileId);
                }
            }
           

            // 3. 给每个学生添加0分
            getAllStudent();

            // 成功
            successManager();
        }
        #endregion

        #region 添加url
        public void addUrlInfoManamer(string urls, string linkid) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into exam_file_list_sub(url,file_id) values(@url,@file_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@url", SqlDbType.VarChar, 5000);
            cmd.Parameters["@url"].Value = urls;

            cmd.Parameters.Add("@file_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@file_id"].Value = linkid;


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }        
        #endregion

        #region 成功方法
        public void successManager()
        {

            OutDownLoad_List out_base_setting = new OutDownLoad_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 给所有的学生插入这个卷子为0分
        public void getAllStudent(){
            Student_Action studentAction = new Student_Action();
            List<Student_Model> studentList = studentAction.getAllStudent("1",classId);

            for (int i = 0; i < studentList.Count; i++) {
                string linkId = Constance.Instance.getRandomStr(10) + i;

                Student_Model studentModel = studentList[i];
                insertExamInfo(studentModel.id, fileId, linkId, classId);
            }
        }
        #endregion

        #region 插入试卷内容
        public void insertExamInfo(string studentId, string examId,string linkId,string classId)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into exam(student_id,exam_id,score,linkId,datetime,classId) values(@student_id,@exam_id,@score,@linkId,@datetime,@classId)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@student_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@student_id"].Value = studentId;

            cmd.Parameters.Add("@classId", SqlDbType.VarChar, 50);
            cmd.Parameters["@classId"].Value = classId;
            // 1. 日记标题
            cmd.Parameters.Add("@exam_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@exam_id"].Value = examId;
            // 1. 日记标题
            cmd.Parameters.Add("@score", SqlDbType.VarChar, 50);
            cmd.Parameters["@score"].Value = 0;
            // 1. 日记标题
            cmd.Parameters.Add("@linkId", SqlDbType.VarChar, 1000);
            cmd.Parameters["@linkId"].Value = linkId;
            // 2.日记详情
            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = "";

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}