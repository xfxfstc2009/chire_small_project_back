﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Exam
{
    public class Exam_File_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string datetime { get; set; }
        public string beizhu { get; set; }
        public List<string> filesList { get; set; }
        public string class_id { get; set; }
    }

    public class Exam_File_List_Model
    {
        public List<Exam_File_Model> list { get; set; }
    }

    public class OUTExam_File_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Exam_File_List_Model data { get; set; }
    }

    public class OUTExam_File_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Exam_File_Model data { get; set; }
    }
}