﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_File_Update 的摘要说明
    /// </summary>
    public class Exam_File_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string url = "";
        string fileId = "";
        string beizhu = "";
        string classId = "";
        string examId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("examId"))
            {
                examId = contextWithBase.Request.Form["examId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("beizhu"))
            {
                beizhu = contextWithBase.Request.Form["beizhu"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classId"))
            {
                classId = contextWithBase.Request.Form["classId"];
            }
            examInfo();
        }
        #endregion

        #region 获取考试数据
        public void examInfo()
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
  
            string strSqls = "update exam_file_list set name = '" + name + "',datetime='" + DateTime.Now.ToString() + "' ,beizhu = '" + beizhu + "'  where file_id = '" + examId + "' and class_id ='" + classId + "' ";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();


            // 删除所有图片信息
            deleteImgs();

            // 2.添加图片信息
            if (url.Length > 0)
            {
                string[] workStringArr = url.Split(',');
                List<string> workList = workStringArr.ToList<string>();
                for (int i = 0; i < workList.Count; i++)
                {
                    string info = workList[i];
                    insetImgs(info);
                }
            }


            // 成功抛出
            successManager();
         
        }
        #endregion

        #region 删除原先图片。
        public void deleteImgs() {

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from exam_file_list_sub where file_id = '"+examId+"'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 添加图片
        public void insetImgs(string subImg) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into exam_file_list_sub(url,file_id) values(@url,@file_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@url", SqlDbType.VarChar, 5000);
            cmd.Parameters["@url"].Value = subImg;

            cmd.Parameters.Add("@file_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@file_id"].Value = examId;


            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTExam_File_Model out_base_setting = new OUTExam_File_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}