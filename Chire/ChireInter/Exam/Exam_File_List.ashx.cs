﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_File_List 的摘要说明
    /// </summary>
    public class Exam_File_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string classId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("classId"))
            {
                classId = contextWithBase.Request.Form["classId"];
            }

            getAllInfo();
        }
        #endregion

        public void getAllInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam_file_list order by datetime desc";
            if (classId.Length > 0) {
                strselect = "select * from exam_file_list where class_id = '" + classId + "' order by datetime desc";
            }
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Exam_File_Model> exam_list = new List<Exam_File_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["file_id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();
                string beizhu = dt.Rows[i]["beizhu"].ToString();
                List<string> examList = getFilesList(id);
                Exam_File_Model examFileModel = new Exam_File_Model()
                {
                    id = id,
                    name = name,
                    datetime = datetime,
                    beizhu = beizhu,
                    filesList = examList,
                };
                exam_list.Add(examFileModel);
            }


            sqlcon.Close();
            successManager(exam_list);
        }

        #region 获取当前的数组
        public List<string> getFilesList(string fileId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam_file_list_sub where file_id = '"+fileId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> exam_list = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string url = dt.Rows[i]["url"].ToString();

                exam_list.Add(url);
            }
            sqlcon.Close();
            return exam_list;
        }
        #endregion

        #region 执行方法
        public void successManager(List<Exam_File_Model> model)
        {
            OUTExam_File_List_Model out_base_setting = new OUTExam_File_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Exam_File_List_Model listModel = new Exam_File_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}