﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_Detail 的摘要说明
    /// </summary>
    public class Exam_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string examId = "";
        string classId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("examId"))
            {
                examId = contextWithBase.Request.Form["examId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classId"))
            {
                classId = contextWithBase.Request.Form["classId"];
            }
            examInfo();
        }
        #endregion

        #region 获取考试数据
        public void examInfo()
        {
            Exam_Action examAction = new Exam_Action();
            List<Exam_AllStudent_Single_Model> examList = examAction.getAllExamDetailWithFileId(examId,classId);
            successManager(examList);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Exam_AllStudent_Single_Model> examList)
        {
            OutExam_AllStudent_List_Model out_base_setting = new OutExam_AllStudent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Exam_AllStudent_List_Model listModel = new Exam_AllStudent_List_Model();
            listModel.list = examList;

            out_base_setting.data = listModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}