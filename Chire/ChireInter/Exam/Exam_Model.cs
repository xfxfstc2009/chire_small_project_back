﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Exam
{
    public class Exam_Model
    {
        public string id { get; set; }
        public string student_id { get; set; }
        public string exam_id { get; set; }
        public string score { get; set; }
        public string linkId { get; set; }
        public string datetime { get; set; }
        public string marks { get; set; }


        public Student_Model student { get; set; }
        public Exam_File_Model examFile { get; set; }
        public List<string> examScoreList { get; set; }
    }

    public class Exam_List_Model
    {
        public List<Exam_Model> list;
    }

    public class OUTExam_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Exam_List_Model data { get; set; }
    }

    public class OutExam_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Exam_Model data { get; set; }
    }

    public class OutExam_Chart_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Exam_Chart_List_Model data { get; set; }
    }

    public class Exam_Chart_Model
    {
        // 1. 学生model
        public Student_Model studentModel { get; set; }
        // 2. 错误信息
        public List<int> info { get; set; }
    }

    public class Exam_Chart_List_Model {
        public List<Exam_Chart_Model> chartList { get; set; }
        public List<string> dateList { get; set; }
    }


    // 获取所有学生该场考试成绩列表
    public class Exam_AllStudent_Single_Model
    {
        public string id { get; set; }
        public Student_Model student { get; set; }
        public string exam_id { get; set; }
        public string score { get; set; }
        public string linkId { get; set; }
        public string datetime { get; set; }
        public string marks { get; set; }
        public List<string>imgList { get; set; }
    }

    public class Exam_AllStudent_List_Model
    {
        public List<Exam_AllStudent_Single_Model> list { get; set; }
    }

    public class OutExam_AllStudent_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Exam_AllStudent_List_Model data { get; set; }
    }

}