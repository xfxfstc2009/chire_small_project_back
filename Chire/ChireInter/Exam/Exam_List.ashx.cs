﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Newtonsoft.Json;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_List 的摘要说明
    /// </summary>
    public class Exam_List : IHttpHandler
    {

        HttpContext contextWithBase;
        string classId = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("classId"))
            {
                classId = contextWithBase.Request.Form["classId"];
            }

            getAllInfo();
        }
        #endregion

        #region 获取所有测验信息
        public void getAllInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam where score != 0";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Exam_Model> exam_list = new List<Exam_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();
                string exam_id = dt.Rows[i]["exam_id"].ToString();
                string score = dt.Rows[i]["score"].ToString();
                string fileUrl = dt.Rows[i]["fileUrl"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();

                // 获取学生信息
                Student_Model studentModel = getStudentWithId(student_id);
                Exam_File_Model fileModel = getExamFileWithId(exam_id);
                Exam_Model examModel = new Exam_Model()
                {
                    id = id,
                    student_id = student_id,
                    exam_id = exam_id,
                    score = score,
                    datetime = datetime,
                    student = studentModel,
                    examFile = fileModel,
                    
                };
                exam_list.Add(examModel);
            }
            sqlcon.Close();
            successManager(exam_list);
        }
        #endregion

        #region 根据学生id获取学生信息
        public Student_Model getStudentWithId(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from student where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Student_Model studentModel = new Student_Model();
            if (dr.Read())
            {
                string studentId = dr["id"].ToString();                                        // 显示的网页
                string name = dr["name"].ToString();
                studentModel.id = studentId;
                studentModel.name = name;
            }
            sqlcon.Close();
            return studentModel;
        }
        #endregion

        #region 根据学生id获取测验列表
        public List<string> sendRequestToGetExamListWithId(string studentId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam where student_id='"+studentId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> exam_list = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string score = dt.Rows[i]["score"].ToString();

                exam_list.Add(score);
            }
            sqlcon.Close();
            return exam_list;
        }
        #endregion

        #region 根据测验id获取测验内容信息
        public Exam_File_Model getExamFileWithId(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from exam_file_list where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Exam_File_Model examFileModel = new Exam_File_Model();

            if (dr.Read())
            {
                string examfileId = dr["id"].ToString();                                        // 显示的网页
                string name = dr["name"].ToString();
                string url = dr["url"].ToString();

                examFileModel.id = examfileId;
                examFileModel.name = name;
            }
            sqlcon.Close();
            return examFileModel;

        }
        #endregion

        #region 执行方法
        public void successManager(List<Exam_Model> model)
        {
            OUTExam_List_Model out_base_setting = new OUTExam_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Exam_List_Model listModel = new Exam_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion


    }
}