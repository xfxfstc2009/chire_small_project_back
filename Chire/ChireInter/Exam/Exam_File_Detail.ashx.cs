﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_File_Detail 的摘要说明
    /// </summary>
    public class Exam_File_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string examId = "";
        string classid = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("examId"))
            {
                examId = contextWithBase.Request.Form["examId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classid"))
            {
                classid = contextWithBase.Request.Form["classid"];
            }
            examInfo();
        }
        #endregion

        #region 获取考试数据
        public void examInfo()
        {
            Exam_Action examAction = new Exam_Action();
            Exam_File_Model examList = examAction.getExamFileDetail(examId, classid);
            successManager(examList);
        }
        #endregion

        #region 成功方法
        public void successManager(Exam_File_Model examList)
        {
            OUTExam_File_Model out_base_setting = new OUTExam_File_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = examList;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}