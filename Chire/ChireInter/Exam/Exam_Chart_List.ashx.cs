﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Newtonsoft.Json;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_Chart_List 的摘要说明
    /// </summary>
    public class Exam_Chart_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string classId = "";
        List<string> studentIdList;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }
        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("classId"))
            {
                classId = contextWithBase.Request.Form["classId"];
            }

            getAllInfo();
        }
        #endregion

        #region 获取所有测验信息
        public void getAllInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam where classId = '"+classId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            if (studentIdList == null) {
                studentIdList = new List<string>();
            }

            for (int i = 0; i < rows; i++)
            {
                string student_id = dt.Rows[i]["student_id"].ToString();
                if (studentIdList.Contains(student_id) == false)
                {
                    studentIdList.Add(student_id);
                }
            }
            List<Exam_Chart_Model> chartList = new List<Exam_Chart_Model>();
            for (int i = 0 ; i < studentIdList.Count;i++){
                string studentId = studentIdList[i];
                if (studentId.Length > 0) {
                    Student_Model studentModel = getStudentWithId(studentId);
                    List<int> info = sendRequestToGetExamListWithId(studentId);

                    Exam_Chart_Model chartModel = new Exam_Chart_Model()
                    {
                        studentModel = studentModel,
                        info = info,
                    };

                    chartList.Add(chartModel);
                }
            }
            successManager(chartList);
        }
        #endregion

        #region 根据学生id获取学生信息
        public Student_Model getStudentWithId(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from student where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Student_Model studentModel = new Student_Model();
            if (dr.Read())
            {
                string studentId = dr["id"].ToString();                                        // 显示的网页
                string name = dr["name"].ToString();
                studentModel.id = studentId;
                studentModel.name = name;
            }
            sqlcon.Close();
            return studentModel;
        }
        #endregion

        #region 根据学生id获取测验列表
        public List<int> sendRequestToGetExamListWithId(string studentId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam where student_id='" + studentId + "'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<int> exam_list = new List<int>();
            for (int i = 0; i < rows; i++)
            {
                string scoreStr = dt.Rows[i]["score"].ToString();
                if (scoreStr != null && scoreStr != "")
                {
                    Int32 score = Convert.ToInt32(scoreStr);

                    exam_list.Add(score);
                }
        
            }
            sqlcon.Close();
            return exam_list;
        }
        #endregion

        #region 获取日期
        public List<string> getTimeInfo() {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam_file_list";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> dateList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
      
                string datetime = dt.Rows[i]["datetime"].ToString();
                string[] sArray = datetime.Split(' ');
                dateList.Add(sArray[0]);
            }
            sqlcon.Close();
            return dateList;
        }
        #endregion

        #region 执行方法
        public void successManager(List<Exam_Chart_Model> model)
        {
            OutExam_Chart_Model out_base_setting = new OutExam_Chart_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Exam_Chart_List_Model listModel = new Exam_Chart_List_Model();
            listModel.chartList = model;
            listModel.dateList = getTimeInfo();
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}