﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Newtonsoft.Json;

namespace Chire.ChireInter.Subject
{
    /// <summary>
    /// Student_Search 的摘要说明
    /// </summary>
    public class Student_Search : IHttpHandler
    {
        HttpContext contextWithBase;
        string keyword = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("keyword"))
            {
                keyword = contextWithBase.Request.Form["keyword"];
            }

            searchManager();
        }
        #endregion

        #region 搜索方法
        public void searchManager() {
            Student_Action studentAction = new Student_Action();
            List<Student_Model> studentList = studentAction.studentSearchWithKeyword(keyword);
            successManager(studentList);
        }

        #endregion

        #region 执行方法
        public void successManager(List<Student_Model> model)
        {
            OUTStudent_List_Model out_base_setting = new OUTStudent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Student_List_Model listModel = new Student_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}