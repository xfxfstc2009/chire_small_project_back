﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Students
{
    /// <summary>
    /// Students_Update 的摘要说明
    /// </summary>
    public class Students_Update : IHttpHandler
    {
        HttpContext contextWithBase;
        string openid = "";                     // 用户id
        string avatar = "";                     // 头像
        string name = "";                       // 名字
        string gender = "";                     // 性别
        string birthday = "";                   // 性别
        string phone = "";                      // 手机号
        string school = "";                     // 学校
        string parentnumber = "";                // 父母手机
        string mark = "";                       // 签名
        string classid = "";                    // 手机

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();            
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("avatar"))
            {
                avatar = contextWithBase.Request.Form["avatar"];
            }
                  if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
                  if (contextWithBase.Request.Form.AllKeys.Contains("gender"))
                  {
                      gender = contextWithBase.Request.Form["gender"];
                  }
               if (contextWithBase.Request.Form.AllKeys.Contains("birthday"))
                  {
                      birthday = contextWithBase.Request.Form["birthday"];
                  }
               if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
               {
                   phone = contextWithBase.Request.Form["phone"];
               }
    if (contextWithBase.Request.Form.AllKeys.Contains("school"))
               {
                   school = contextWithBase.Request.Form["school"];
               }
    if (contextWithBase.Request.Form.AllKeys.Contains("parentnumber"))
               {
                   parentnumber = contextWithBase.Request.Form["parentnumber"];
               }
               if (contextWithBase.Request.Form.AllKeys.Contains("mark"))
               {
                   mark = contextWithBase.Request.Form["mark"];
               }
               if (contextWithBase.Request.Form.AllKeys.Contains("classid"))
               {
                   classid = contextWithBase.Request.Form["classid"];
               }
            updateStudentInfo();
        }
        #endregion

        #region 修改学生信息
        public void updateStudentInfo()
        {


            Student_Action studentAction = new Student_Action();
            // 1. 判断是否有学生
            Student_Model studentModel = studentAction.getStudentInfoWithStudentOpenId(openid);
            if (studentModel.id.Length > 0) { // 表示有学生
                //连接数据库
                SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
                sqlcon1.Open();
                //修改数据信息
                string strSqls = "update student set name = '" + name + "',birthday = '" + birthday + "',gender = '" + gender + "',avatar = '" + avatar + "',phone = '" + phone + "',school = '" + school + "',parentnumber='" + parentnumber + "',mark='" + mark + "',classid = '" + classid + "' where openid ='" + openid + "'";

                SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
                //添加参数并且设置参数值
                cmd.ExecuteNonQuery();
                sqlcon1.Close();


            }
            successManager();
        }
        #endregion

        #region 执行方法
        public void successManager()
        {
            OUTStudent_List_Model out_base_setting = new OUTStudent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
     
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}