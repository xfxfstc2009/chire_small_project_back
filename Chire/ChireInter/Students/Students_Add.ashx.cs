﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Login;
using Chire.wechat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Students
{
    /// <summary>
    /// Students_Add 的摘要说明
    /// </summary>
    public class Students_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";
        string age = "";
        string phone = "";
        string school = "";
        string snumber = "";
        string parentnumber = "";
        string qq = "";
        string wechat = "";
        string avatar = "";
        string gender = "";
        string mark = "";
        string infomation = "";
        string code = "";
        string unionid = "";
        string openid = "";
        string classid = "";
        string birthday = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("gender"))
            {
                gender = contextWithBase.Request.Form["gender"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("age"))
            {
                age = contextWithBase.Request.Form["age"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("avatar"))
            {
                avatar = contextWithBase.Request.Form["avatar"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("phone"))
            {
                phone = contextWithBase.Request.Form["phone"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("school"))
            {
                school = contextWithBase.Request.Form["school"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("snumber"))
            {
                snumber = contextWithBase.Request.Form["snumber"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("parentnumber"))
            {
                parentnumber = contextWithBase.Request.Form["parentnumber"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("qq"))
            {
                qq = contextWithBase.Request.Form["qq"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("wechat"))
            {
                wechat = contextWithBase.Request.Form["wechat"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("mark"))
            {
                mark = contextWithBase.Request.Form["mark"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("infomation"))
            {
                infomation = contextWithBase.Request.Form["infomation"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("unionid"))
            {
                unionid = contextWithBase.Request.Form["unionid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classid"))
            {
                classid = contextWithBase.Request.Form["classid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("birthday"))
            {
                birthday = contextWithBase.Request.Form["birthday"];
            }
   

            actionMainManager();
        }
        #endregion

        #region 主方法
        public void actionMainManager()
        {
            // 1.判断是否有code
            if (code.Length > 0 && openid.Length <= 0)
            {                // 表示公众号进入。不是小程序
                getWechatInfo();
            }
            else
            {              // 表示小程序进入。不是公众号进入
                insertUserStudent();
            }
        }
        #endregion

        #region 获取微信信息
        public void getWechatInfo()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from wechat_power where code = '" + code + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            if (dr.Read())
            {
                string info = dr["info"].ToString();                                        // 显示的网页
                JObject jo = (JObject)JsonConvert.DeserializeObject(info);
                unionid = jo["unionid"].ToString();
                openid = jo["openid"].ToString();
                avatar = jo["headimgurl"].ToString();
                gender = jo["sex"].ToString().Equals("1") == true ? "男" : "女";
            }
            sqlcon.Close();
            insertUserStudent();
        }
        #endregion

        #region 插入学生表
        public void insertUserStudent()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into student(id,name,age,phone,school,snumber,parentnumber,qq,wechat,avatar,gender,jointime,status,mark,infomation,unionid,openid,classid,authStatus,birthday) values (@id,@name,@age,@phone,@school,@snumber,@parentnumber,@qq,@wechat,@avatar,@gender,@jointime,@status,@mark,@infomation,@unionid,@openid,@classid,@authStatus,@birthday)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 1. 作者id
            cmd.Parameters.Add("@id", SqlDbType.VarChar, 50);
            cmd.Parameters["@id"].Value = openid;

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            // 1. 日记标题
            cmd.Parameters.Add("@age", SqlDbType.VarChar, 10);
            cmd.Parameters["@age"].Value = age;
            // 2.日记详情
            cmd.Parameters.Add("@phone", SqlDbType.VarChar, 1000);
            cmd.Parameters["@phone"].Value = phone;

            cmd.Parameters.Add("@school", SqlDbType.VarChar, 1000);
            cmd.Parameters["@school"].Value = school;

            cmd.Parameters.Add("@snumber", SqlDbType.VarChar, 1000);
            cmd.Parameters["@snumber"].Value = snumber;

            cmd.Parameters.Add("@parentnumber", SqlDbType.VarChar, 1000);
            cmd.Parameters["@parentnumber"].Value = parentnumber;

            cmd.Parameters.Add("@qq", SqlDbType.VarChar, 1000);
            cmd.Parameters["@qq"].Value = qq;

            cmd.Parameters.Add("@wechat", SqlDbType.VarChar, 1000);
            cmd.Parameters["@wechat"].Value = wechat;

            cmd.Parameters.Add("@jointime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@jointime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@avatar", SqlDbType.VarChar, 1000);
            cmd.Parameters["@avatar"].Value = avatar;

            cmd.Parameters.Add("@gender", SqlDbType.VarChar, 1000);
            cmd.Parameters["@gender"].Value = gender;

            cmd.Parameters.Add("@status", SqlDbType.VarChar, 5);
            cmd.Parameters["@status"].Value = 1;

            cmd.Parameters.Add("@mark", SqlDbType.VarChar, 1000);
            cmd.Parameters["@mark"].Value = mark;

            cmd.Parameters.Add("@infomation", SqlDbType.VarChar, 1000);
            cmd.Parameters["@infomation"].Value = infomation;

            cmd.Parameters.Add("@unionid", SqlDbType.VarChar, 200);
            cmd.Parameters["@unionid"].Value = unionid;

            cmd.Parameters.Add("@openid", SqlDbType.VarChar, 200);
            cmd.Parameters["@openid"].Value = openid;


            cmd.Parameters.Add("@classid", SqlDbType.VarChar, 200);
            cmd.Parameters["@classid"].Value = classid;

            cmd.Parameters.Add("@authStatus", SqlDbType.Int);
            cmd.Parameters["@authStatus"].Value = 0;

            cmd.Parameters.Add("@birthday", SqlDbType.VarChar, 200);
            cmd.Parameters["@birthday"].Value = birthday;
            

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();

            // 1. 修改用户类型为学生
            updateUserType();

            Student_Model studentModel = new Student_Model();
            studentModel.avatar = avatar;
            studentModel.classid = classid;
            studentModel.gender = gender;
            studentModel.mark = mark;
            studentModel.name = name;
            studentModel.openid = openid;
            studentModel.parentnumber = parentnumber;
            studentModel.phone = phone;
            studentModel.school = school;
            studentModel.birthday = birthday;
            studentModel.authStatus = "0";

            successManager(studentModel);
        }
        #endregion

        #region 修改用户类型
        private void updateUserType()
        {
            Login_Action login_Action = new Login_Action();
            // 1. 修改用户类型
            login_Action.updateUserInfoWithopenId(openid, "user_type", "2");
            // 2. 修改当前用户的认证状态
            login_Action.updateUserInfoWithopenId(openid, "has_renzhen", "0");

        }
        #endregion

        #region 成功方法
        public void successManager(Student_Model student)
        {
            OUTStudent_Model out_base_setting = new OUTStudent_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = student;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}