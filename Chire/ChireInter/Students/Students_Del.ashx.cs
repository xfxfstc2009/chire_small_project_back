﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Students
{
    /// <summary>
    /// Students_Del 的摘要说明
    /// </summary>
    public class Students_Del : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            searchAll();
        }
        #endregion

        #region 查询学生表
        public void searchAll()
        {
            Student_Action studentAction = new Student_Action();
            studentAction.studentDeleteWithId(id);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTStudent_Model out_base_setting = new OUTStudent_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}