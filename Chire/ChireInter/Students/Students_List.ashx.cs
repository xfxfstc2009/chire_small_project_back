﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Students
{
    /// <summary>
    /// Students_List 的摘要说明
    /// </summary>
    public class Students_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string status = "1";
        string classid = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("status"))
            {
                status = contextWithBase.Request.Form["status"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classid"))
            {
                classid = contextWithBase.Request.Form["classid"];
            }
            
            getAllInfo();
        }
        #endregion

        #region 获取学生
        public void getAllInfo()
        {
            Student_Action studentAction = new Student_Action();
            if (classid.Length > 0)
            {
                List<Student_Model> studentList = studentAction.getAllStudent(status, classid);
                successManager(studentList);
            }
            else {
                List<Student_Model> studentList = studentAction.getAllStudent(status, "");
                successManager(studentList);
            }
           
        }
        #endregion

        #region 执行方法
        public void successManager(List<Student_Model> model)
        {
            OUTStudent_List_Model out_base_setting = new OUTStudent_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Student_List_Model listModel = new Student_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}