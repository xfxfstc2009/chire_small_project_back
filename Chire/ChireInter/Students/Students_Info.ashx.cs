﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Students
{
    /// <summary>
    /// Students_Info 的摘要说明
    /// </summary>
    public class Students_Info : IHttpHandler
    {

        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            getStudentInfo();
        }
        #endregion

        #region 查询学生表
        public void getStudentInfo()
        {
            Student_Action studentAction = new Student_Action();
            Student_Model studentModel = studentAction.getStudentInfoWithId(id);
            successManager(studentModel);
        }
        #endregion

        #region 成功方法
        public void successManager(Student_Model studentModel)
        {
            OUTStudent_Model out_base_setting = new OUTStudent_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = studentModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion

    }
}

















