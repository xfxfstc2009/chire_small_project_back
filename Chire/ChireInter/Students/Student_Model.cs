﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Students
{




    public class Student_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string age { get; set; }
        public string gender { get; set; }
        public string avatar { get; set; }
        public string phone { get; set; }
        public string school { get; set; }
        public string snumber { get; set; }
        public string parentnumber { get; set; }
        public string qq { get; set; }
        public string wechat { get; set; }
        public string jointime { get; set; }
        public string mark { get; set; }
        public string infomation { get; set; }
        public string openid { get; set; }
        public string unionid { get; set; }
        public string classid { get; set; }
        public string status { get; set; }
        public string authStatus { get; set; }
        public string birthday { get; set; }
        public string price_menu { get; set; }

    }

    public class Student_List_Model
    {
        public List<Student_Model> list;
    }

    public class OUTStudent_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Student_List_Model data { get; set; }
    }


    public class OUTStudent_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Student_Model data { get; set; }
    }
}