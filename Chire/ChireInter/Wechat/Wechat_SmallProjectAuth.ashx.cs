﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Wechat.Model;
using Chire.wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Wechat
{
    /// <summary>
    /// Wechat_SmallProjectAuth 的摘要说明
    /// </summary>
    public class Wechat_SmallProjectAuth : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "";
        public void ProcessRequest(HttpContext context)
        { 
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }

            Wechat_AccessToken wechatAccessToken = new Wechat_AccessToken();
            Wechat_SmallProject_Model mainInfo = wechatAccessToken.getcode2SessionManager(code);
            successManager(mainInfo); 
        }
        #endregion

        #region 成功方法
        public void successManager(Wechat_SmallProject_Model desc)
        {
            OUTWechat_SmallProject_Model out_base_setting = new OUTWechat_SmallProject_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
           out_base_setting.data = desc;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
     
    }
}