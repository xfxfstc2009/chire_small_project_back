﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.wechat;

namespace Chire.ChireInter.Wechat
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Wechat_AccessToken token = new Wechat_AccessToken();
            token.wechatMenuAdd();
        
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}