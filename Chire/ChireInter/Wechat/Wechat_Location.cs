﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Wechat
{
    public class Wechat_Location
    {
        public string school { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
    }
}