﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Wechat.Model;
using Newtonsoft.Json;

namespace Chire.ChireInter.Wechat
{
    /// <summary>
    /// Wechat_Location_List 的摘要说明
    /// </summary>
    public class Wechat_Location_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            getAllInfo();
        }
        #endregion

        #region 获取所有学生列表
        public void getAllInfo()
        {
            Wechat_Action wechatAction = new Wechat_Action();
            List<Wechat_Location_Model> wechatLocationList = wechatAction.locationGetStudentList();
            successManager(wechatLocationList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Wechat_Location_Model> wechatLocationList)
        {
            OUTWechat_Location_List_Model out_base_setting = new OUTWechat_Location_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Wechat_Location_List_Model listModel = new Wechat_Location_List_Model();
            listModel.list = wechatLocationList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}