﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Chire.wechat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Wechat
{
    /// <summary>
    /// wechat_auth 的摘要说明
    /// </summary>
    public class wechat_auth : IHttpHandler
    {
    
        HttpContext contextWithBase;
        string code = "";
        string access_token = "";
        string open_id = "";
        string info = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            if (code.Length > 0)
            {
                getWechatInfo();
            }

        }
        #endregion

        #region 微信进行获取内容
        public void getWechatInfo() {
            Wechat_AccessToken token = new Wechat_AccessToken();
            // 1. 获取access_token
            string access_tokenJson = token.getAccessTokenWithCode(code);
            // 2. 获取openid
            JObject jo = (JObject)JsonConvert.DeserializeObject(access_tokenJson);
            open_id = jo["openid"].ToString();

            access_token = jo["access_token"].ToString();

            info = token.getUserInfoWithTokenOpenId(access_token, open_id);
            searchAll();
        }
        #endregion




        #region 查询学生表
        public void searchAll()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into wechat_power(code,info) values(@code,@info)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            cmd.Parameters.Add("@code", SqlDbType.VarChar, 500);
            cmd.Parameters["@code"].Value = code;

            // 1. 作者id
            cmd.Parameters.Add("@info", SqlDbType.VarChar, 5000);
            cmd.Parameters["@info"].Value = info;
     
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTStudent_Model out_base_setting = new OUTStudent_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}