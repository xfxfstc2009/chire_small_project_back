﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Wechat.Model
{
    #region 微信地理位置列表
    public class Wechat_Location_Model
    {
        public string id { get; set; }
        public string openid { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public Student_Model student { get; set; }
    }
    public class Wechat_Location_List_Model
    {
        public List<Wechat_Location_Model> list;
    }
    public class OUTWechat_Location_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Wechat_Location_List_Model data { get; set; }
    }
    #endregion

    #region 微信小程序
    public class Wechat_SmallProject_Model
    {
        public string session_key { get; set; }
        public string openid { get; set; }
        public string info { get; set; }
      
    }

    public class OUTWechat_SmallProject_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Wechat_SmallProject_Model data { get; set; }
    }
    #endregion

}