﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Weather
{
    public class Weather_Model
    {
        public string city { get; set; }
        public string weather { get; set; }
        public string weatherType { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string temperature { get; set; }

    }

    public class OUTWeather_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Weather_Model data { get; set; }

    }
}