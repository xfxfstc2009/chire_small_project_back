﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Qualifications
{
    public class Qualifications_Action
    {
        #region 添加资质
        public void addQualifications(string name, string img) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into qualifications(name,img) values(@name,@img)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;

            cmd.Parameters.Add("@img", SqlDbType.VarChar, 1000);
            cmd.Parameters["@img"].Value = img;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 删除资质
        public void deleteQualifications(string id) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "delete from qualifications where id = '" + id + "'";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 获取资质列表
        public List<Qualifications_Model> getQualificationsListManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from qualifications ";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Qualifications_Model> qualificationList = new List<Qualifications_Model>();
            for (int i = 0; i < rows; i++)
            {
                Qualifications_Model model = new Qualifications_Model();
                model.id = dt.Rows[i]["id"].ToString();
                model.name = dt.Rows[i]["name"].ToString();
                string imgStr = dt.Rows[i]["img"].ToString();
                List<string> imgList = new List<string>();
                imgList.Add(imgStr);
                model.img = imgList;
                qualificationList.Add(model);
            }
            sqlcon.Close();
            return qualificationList;
        }

        #endregion
    }
}