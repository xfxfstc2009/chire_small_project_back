﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Qualifications
{
    /// <summary>
    /// Qualifications_Add 的摘要说明
    /// </summary>
    public class Qualifications_Add : IHttpHandler
    {
        HttpContext contextWithBase;

        string name = "";             // 课程名字
        string img = "";              // 课程价格
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }
        #region 验证信息
        public void analysisInfo()
        {


            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("img"))
            {
                img = contextWithBase.Request.Form["img"];
            }

            insertInfoManager();
        }
        #endregion

        public void insertInfoManager()
        {
            Qualifications_Action menuAction = new Qualifications_Action();
            menuAction.addQualifications(name, img);
            successManager();
        }


        #region 成功方法
        public void successManager()
        {
            Out_Qualifications_List_Model out_base_setting = new Out_Qualifications_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}