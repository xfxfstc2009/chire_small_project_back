﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Qualifications
{
    public class Qualifications_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<string> img { get; set; }
    }

    public class Qualifications_List_Model
    {
        public List<Qualifications_Model> list;
    }

    public class Out_Qualifications_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Qualifications_List_Model data { get; set; }
    }
}