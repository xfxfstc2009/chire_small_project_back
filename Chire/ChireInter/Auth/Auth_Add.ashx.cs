﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Auth
{
    /// <summary>
    /// Auth_Add 的摘要说明
    /// </summary>
    public class Auth_Add : IHttpHandler
    {
        HttpContext contextWithBase;

        string userOpenId = "";
        string type = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("userOpenId"))
            {
                userOpenId = contextWithBase.Request.Form["userOpenId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }
            getAuthListWithType();
        }
        #endregion

        #region 获取进行认证
        public void getAuthListWithType()
        {
            Auth_Action authAction = new Auth_Action();
            if (type.Equals("student") == true)
            {
                authAction.authStudentWithOpenId(userOpenId);
                authAction.authUserWithOpenId(userOpenId);
            }
            else if (type.Equals("teacher")==true){
                authAction.authTeacherWithOpenId(userOpenId);
                authAction.authUserWithOpenId(userOpenId);
            }
            successManager();
        }

        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTAuth_Model out_base_setting = new OUTAuth_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}