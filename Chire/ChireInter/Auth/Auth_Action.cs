﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Chire.ChireInter.Teacher;

namespace Chire.ChireInter.Auth
{
    public class Auth_Action
    {
        // 1. 获取未认证的学生
        public List<Student_Model> getAuthListManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from student where authStatus = 0";


            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Student_Model> music_ablumList = new List<Student_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string age = dt.Rows[i]["age"].ToString();
                string gender = dt.Rows[i]["gender"].ToString();

                string avatar = dt.Rows[i]["avatar"].ToString();
                string phone = dt.Rows[i]["phone"].ToString();
                string school = dt.Rows[i]["school"].ToString();
                string snumber = dt.Rows[i]["snumber"].ToString();
                string parentnumber = dt.Rows[i]["parentnumber"].ToString();
                string qq = dt.Rows[i]["qq"].ToString();
                string wechat = dt.Rows[i]["wechat"].ToString();
                string jointime = dt.Rows[i]["jointime"].ToString();
                Student_Model downloadModel = new Student_Model()
                {
                    id = id,
                    name = name,
                    age = age,
                    gender = gender,
                    avatar = avatar,
                    phone = phone,
                    school = school,
                    snumber = snumber,
                    parentnumber = parentnumber,
                    qq = qq,
                    wechat = wechat,
                    jointime = jointime
                };
                music_ablumList.Add(downloadModel);
            }
            sqlcon.Close();
            return music_ablumList;
        }

        // 2. 获取未认证的老师
        public List<Teacher_Model> getNoAuthTeacherListManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from teacher where authStatus = '0'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;


            List<Teacher_Model> teacherList = new List<Teacher_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string avatar = dt.Rows[i]["avatar"].ToString();
                string gender = dt.Rows[i]["gender"].ToString();
                string banner = dt.Rows[i]["banner"].ToString();
                string phone = dt.Rows[i]["phone"].ToString();
                string school = dt.Rows[i]["school"].ToString();
                string diploma = dt.Rows[i]["diploma"].ToString();
                string remark = dt.Rows[i]["remark"].ToString();

                int statusEnumInt = Convert.ToInt32(dt.Rows[i]["authStatus"].ToString());
                Chire.ChireInter.Login.Login_Action.User_Status status = (Chire.ChireInter.Login.Login_Action.User_Status)statusEnumInt;
                Teacher_Model downloadModel = new Teacher_Model()
                {
                    id = id,
                    name = name,
                    gender = gender,
                    phone = phone,
                    avatar = avatar,
                    banner = banner,
                    school = school,
                    diploma = diploma,
                    remark = remark,
                    status = status,
                };
                teacherList.Add(downloadModel);
            }
            sqlcon.Close();
            return teacherList;
        }

        // 3. 进行老师认证
        public void authTeacherWithOpenId(string openid) {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update teacher set authStatus = 1  where id ='" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }

        // 4. 进行学生认证
        public void authStudentWithOpenId(string openid) {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update student set authStatus = 1  where openid ='" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }

        // 5. 修改用户表
        public void authUserWithOpenId(string openid)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update user_list set has_renzhen = '1'  where userId ='" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }

        // 6.进行家长认证
        public void authParentWithOpenId(string openid) {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update user_list set user_type = '3', has_renzhen = '1' where userId ='" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
    }
}