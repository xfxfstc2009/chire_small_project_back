﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Chire.ChireInter.Teacher;
using Newtonsoft.Json;

namespace Chire.ChireInter.Auth
{
    /// <summary>
    /// Auth_List 的摘要说明
    /// </summary>
    public class Auth_List : IHttpHandler
    {
        HttpContext contextWithBase;


         string type = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("type"))
            {
                type = contextWithBase.Request.Form["type"];
            }
            getAuthListWithType(type);
        }
        #endregion

    #region 获取认证列表
        public void getAuthListWithType(string type){
            Auth_Action authActon = new Auth_Action();

            // 1. 获取当前未认证的学生
            List<Student_Model> studentList = authActon.getAuthListManager();
            // 2. 获取当前未认证的老师
            List<Teacher_Model> teacherList = authActon.getNoAuthTeacherListManager();

            // 3. 抛出
            Auth_Model authModel = new Auth_Model();
            authModel.studentList = studentList;
            authModel.teacherList = teacherList;
            successManager(authModel);
        }
		 
	#endregion

        #region 成功方法
        public void successManager(Auth_Model authModel)
        {
            OUTAuth_Model out_base_setting = new OUTAuth_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = authModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}