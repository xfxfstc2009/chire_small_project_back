﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Chire.ChireInter.Teacher;

namespace Chire.ChireInter.Auth
{
    public class Auth_Model
    {
        public List<Student_Model> studentList{get;set;}
        public List<Teacher_Model> teacherList{ get; set;}
    }

    public class OUTAuth_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Auth_Model data { get; set; }
    }

}