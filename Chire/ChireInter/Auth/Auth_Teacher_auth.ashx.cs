﻿using Chire.ChireInter.Activity;
using Chire.ChireInter.Login;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Auth
{
    /// <summary>
    /// Auth_Teacher_auth 的摘要说明
    /// </summary>
    public class Auth_Teacher_auth : IHttpHandler
    {
        HttpContext contextWithBase;

        string user_id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("user_id"))
            {
                user_id = contextWithBase.Request.Form["user_id"];
            }

          
            changeTeacherStatus();
        }
        #endregion


        #region 修改老师状态
        public void changeTeacherStatus() { 
            // 1. 修改老师用户状态
            updateTeacherInfo(user_id);

            // 2.修改老师审核状态
            updateTeacherShenheStatusInfo(user_id);
            successManager();
        }
        #endregion



        #region 修改老师信息
        public void updateTeacherInfo(string teacherId)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update user_list set user_type = '" + (int)User_Type.User_TypeTeacher + "'  where userId ='" + teacherId + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 修改老师审核状态信息
        public void updateTeacherShenheStatusInfo(string teacherId)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update teacher set status = '" + (int)Chire.ChireInter.Login.Login_Action.User_Status.user_status_normal + "'  where id ='" + teacherId + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion


        #region 成功方法
        public void successManager()
        {
            OUTActivity_List_Model out_base_setting = new OUTActivity_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}