﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.PriceMenu;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Recharge
{
    public class Recharge_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string user_id { get; set; }
        public string priceMenu_id { get; set; }
        public string money { get; set; }
        public string create_time { get; set; }
        public PriceMenu_Model priceMenu { get; set; }
        public Student_Model student { get; set; }
    }

    public class OUT_Recharge_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Recharge_Model data { get; set; }
    }




    public class Recharge_List_Model
    {
        public List<Recharge_Model> list { get; set; }
        public int count { get; set; }
    }

    public class OUT_Recharge_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Recharge_List_Model data { get; set; }
    }
}