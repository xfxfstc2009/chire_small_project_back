﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Login;
using Chire.ChireInter.PriceMenu;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Recharge
{
    public class Recharge_Action
    {
        #region 添加充值信息
        public void addRechargeManager(string openid, string studentId, string priceMenuId, int money)
        {
            // 1. 判断是否是管理员
            Login_Action loginAction = new Login_Action();
            bool hasAdmin = loginAction.hasAdminManager(openid);

            // 2. 授权是管理员
            //if (hasAdmin == true)
            //{
                // 连接数据库
                SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
                sqlcon.Open();
                string StrInsert = "";

                StrInsert = "insert into recharge_history(name,user_id,priceMenu_id,money,create_time) values(@name,@user_id,@priceMenu_id,@money,@create_time)";
                SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
                // 添加参数并且设置参数值
                // 1. 作者id
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                cmd.Parameters["@name"].Value = "学费充值";

                cmd.Parameters.Add("@user_id", SqlDbType.VarChar, 50);
                cmd.Parameters["@user_id"].Value = studentId;

                cmd.Parameters.Add("@priceMenu_id", SqlDbType.VarChar, 50);
                cmd.Parameters["@priceMenu_id"].Value = priceMenuId;

                cmd.Parameters.Add("@money", SqlDbType.Int);
                cmd.Parameters["@money"].Value = money;

                cmd.Parameters.Add("@create_time", SqlDbType.VarChar, 50);
                cmd.Parameters["@create_time"].Value = DateTime.Now.ToString();

                // 执行插入数据的操作
                cmd.ExecuteNonQuery();
                sqlcon.Close();


            // 2. 账户添加 钱
                addMoneyInAccount(studentId, money);
            //}
        }

        #endregion

        #region 充值到账户信息
        public void addMoneyInAccount(string openid,  int money)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update balance set balance += " + money + "  where open_id ='" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 扣费账户信息
        public void cutMoneyInAccount(string openid, int money)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update balance set balance -= " + money + "  where open_id ='" + openid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 获取充值列表
        public Recharge_List_Model getRechargeListAndCountManager(int page, int size, string studentid)
        {
            Recharge_List_Model listModel = new Recharge_List_Model();
            List<Recharge_Model> list = getRechargeListManager(page, size, studentid);
            listModel.list = list;

            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select count(*) as rootcount from recharge_history where user_id = '" + studentid + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();


            if (dr.Read())
            {
                listModel.count = Convert.ToInt32(dr["rootcount"].ToString());
            }
            else {
                listModel.count = 0;
            }
            sqlcon.Close();
            return listModel;
        }


        public List<Recharge_Model> getRechargeListManager(int page, int size, string studentid)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            int pageWithIndex = ((page - 1) * size) + 1;
            int maxPageIndex = pageWithIndex + size - 1;

            string strselect = "";
            if (studentid.Length > 0)
            {
                strselect = "select a.* from(select row_number() over (order by id desc) as rn,* from recharge_history) a where user_id ='" + studentid + "' and rn between '" + pageWithIndex + "' and '" + maxPageIndex + "'";
            }
            else {
               strselect = "select a.* from(select row_number() over (order by id desc) as rn,* from recharge_history) a where rn between '" + pageWithIndex + "' and '" + maxPageIndex + "'";
            }
               

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;
    
            // 获取菜单信息
            PriceMenu_Action priceMenuAction = new PriceMenu_Action();
            // 获取学生信息
            Student_Action studentAction = new Student_Action();

            List<Recharge_Model> rechargeList = new List<Recharge_Model>();
            for (int i = 0; i < rows; i++)
            {

                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string user_id = dt.Rows[i]["user_id"].ToString();
                string priceMenu_id = dt.Rows[i]["priceMenu_id"].ToString();
                string money = dt.Rows[i]["money"].ToString();
                string create_time = dt.Rows[i]["create_time"].ToString();
                PriceMenu_Model priceMenuModel = priceMenuAction.getPriceMenuWithId(priceMenu_id);
                Student_Model studentModel = studentAction.getStudentInfoWithId(user_id);
                Recharge_Model newsModel = new Recharge_Model()
                {
                    id = id,
                    name = name,
                    user_id = user_id,
                    priceMenu_id = priceMenu_id,
                    money = money,
                    create_time = create_time,
                    priceMenu = priceMenuModel,
                    student = studentModel,

                };
                rechargeList.Add(newsModel);
            }
            sqlcon.Close();
            return rechargeList;
        }
        #endregion

        #region 获取充值详情
        public Recharge_Model getRechargeDetalManager(string id)
        {
            Recharge_Model rechargeModel = new Recharge_Model();

            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from recharge_history where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();


            if (dr.Read())
            {
                // 获取菜单信息
                PriceMenu_Action priceMenuAction = new PriceMenu_Action();
                // 获取学生信息
                Student_Action studentAction = new Student_Action();

                rechargeModel.id = dr["id"].ToString();
                rechargeModel.name = dr["name"].ToString();
                rechargeModel.user_id = dr["user_id"].ToString();
                rechargeModel.priceMenu_id = dr["priceMenu_id"].ToString();
                rechargeModel.money = dr["money"].ToString();
                rechargeModel.create_time = dr["create_time"].ToString();
                PriceMenu_Model priceMenuModel = priceMenuAction.getPriceMenuWithId(rechargeModel.priceMenu_id);
                Student_Model studentModel = studentAction.getStudentInfoWithId(rechargeModel.user_id);
                rechargeModel.priceMenu = priceMenuModel;
                rechargeModel.student = studentModel;
            }
            sqlcon.Close();
            return rechargeModel;
        }
        
        #endregion

    }
}