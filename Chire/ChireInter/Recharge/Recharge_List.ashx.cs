﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Recharge
{
    /// <summary>
    /// Recharge_List 的摘要说明
    /// </summary>
    public class Recharge_List : IHttpHandler
    {
        HttpContext contextWithBase;
        int page = 1;                  // 页码
        int size = 20;                 // 一页多少条
        string studentid = "";         // 学生id
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("page"))
            {
                page = Convert.ToInt32(contextWithBase.Request.Form["page"]);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("size"))
            {
                size = Convert.ToInt32(contextWithBase.Request.Form["size"]);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("studentid"))
            {
                studentid = contextWithBase.Request.Form["studentid"];
            }
       
            mainManager();
        }
        #endregion

        public void mainManager()
        {
            Recharge_Action rechargeAction = new Recharge_Action();
            Recharge_List_Model rechargeList = rechargeAction.getRechargeListAndCountManager(page, size, studentid);
            successManager(rechargeList);

        }

        #region 成功方法
        public void successManager(Recharge_List_Model list)
        {
            OUT_Recharge_List_Model out_base_setting = new OUT_Recharge_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = list;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}