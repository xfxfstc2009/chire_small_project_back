﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Recharge
{
    /// <summary>
    /// Recharge_Detail 的摘要说明
    /// </summary>
    public class Recharge_Detail : IHttpHandler
    {
        string id = "";
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
           
            mainManager();
        }
        #endregion

        public void mainManager() {
            Recharge_Action rechargeAction = new Recharge_Action();
            Recharge_Model rechargeModel = rechargeAction.getRechargeDetalManager(id);
            successManager(rechargeModel);

        }
        #region 成功方法
        public void successManager(Recharge_Model rechargeModel)
        {
            OUT_Recharge_Model out_base_setting = new OUT_Recharge_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = rechargeModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}