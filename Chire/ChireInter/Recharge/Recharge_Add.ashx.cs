﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Newtonsoft.Json;

namespace Chire.ChireInter.Recharge
{
    /// <summary>
    /// 充值方法
    /// </summary>
    public class Recharge_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string openid = "";                   // 用户id
        string studentId = "";                // 学生编号
        string priceMenuId = "";              // 充值的钱
        int money = 0;                    // 课时标准

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("studentId"))
            {
                studentId = contextWithBase.Request.Form["studentId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("priceMenuId"))
            {
                priceMenuId = contextWithBase.Request.Form["priceMenuId"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("money"))
            {
                money = Convert.ToInt32(contextWithBase.Request.Form["money"]);
            }

    
            mainManager();
        }
        #endregion

        public void mainManager() {
            Recharge_Action rechargeAction = new Recharge_Action();
            rechargeAction.addRechargeManager(openid, studentId, priceMenuId, money);
            // 修改学生信息里面的套餐列表
            Student_Action studentAction = new Student_Action();
            studentAction.changeStudentPriceMenu(studentId, priceMenuId);

            successManager();
        }

        #region 成功方法
        public void successManager()
        {
            OUT_Recharge_Model out_base_setting = new OUT_Recharge_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        
        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}