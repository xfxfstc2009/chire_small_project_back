﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Fanyi
{


    /// <summary>
    /// Fanyi_JP_Manager 的摘要说明
    /// </summary>
    public class Fanyi_JP_Manager : IHttpHandler
    {
        HttpContext contextWithBase;
        string infotxt = "炽热";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("infotxt"))
            {
                infotxt = contextWithBase.Request.Form["infotxt"];
            }

            fanyi();
        }
        #endregion

        public void fanyi()
        {
            string q = infotxt;
            string newQ = utf8Manager(q);
            string from = "auto";
            string to = "jp";
            string appid = "20181128000240752";
            string salt = getRandomStr(10);
            string key = "eY9KIQU900pNw6LUQa_p";

            string t1 = q;
            string smartSign = appid + t1 + salt + key;

            string sign2 = Md5Hash(smartSign);
            var request = (HttpWebRequest)WebRequest.Create("http://api.fanyi.baidu.com/api/trans/vip/translate");

            var postData = "q=" + newQ;
            postData += "&from=" + from;
            postData += "&to=" + to;
            postData += "&appid=" + appid;
            postData += "&salt=" + salt;
            postData += "&sign=" + sign2;



            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JObject jo = (JObject)JsonConvert.DeserializeObject(responseString);
            string openid = jo["trans_result"].ToString();
            JArray array = (JArray)jo["trans_result"];

            int i = array.Count;

            string aa = "";

            foreach (var jObject in array)
            {

                //赋值属性

                aa = jObject["dst"].ToString();//获取字符串中id值

            }





            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), aa);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());

        }

        #region 1.Get请求

        public static string HttpGet(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.UTF8);
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }

        #endregion

        #region 转换utf8
        public string utf8Manager(string str)
        {
            string info1 = str.Trim();
            string sma = System.Web.HttpUtility.UrlEncode(info1, Encoding.UTF8);
            return sma;
        }

        #endregion

        #region 进行生成复杂字符串
        public string getRandomStr(int n)//b：是否有复杂字符，n：生成的字符串长度
        {
            string str = "0123456789";

            StringBuilder SB = new StringBuilder();
            Random rd = new Random();
            for (int i = 0; i < n; i++)
            {
                SB.Append(str.Substring(rd.Next(0, str.Length), 1));
            }
            return SB.ToString();
        }
        #endregion

        #region MD5
        public static string MD5Encrypt(string strText)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(strText));
            return System.Text.Encoding.Default.GetString(result);
        }

        private static string Md5Hash(string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytValue, bytHash;
            bytValue = System.Text.Encoding.UTF8.GetBytes(input);
            bytHash = md5.ComputeHash(bytValue);
            md5.Clear();
            string sTemp = "";
            for (int i = 0; i < bytHash.Length; i++)
            {
                sTemp += bytHash[i].ToString("X").PadLeft(2, '0');
            }
            return sTemp.ToLower();
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}