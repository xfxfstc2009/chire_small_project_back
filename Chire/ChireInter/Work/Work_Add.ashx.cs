﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Work
{
    /// <summary>
    /// Work_Add 的摘要说明
    /// </summary>
    public class Work_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string workInfo = "";           // 传入的作业列表
        string pushType = "";           // 传入的类型，1是班级，2学生
        string pushUser = "";           // 推送的id信息,
        string hasPush = "";            // 是否推送
        string end_time = "";           // 什么时候完成作业


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
         
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("workInfo"))
            {
                workInfo = contextWithBase.Request.Form["workInfo"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("pushType"))
            {
                pushType = contextWithBase.Request.Form["pushType"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("pushUser"))
            {
                pushUser = contextWithBase.Request.Form["pushUser"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("hasPush"))
            {
                hasPush = contextWithBase.Request.Form["hasPush"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("end_time"))
            {
                end_time = contextWithBase.Request.Form["end_time"];
            }
            addWorkInfo();
        }
        #endregion

        #region 添加作业
        public void addWorkInfo()
        {
            Work_Action workAction = new Work_Action();

            // 1. 把作业信息进行拆分
            string[] workStringArr = workInfo.Split(',');
            List<string> workList = workStringArr.ToList<string>();

            // 2.把推送人员进行拆分
            string[] userArr = pushUser.Split(',');
            List<string> userList = userArr.ToList<string>();


            workAction.addWork(workList, Convert.ToInt32(pushType), userList, Convert.ToInt32(hasPush), end_time);
            successManager();
        }
        #endregion

        #region 执行方法
        public void successManager()
        {
            OutWork_Model out_base_setting = new OutWork_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}