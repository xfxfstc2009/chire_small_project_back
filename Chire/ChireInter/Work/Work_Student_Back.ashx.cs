﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Exam;
using Newtonsoft.Json;

namespace Chire.ChireInter.Work
{
    /// <summary>
    /// Work_Student_Back 的摘要说明
    /// </summary>
    public class Work_Student_Back : IHttpHandler
    {

        HttpContext contextWithBase;
        string workid = "";
        string studentid = "";
        string html = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("workid"))
            {
                workid = contextWithBase.Request.Form["workid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("studentid"))
            {
                studentid = contextWithBase.Request.Form["studentid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("html"))
            {
                html = contextWithBase.Request.Form["html"];
                html = HttpUtility.UrlDecode(html);
            }

            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update work_student set student_back = '" + html + "' where student_id = '" + studentid + "' and linkId = '" + workid + "'";


            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTExam_File_List_Model out_base_setting = new OUTExam_File_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
 
    }
}