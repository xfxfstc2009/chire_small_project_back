﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Work
{
    public class Work_Model
    {
        public string id { get; set; }
        public string workName { get; set; }
        public string hasPush { get; set; }
        public string pushType { get; set; }
        public string pushUser { get; set; }
        public string date { get; set; }
        public string linkId { get; set; }
        public List<string> workList { get; set; }
        public string end_time { get; set; }
        public Work_Student_Model workback { get; set; }
    }

    public class Work_List_Model
    {
        public List<Work_Model> list { get; set; }
        public string count { get; set; }
    }

    public class OutWork_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Work_List_Model data { get; set; }
    }

    public class OutWork_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Work_Model data { get; set; }
    }


    public class Work_Student_Model
    {
        public string id { get; set; }
        public string student_id { get; set; }
        public string linkId { get; set; }
        public string datetime { get; set; }
        public Student_Model student { get; set; }
        public string beizhu { get; set; }
        public string has_push { get; set; }
        public string student_back { get; set; }
    }

    public class Work_Student_List_Model
    {
        public List<Work_Student_Model> list { get; set; }
       

    }
    public class OutWork_Student_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Work_Student_List_Model data { get; set; }
    }
}