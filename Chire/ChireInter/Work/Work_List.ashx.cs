﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Work
{
    /// <summary>
    /// Work_List 的摘要说明
    /// </summary>
    public class Work_List : IHttpHandler
    {
        HttpContext contextWithBase;
        int invalid = 0;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {

            if (contextWithBase.Request.Form.AllKeys.Contains("invalid"))
            {
                invalid = Convert.ToInt32(contextWithBase.Request.Form["invalid"]);
            }
            getWorkList();
        }
        #endregion

        #region 获取作业列表
        public void getWorkList() { 
            Work_Action workAction = new Work_Action();
            Work_List_Model workList = workAction.getWorkList(invalid);
            successManager(workList);
        }
        #endregion

        #region 执行方法
        public void successManager(Work_List_Model workList)
        {
            OutWork_List_Model out_base_setting = new OutWork_List_Model();
          
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = workList;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}