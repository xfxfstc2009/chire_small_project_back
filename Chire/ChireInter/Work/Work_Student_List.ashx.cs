﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Identity;
using Chire.ChireInter.Students;
using Chire.ChireInter.Wechat;
using Newtonsoft.Json;
using Chire.ChireInter.Parent;

namespace Chire.ChireInter.Work
{
    /// <summary>
    /// Work_Student_List 的摘要说明
    /// </summary>
    public class Work_Student_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "";
        string openid = "";
       
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            getMainManager();
        }
        #endregion

        #region mainManager
        public void getMainManager() {
            if (openid.Length > 0)
            {

            }
            else {
                Wechat_Action wechatAction = new Wechat_Action();
                openid = wechatAction.getOpenIdWithCode(code);
            }

            Student_Model studentModel;


            // 1. 判断是学生还是家长
            Student_Action studentAction = new Student_Action();
            Student_Model tempStudentModel = studentAction.getStudentIdWithOpenId(openid);
            if (tempStudentModel.id != null)
            {        // 表示是学生
                studentModel = tempStudentModel;
            }
            else
            {
                Parent_Action parentAction = new Parent_Action();
                Student_Model parentStudentModel = parentAction.getStudentIdWithBindingParentId(openid);
                studentModel = parentStudentModel;
            }


            // 3. 根据学生信息获取我的作业
            Work_Action workAction = new Work_Action();
            List<Work_Model> workList = workAction.getWorkListWithStudentIdManager(studentModel.id,true);

            successManager(workList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Work_Model> workList)
        {
            OutWork_List_Model out_base_setting = new OutWork_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Work_List_Model workListModel = new Work_List_Model();
            workListModel.list = workList;
            out_base_setting.data = workListModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}