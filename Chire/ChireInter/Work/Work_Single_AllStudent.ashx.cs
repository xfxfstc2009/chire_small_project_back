﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Newtonsoft.Json;

namespace Chire.ChireInter.Work
{
    /// <summary>
    /// Work_Single_AllStudent 的摘要说明
    /// </summary>
    public class Work_Single_AllStudent : IHttpHandler
    {
        HttpContext contextWithBase;
        string workid = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("workid"))
            {
                workid = contextWithBase.Request.Form["workid"];
            }

            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from work_student where linkId = '"+workid+"'";

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Work_Student_Model> workList = new List<Work_Student_Model>();
            Student_Action studentAction = new Student_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();
                string beizhu = dt.Rows[i]["beizhu"].ToString();
                string has_push = dt.Rows[i]["has_push"].ToString();
                string student_back = dt.Rows[i]["student_back"].ToString();

                Student_Model student = studentAction.getStudentInfoWithId(student_id);

                Work_Student_Model workModel = new Work_Student_Model()
                {
                    id = id,
                    student_id = student_id,
                    datetime = datetime,
                    beizhu = beizhu,
                    has_push = has_push,
                    student = student,
                    student_back = student_back,
                
                };
                workList.Add(workModel);
            }
            sqlcon.Close();

            successManager(workList);

        }
        #endregion

        #region 成功方法
        public void successManager(List<Work_Student_Model> studentList)
        {
            OutWork_Student_List_Model out_base_setting = new OutWork_Student_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Work_Student_List_Model lstModel = new Work_Student_List_Model();
            lstModel.list = studentList;
            out_base_setting.data = lstModel;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}