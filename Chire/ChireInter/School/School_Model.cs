﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.School
{
    public class School_Model
    {
        public string id { get; set; }
        public string school { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string map_img { get; set; }
    }

    public class School_List_Model
    {
        public List<School_Model> list { get; set; }
    }

    public class OUTSchool_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public School_List_Model data { get; set; }
    }
}