﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.School
{
    /// <summary>
    /// School_List 的摘要说明
    /// </summary>
    public class School_List : IHttpHandler
    {
        HttpContext contextWithBase;
       
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

       #region 验证信息
        public void analysisInfo()
        {
           mainManager();
        }
        #endregion

        #region 主要操作
        public void mainManager(){
            School_Action schoolAction = new School_Action();
            List<School_Model>schoolList = schoolAction.getAllSchool();
            scuccessedManager(schoolList);
        }

    	#endregion

         #region 成功方法
		 public void scuccessedManager(List<School_Model> schoolList){
            OUTSchool_List_Model out_base_setting = new OUTSchool_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "success";

            School_List_Model schoolListModel = new School_List_Model();
            schoolListModel.list = schoolList;
            out_base_setting.data = schoolListModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
         }
	    #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

      
    }
}