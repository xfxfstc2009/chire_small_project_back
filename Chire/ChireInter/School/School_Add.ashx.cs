﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.School
{
    /// <summary>
    /// School_Add 的摘要说明
    /// </summary>
    public class School_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string school = "";
        string latlon = "";
        string lat = "";
        string lon = "";
        string map_img = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }
        
        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("school"))
            {
                school = contextWithBase.Request.Form["school"];
            }
             if (contextWithBase.Request.Form.AllKeys.Contains("latlon"))
            {
                latlon = contextWithBase.Request.Form["latlon"];
            }
             if (contextWithBase.Request.Form.AllKeys.Contains("lat"))
             {
                 lat = contextWithBase.Request.Form["lat"];
             }
             if (contextWithBase.Request.Form.AllKeys.Contains("lon"))
             {
                 lon = contextWithBase.Request.Form["lon"];
             }
             if (contextWithBase.Request.Form.AllKeys.Contains("map_img"))
             {
                 map_img = contextWithBase.Request.Form["map_img"];
             }
            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager() {
            // 1. 根据,截取
            if (latlon.Length > 0) {
                string[] latArr = latlon.Split(',');
                lat = latArr[0];
                lon = latArr[1];
            }
            

            School_Action schoolAction = new School_Action();
            schoolAction.insertSchool(school, lat, lon,map_img);
            scuccessedManager();
        }
        #endregion

        #region 成功方法
        public void scuccessedManager()
        {
            OUTSchool_List_Model out_base_setting = new OUTSchool_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "success";

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}