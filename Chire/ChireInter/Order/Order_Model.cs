﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.PriceMenu;
using Chire.ChireInter.Students;
using Chire.ChireInter.Subject;

namespace Chire.ChireInter.Order
{
    public class Order_Student_Model { 

         public string id { get; set; }
         public string name { get; set; }
         public string student_id { get; set; }
         public Student_Model student { get; set; }
         public string class_id { get; set; }
         public Class_Model classModel { get; set; }
         public string order_type { get; set; }
         public string money { get; set; }
         public string price_menu_id { get; set; }
         public PriceMenu_Model price_menu { get; set; }
         public string hours { get; set; }
         public string date_time { get; set; }
         public string timeorderid { get; set; }
         public Order_Time_Model timeorder { get; set; }

    }

    public class Order_Student_List_Model
    {

        public List<Order_Student_Model> list { get; set; }
        public int count { get; set; }
      
    }

    public class Out_Order_Student_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Order_Student_List_Model data { get; set; }
    }

    // 课时订单
    public class Order_Time_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string student_id { get; set; }
        public Student_Model student { get; set; }
        public string class_id { get; set; } 
        public Class_Model classModel { get; set; }
        public string order_type { get; set; }
        public double order_time { get; set; }
        public string detail { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string status { get; set; }
        public string timeorder_id { get; set; }

        // temp
        public string money { get; set; }

    }

    public class Order_Time_List_Model
    {
        public List<Order_Time_Model> willStartList { get; set; }               // 将要开始
        public List<Order_Time_Model> endList { get; set; }                     // 已结束
        public List<Order_Time_Model> list { get; set; }                        // 所有的
    }

    public class Out_Order_Time_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Order_Time_List_Model data { get; set; }
    }
}