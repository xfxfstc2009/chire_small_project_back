﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Order
{
    /// <summary>
    /// Order_List 的摘要说明
    /// </summary>
    public class Order_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            getAllInfo();
        }
        #endregion

        #region 获取列表
        public void getAllInfo() {
            Order_Action orderAction = new Order_Action();
            List<Order_Time_Model> orderList = orderAction.getAllOrderManager();
            successManager(orderList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Order_Time_Model> model)
        {
            Out_Order_Time_List_Model out_base_setting = new Out_Order_Time_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Order_Time_List_Model listModel = new Order_Time_List_Model();
            listModel.list = model;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

      
    }
}