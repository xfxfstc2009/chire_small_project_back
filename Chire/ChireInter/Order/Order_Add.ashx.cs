﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Order
{
    /// <summary>
    /// Order_Add 的摘要说明
    /// </summary>
    public class Order_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "";                   // 订单名字
        string student_id = "";             // 学生id 
        string order_type = "";             // 订单类型     
        string money = "";                  // 消费价格
        string price_menu = "";             // 课程菜单
        string hours = "";                  // 授课时间
        string timeorder_id = "";           // 学生签到签退id、
        string class_id = "";               // 课程id



        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("student_id"))
            {
                student_id = contextWithBase.Request.Form["student_id"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("order_type"))
            {
                order_type = contextWithBase.Request.Form["order_type"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("money"))
            {
                money = contextWithBase.Request.Form["money"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("price_menu"))
            {
                price_menu = contextWithBase.Request.Form["price_menu"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("hours"))
            {
                hours = contextWithBase.Request.Form["hours"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("timeorder_id"))
            {
                timeorder_id = contextWithBase.Request.Form["timeorder_id"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("class_id"))
            {
                class_id = contextWithBase.Request.Form["class_id"];
            }
            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager() {
            Order_Action orderAction = new Order_Action();
            orderAction.addOrderManager(student_id, class_id); 
        }
        #endregion



        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}