﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Students;
using Chire.ChireInter.Wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Order
{
    /// <summary>
    /// Order_Single_List 的摘要说明
    /// </summary>
    public class Order_Single_List : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "";
        string openid = "";                 // 微信获取到的微信id
        string page = "";
        string size = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("openid"))
            {
                openid = contextWithBase.Request.Form["openid"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("page"))
            {
                page = contextWithBase.Request.Form["page"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("size"))
            {
                size = contextWithBase.Request.Form["size"];
            }

            mainManager();
        }
        #endregion

        #region 主方法
        public void mainManager() {
            // 获取1.openid
            Wechat_Action wechatAction = new Wechat_Action();
            string openId = "";
            if (code.Length > 0)
            {
                openId = wechatAction.getOpenIdWithCode(code);
            }
            else {
                openId = openid;
            }

     
            // 3. 根据学生id 获取列表
            Order_Action orderAction = new Order_Action();
            Order_Student_List_Model orderListModel = orderAction.getStudentOrderListManager(openid);

            successManager(orderListModel);
        }
        #endregion

        #region 成功方法
        public void successManager(Order_Student_List_Model orderList)
        {
            Out_Order_Student_List_Model out_base_setting = new Out_Order_Student_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";


            out_base_setting.data = orderList;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion

    }
}