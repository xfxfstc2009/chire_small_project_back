﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.PriceMenu;
using Chire.ChireInter.Recharge;
using Chire.ChireInter.Students;
using Chire.ChireInter.Subject;
using Chire.wechat;

namespace Chire.ChireInter.Order
{
    public class Order_Action
    {
        #region 即将开课，生成开课记录
        public void order_willStartGetList(string classid)
        {
            Class_Action classAction = new Class_Action();
            Class_Model classModel = classAction.getClassInfoWithId(classid);
            // 1. 获取班内所有的学生
            Student_Action studentAction = new Student_Action();
            List<Student_Model> studentList = studentAction.getAllStudent("1", classid);
            // 2. 给所有学生插入开课记录
            for (int i = 0; i < studentList.Count; i++)
            {
                Student_Model studentModel = studentList[i];
                Order_Time_Model model = new Order_Time_Model();

                model.name = "【待上】" + classModel.name;
                model.student_id = studentModel.id;
                model.class_id = classid;
                model.order_type = "支出";
                model.order_time = 0;
                model.detail = "";
                model.date = DateTime.Now.ToShortDateString();
                model.time = DateTime.Now.ToString();
                model.status = "即将开课";

                addClassInfo(model);
            }
        }
        #endregion

        // 2. 获取当前周几

        #region 已经签退，计算课时
        public Order_Time_Model signOutCalculationClassTime(string classid, string student_id)
        {
            // 1. 获取课程信息
            Class_Action classAction = new Class_Action();
            Class_Model classModel = classAction.getClassInfoWithId(classid);
            // 2. 获取今天的课时
            string weekDay = getCurrentDay(DateTime.Now);
            Class_Time_Model currentTimeModel = new Class_Time_Model();
            for (int i = 0; i < classModel.classtimelist.Count; i++) {
                Class_Time_Model classTimeModel = classModel.classtimelist[i];
                if (classTimeModel.week.Equals(weekDay) == true) {
                    currentTimeModel = classTimeModel;
                    break;
                }
            }




            // 2. 获取这个学生
            Student_Action studentAction = new Student_Action();

            Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);

            Order_Time_Model model = new Order_Time_Model();
            model.name = "【已结算】" + classModel.name;
            model.student_id = studentModel.id;
            model.class_id = classid;
            model.order_type = "支出";

            DateTime dt = Convert.ToDateTime(currentTimeModel.begintime);          // 开课时间
            int beginHour = dt.Hour;
            int beginMin = dt.Minute;
            double beginMinF = beginMin * 1.0 / 60;
            double beginMinTime = beginHour + beginMinF;

            DateTime dt1 = Convert.ToDateTime(currentTimeModel.endtime);          // 结束时间
            int endHour = dt1.Hour;
            int endMin = dt1.Minute;
            double endMinF = endMin * 1.0 / 60;
            double endMinTime = endHour + endMinF;

            model.order_time = (endMinTime - beginMinTime);
            model.detail = "开课时间为：" + currentTimeModel.begintime + "签退时间为：" + DateTime.Now.ToString();
            model.date = DateTime.Now.ToShortDateString();
            model.time = DateTime.Now.ToString();
            model.status = "已签退";
            // 判断是否有这么开课记录，如果有就进行修改，如果没有就插入
            Order_Time_Model mainModel = getOrderTimeInfo(studentModel.id, classid, DateTime.Now.ToShortDateString());
            model.student = studentModel;
            model.classModel = classModel;

            if (mainModel.timeorder_id != null) {
                model.timeorder_id = mainModel.timeorder_id;
                updateCurrentOrderInfo(model);
            }
      
            else
            {
                addClassInfo(model);
            }

            return model;
        }
        #endregion

        #region 插入课程信息
        public Order_Time_Model addClassInfo(Order_Time_Model model)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into time_order(name,student_id,class_id,order_type,order_time,detail,date,time,status,timeorder_id) values (@name,@student_id,@class_id,@order_type,@order_time,@detail,@date,@time,@status,@timeorder_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = model.name;
            // 1. 日记标题
            cmd.Parameters.Add("@student_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@student_id"].Value = model.student_id;
            // 2.日记详情
            cmd.Parameters.Add("@class_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@class_id"].Value = model.class_id;

            cmd.Parameters.Add("@order_type", SqlDbType.VarChar, 50);
            cmd.Parameters["@order_type"].Value = model.order_type;

            cmd.Parameters.Add("@order_time", SqlDbType.VarChar, 50);
            cmd.Parameters["@order_time"].Value = model.order_time;

            cmd.Parameters.Add("@detail", SqlDbType.VarChar, 1000);
            cmd.Parameters["@detail"].Value = model.detail;

            cmd.Parameters.Add("@date", SqlDbType.VarChar, 1000);
            cmd.Parameters["@date"].Value = model.date;

            cmd.Parameters.Add("@time", SqlDbType.VarChar, 1000);
            cmd.Parameters["@time"].Value = model.time;

            cmd.Parameters.Add("@status", SqlDbType.VarChar, 1000);
            cmd.Parameters["@status"].Value = model.status;

            cmd.Parameters.Add("@timeorder_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@timeorder_id"].Value = getRandomStr(10);

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();

            return model;
        }
        #endregion

        #region 查询是否有当前信息
        public Order_Time_Model getOrderTimeInfo(string studentId,string classId,string date)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from time_order where date = '" + date + "' and student_id = '" + studentId + "' and class_id = '" + classId + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Order_Time_Model mainModel = new Order_Time_Model();

            if (dr.Read())
            {
                mainModel.id = dr["id"].ToString();
                mainModel.name = dr["name"].ToString();
                mainModel.student_id = dr["student_id"].ToString();
                Student_Action studentAction = new Student_Action();
                Student_Model studentModel = studentAction.getStudentInfoWithId(dr["student_id"].ToString());
                mainModel.student = studentModel;
                mainModel.class_id = dr["class_id"].ToString();
                Class_Action classAction = new Class_Action();
                Class_Model classModel = classAction.getClassInfoWithId(dr["class_id"].ToString());
                mainModel.classModel = classModel;
                mainModel.order_type = dr["order_type"].ToString();
                mainModel.order_time = Convert.ToDouble(dr["order_time"].ToString());

                mainModel.detail = dr["detail"].ToString();
                mainModel.date = dr["date"].ToString();
                mainModel.time = dr["time"].ToString();
                mainModel.status = dr["status"].ToString();
                mainModel.timeorder_id = dr["timeorder_id"].ToString();

            }
            sqlcon.Close();

            return mainModel;
        }
        #endregion

        #region 查询是否有当前信息
        public Order_Time_Model getOrderTimeInfo(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from time_order where timeorder_id = '"+id+"'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Order_Time_Model mainModel = new Order_Time_Model();

            if (dr.Read())
            {
                mainModel.id = dr["id"].ToString();
                mainModel.name = dr["name"].ToString();
                mainModel.student_id = dr["student_id"].ToString();
                Student_Action studentAction = new Student_Action();
                Student_Model studentModel = studentAction.getStudentInfoWithId(dr["student_id"].ToString());
                mainModel.student = studentModel;
                mainModel.class_id = dr["class_id"].ToString();
                Class_Action classAction = new Class_Action();
                Class_Model classModel = classAction.getClassInfoWithId(dr["class_id"].ToString());
                mainModel.classModel = classModel;
                mainModel.order_type = dr["order_type"].ToString();
                mainModel.order_time = Convert.ToDouble(dr["order_time"].ToString());

                mainModel.detail = dr["detail"].ToString();
                mainModel.date = dr["date"].ToString();
                mainModel.time = dr["time"].ToString();
                mainModel.status = dr["status"].ToString();
                mainModel.timeorder_id = dr["timeorder_id"].ToString();

            }
            sqlcon.Close();

            return mainModel;
        }
        #endregion

        #region 修改当前信息-已签退
        public Order_Time_Model updateCurrentOrderInfo(Order_Time_Model model)
        {
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "";
            strSqls = "update time_order set name = '" + model.name + "', status = '" + model.status + "', order_time = '" + model.order_time + "',detail='" + model.detail + "'  where date = '" + model.date + "' and student_id = '" + model.student_id + "' and class_id = '" + model.class_id + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();

            return model;
        }

        #endregion





        #region 获取学生的消费记录
        public Order_Student_List_Model getStudentOrderListManager(string openId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from order_list where student_id = '" + openId + "' order by id desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Order_Student_Model> orderList = new List<Order_Student_Model>();
            Student_Action studentAction = new Student_Action();
            Subject_Action subjectAction = new Subject_Action();
            Class_Action classAction = new Class_Action();
            PriceMenu_Action priceMenuAction = new PriceMenu_Action();

            for (int i = 0; i < rows; i++)
            {

                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();

                string student_id = dt.Rows[i]["student_id"].ToString();
                Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);

                string class_id = dt.Rows[i]["class_id"].ToString();
                Class_Model classModel = classAction.getClassInfoWithId(class_id);
                string order_type = dt.Rows[i]["order_type"].ToString();
                string money = dt.Rows[i]["money"].ToString();
                string price_menu = dt.Rows[i]["price_menu"].ToString();
                PriceMenu_Model priceModel = priceMenuAction.getPriceMenuWithId(price_menu);
                string hours = dt.Rows[i]["hours"].ToString();
                string date_time = dt.Rows[i]["date_time"].ToString();
                string timeorderid = dt.Rows[i]["timeorder_id"].ToString();
                Order_Time_Model timeOrder = getOrderTimeInfo(timeorderid);

                Order_Student_Model orderModel = new Order_Student_Model()
                {
                    id = id,
                    name = name,
                    student_id = student_id,
                    student = studentModel,
                    class_id = class_id,
                    classModel = classModel,

                    order_type = order_type,
                    money=money,
                    price_menu_id = price_menu,
                    price_menu = priceModel,
                    hours = hours,
                    date_time = date_time,
                    timeorderid = timeorderid,
                    timeorder = timeOrder
                };
                orderList.Add(orderModel);
            }
            sqlcon.Close();

            Order_Student_List_Model listModel = new Order_Student_List_Model();
            listModel.list = orderList;
            listModel.count = rows;
            return listModel;
        }
        #endregion

        #region 添加消费订单
        public void addOrderManager(string student_id, string class_id)
        {
            // 1.订单表里面进行添加信息 time_order
            Order_Time_Model timeOrderModel = signOutCalculationClassTime(class_id, student_id);
            // 2. 个人的金币进行扣费
            PriceMenu_Action priceMenuAction = new PriceMenu_Action();
            PriceMenu_Model priceMenuModel = priceMenuAction.getPriceMenuWithId(timeOrderModel.student.price_menu);
            double zifei = Convert.ToDouble(priceMenuModel.price) * timeOrderModel.order_time;
            int zifeiInt = Convert.ToInt32(zifei);
            timeOrderModel.money = zifeiInt.ToString();
            // 2. 进行插入当前的资费表
            addSubOrderManager(timeOrderModel);
            // 4. 修改个人的余额
            Recharge_Action rechargeAction = new Recharge_Action();
            rechargeAction.cutMoneyInAccount(student_id,zifeiInt); 
        }

        #region 插入资费order、
        public void addSubOrderManager(Order_Time_Model timeOrder)
        {
            string name = timeOrder.name;
            string studentId = timeOrder.student_id;
            string order_type = "2";                // 一进二出
            string money = timeOrder.money;
            string price_menu = timeOrder.student.price_menu;                 // 报的班级
            string hours = timeOrder.order_time.ToString();
            string timeorder_id = timeOrder.timeorder_id;
            string classId = timeOrder.class_id;

            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into order_list(name,student_id,order_type,money,price_menu,hours,date_time,timeorder_id,class_id) values (@name,@student_id,@order_type,@money,@price_menu,@hours,@date_time,@timeorder_id,@class_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = timeOrder.name;
            // 1. 日记标题
            cmd.Parameters.Add("@student_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@student_id"].Value = studentId;
            // 2.日记详情
            cmd.Parameters.Add("@order_type", SqlDbType.VarChar, 50);
            cmd.Parameters["@order_type"].Value = order_type;

            cmd.Parameters.Add("@money", SqlDbType.VarChar, 50);
            cmd.Parameters["@money"].Value = money;

            cmd.Parameters.Add("@price_menu", SqlDbType.VarChar, 50);
            cmd.Parameters["@price_menu"].Value = price_menu;

            cmd.Parameters.Add("@hours", SqlDbType.VarChar, 1000);
            cmd.Parameters["@hours"].Value = hours;

            cmd.Parameters.Add("@date_time", SqlDbType.VarChar, 1000);
            cmd.Parameters["@date_time"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@timeorder_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@timeorder_id"].Value = timeorder_id == null ? "" : timeorder_id;

            cmd.Parameters.Add("@class_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@class_id"].Value = classId;

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #endregion





        #region 进行生成复杂字符串
        public string getRandomStr(int n)//b：是否有复杂字符，n：生成的字符串长度
        {
            string str = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

            StringBuilder SB = new StringBuilder();
            Random rd = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0; i < n; i++)
            {
                SB.Append(str.Substring(rd.Next(0, str.Length), 1));
            }
            return SB.ToString();
        }


        #endregion

        #region 获取当前所有的订单@暂废弃
        public List<Order_Time_Model> getAllOrderManager()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from time_order order by id desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Order_Time_Model> orderList = new List<Order_Time_Model>();
            Student_Action studentAction = new Student_Action();
            Class_Action classAction = new Class_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();

                Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);
                string class_id = dt.Rows[i]["class_id"].ToString();
                Class_Model classModel = classAction.getClassInfoWithId(class_id);

                string order_type = dt.Rows[i]["order_type"].ToString();
                double order_time = Convert.ToDouble(dt.Rows[i]["order_time"].ToString());
                string detail = dt.Rows[i]["detail"].ToString();
                string date = dt.Rows[i]["date"].ToString();
                string time = dt.Rows[i]["time"].ToString();
                string status = dt.Rows[i]["status"].ToString();
                Order_Time_Model orderModel = new Order_Time_Model()
                {
                    id = id,
                    name = name,
                    student = studentModel,
                    classModel = classModel,
                    order_type = order_type,
                    order_time = order_time,
                    detail = detail,
                    date = date,
                    time = time,
                    status = status
                };
                orderList.Add(orderModel);
            }
            sqlcon.Close();
            return orderList;
        }
        #endregion

        #region 3.获取今天星期几
        public string getCurrentDay(DateTime time)
        {
            string[] Day = new string[] { "0", "1", "2", "3", "4", "5", "6" };
            string week = Day[Convert.ToInt32(time.DayOfWeek.ToString("d"))].ToString();
            return week;
        }
        #endregion

        #region 获取当前用户所有的订单 @暂废弃
        public List<Order_Time_Model> getAllOrderManagerWithUserId(string studentId, string requestStatus)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from   where student_id = '" + studentId + "' and status = '" + requestStatus + "' order by time desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Order_Time_Model> orderList = new List<Order_Time_Model>();
            Student_Action studentAction = new Student_Action();
            Class_Action classAction = new Class_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();
                Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);
                string class_id = dt.Rows[i]["class_id"].ToString();
                Class_Model classModel = classAction.getClassInfoWithId(class_id);

                string order_type = dt.Rows[i]["order_type"].ToString();
                double order_time = Convert.ToDouble(dt.Rows[i]["order_time"].ToString());
                string detail = dt.Rows[i]["detail"].ToString();
                string date = dt.Rows[i]["date"].ToString();
                string time = dt.Rows[i]["time"].ToString();
                string status = dt.Rows[i]["status"].ToString();
                Order_Time_Model orderModel = new Order_Time_Model()
                {
                    id = id,
                    name = name,
                    student = studentModel,
                    classModel = classModel,
                    order_type = order_type,
                    order_time = order_time,
                    detail = detail,
                    date = date,
                    time = time,
                    status = status
                };
                orderList.Add(orderModel);
            }
            sqlcon.Close();
            return orderList;
        }
        #endregion


    }
}