﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Parent
{
    public class Parent_Model
    {
        public string id { get; set;}
        public string name {get;set;}
        public string phone {get;set;}
        public string binding {get;set;}
        public string datetime {get;set;}
        public string openid {get;set;}
    }

    public class Parent_List_Model
    {
        public List<Parent_Model> list { get; set; }
      
    }

    public class Out_Parent_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Parent_List_Model data { get; set; }
    }
}