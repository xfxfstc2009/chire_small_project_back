﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Subject
{
    public class Subject_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string img { get; set; }
        public long createTime { get; set; }
    }

    public class OUTSubject_Model {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Subject_Model data { get; set; }
    }

    public class Subject_List_Model
    {
        public List<Subject_Model> list;
    }

    public class OUTSubject_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Subject_List_Model data { get; set; }
    }
}