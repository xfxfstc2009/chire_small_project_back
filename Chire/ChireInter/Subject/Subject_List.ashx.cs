﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Subject
{
    /// <summary>
    /// Subject_List 的摘要说明
    /// </summary>
    public class Subject_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
      
            getAllInfo();
        }
        #endregion

        #region 获取所有科目
        public void getAllInfo()
        {
            Subject_Action subjectAction = new Subject_Action();
            List<Subject_Model> subjectList = subjectAction.getAllSubject();
            successManager(subjectList);

        }
        #endregion

        #region 执行方法
        public void successManager(List<Subject_Model> subjectList)
        {
            OUTSubject_List_Model out_base_setting = new OUTSubject_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Subject_List_Model listModel = new Subject_List_Model();
            listModel.list = subjectList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion


        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
      
    }
}