﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.wechat;
using Newtonsoft.Json;

namespace Chire.ChireInter.Class
{
    /// <summary>
    /// Class_Add 的摘要说明
    /// </summary>
    public class Class_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        string name = "";            // 课程名字
        string teacher = "";         // 老师
        string betime = "";          // 开始与结束时间
        string begintime = "";
        string endtime = "";         // 结束时间
        string classtime = "";       // 课时
        string marks = "";           // 备注
        string status = "";          // 状态
        string uploads = "";         // 上传图片
        string week = "";       
        string ablum = "";           // 封面
        string subject = "";         // 科目
        string weekstime = "";       // 课时信息

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            id = Constance.Instance.getRandomStr(10);

            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("teacher"))
            {
                teacher = contextWithBase.Request.Form["teacher"];
            }
               
            if (contextWithBase.Request.Form.AllKeys.Contains("betime"))
            {
                betime = contextWithBase.Request.Form["betime"];

                string[] temp = betime.Split('-');
                begintime = temp[0];
                endtime = temp[1];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("classtime"))
            {
                classtime = contextWithBase.Request.Form["classtime"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("marks"))
            {
                marks = contextWithBase.Request.Form["marks"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("status"))
            {
                status = contextWithBase.Request.Form["status"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("upload"))
            {
                uploads = contextWithBase.Request.Form["upload"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("week"))
            {
                week = contextWithBase.Request.Form["week"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("ablum"))
            {
                ablum = contextWithBase.Request.Form["ablum"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("weekstime"))
            {
                weekstime = contextWithBase.Request.Form["weekstime"];
            }

            if (contextWithBase.Request.Form.AllKeys.Contains("subject"))
            {
                subject = contextWithBase.Request.Form["subject"];
            }
      
    


            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into class(id,name,teacher,begintime,endtime,classtime,marks,status,outclasstime,deletestatus,uploads,datetime,week,ablum,subject) values(@id,@name,@teacher,@begintime,@endtime,@classtime,@marks,@status,@outclasstime,@deletestatus,@uploads,@datetime,@week,@ablum,@subject)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            
            cmd.Parameters.Add("@id", SqlDbType.VarChar, 50);
            cmd.Parameters["@id"].Value = id;

            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            
            cmd.Parameters.Add("@teacher", SqlDbType.VarChar, 100);
            cmd.Parameters["@teacher"].Value = teacher;

            cmd.Parameters.Add("@begintime", SqlDbType.VarChar, 10);
            cmd.Parameters["@begintime"].Value = begintime;

            cmd.Parameters.Add("@endtime", SqlDbType.VarChar, 10);
            cmd.Parameters["@endtime"].Value = endtime;

            cmd.Parameters.Add("@classtime", SqlDbType.Int);
            cmd.Parameters["@classtime"].Value = classtime;
            
            cmd.Parameters.Add("@marks", SqlDbType.VarChar, 5000);
            cmd.Parameters["@marks"].Value = marks;

            cmd.Parameters.Add("@uploads", SqlDbType.VarChar, 5000);
            cmd.Parameters["@uploads"].Value = uploads;
            
            cmd.Parameters.Add("@outclasstime", SqlDbType.Int);
            cmd.Parameters["@outclasstime"].Value = 0;
            
            cmd.Parameters.Add("@deletestatus", SqlDbType.Int);
            cmd.Parameters["@deletestatus"].Value = 1;

            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 30);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@status", SqlDbType.VarChar, 10);
            cmd.Parameters["@status"].Value = 1;

            cmd.Parameters.Add("@week", SqlDbType.VarChar, 30);
            cmd.Parameters["@week"].Value = week;

            cmd.Parameters.Add("@ablum", SqlDbType.VarChar, 300);
            cmd.Parameters["@ablum"].Value = ablum;

            cmd.Parameters.Add("@subject", SqlDbType.VarChar, 300);
            cmd.Parameters["@subject"].Value = subject;
            

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();

            classTimeManager();
            successManager();
        }
        #endregion

        #region 插入课程时间信息
        private void classTimeManager() {
            // 1. 
            string[] tempweek = weekstime.Split(',');
            for (int i = 0; i < tempweek.Length; i++)
            {
                string singleInfo = tempweek[i];
                string[] weekAndTime = singleInfo.Split(' ');
                string week = weekAndTime[0];
                string time = weekAndTime[1];
                string[] seTime = time.Split('-');
                string starttime = seTime[0];
                string  endtime = seTime[1];
                insertClassTimeInfo(week, starttime, endtime);
            }
        }

        private void insertClassTimeInfo(string week ,string strarttime,string endtime) {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";

            StrInsert = "insert into class_time(week,begintime,endtime,create_time,linkId) values(@week,@begintime,@endtime,@create_time,@linkId)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值

            string weekStr = "";
            if (week.Equals("星期一")==true){
                weekStr = "1";
            } else if (week.Equals("星期二") == true){
                weekStr = "2";
            }
            else if (week.Equals("星期三") == true)
            {
                weekStr = "3";
            }
            else if (week.Equals("星期四") == true)
            {
                weekStr = "4";
            }
            else if (week.Equals("星期五") == true)
            {
                weekStr = "5";
            }
            else if (week.Equals("星期六") == true)
            {
                weekStr = "6";
            }
            else if (week.Equals("星期日") == true)
            {
                weekStr = "0";
            }


            cmd.Parameters.Add("@week", SqlDbType.VarChar, 50);
            cmd.Parameters["@week"].Value = weekStr;

            cmd.Parameters.Add("@begintime", SqlDbType.VarChar, 10);
            cmd.Parameters["@begintime"].Value = strarttime;

            cmd.Parameters.Add("@endtime", SqlDbType.VarChar, 10);
            cmd.Parameters["@endtime"].Value = endtime;

            cmd.Parameters.Add("@create_time", SqlDbType.VarChar, 30);
            cmd.Parameters["@create_time"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@linkId", SqlDbType.VarChar, 10);
            cmd.Parameters["@linkId"].Value = id;



            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();

        }


        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTClass_List_Model out_base_setting = new OUTClass_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}