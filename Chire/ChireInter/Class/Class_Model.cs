﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Subject;
using Chire.ChireInter.Teacher;

namespace Chire.ChireInter.Class
{
    public class Class_Request_Model {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Class_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public Teacher_Model teacher {get;set;}
        public string marks { get; set; }
        public string status { get; set; }
        public string classtime { get; set; }
        public string outclasstime { get; set; }
        public string uploads { get; set; }
         public string week { get; set; }
         public string begintime { get; set; }
         public string endtime { get; set; }
         public string datetime { get; set; }
         public string deletestatus { get; set; }
         public string ablum { get; set; }
         public Subject_Model subject { get; set; }
         public List<Class_Time_Model> classtimelist { get; set; }


    }

    public class Class_List_Model
    {
        public List<Class_Model> list { get; set; }
    }

    public class OUTClass_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Class_List_Model data { get; set; }
    }

    public class OUTClass_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Class_Model data { get; set; }
    }


    public class Class_Time_Model {
        public string id { get; set; }
        public string week { get; set; }
        public string begintime { get; set; }
        public string endtime { get; set; }
        public string createtime { get; set; }
    }

}