﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Class
{
    /// <summary>
    /// Class_Detail 的摘要说明
    /// </summary>
    public class Class_Detail : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }


        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
          
            getInfoWithClass();
        }
        #endregion

        #region 获取数据
        public void getInfoWithClass()
        {
            Class_Action classAction = new Class_Action();
            Class_Model classModel =  classAction.getClassInfoWithId(id);

            successManager(classModel);
        }
        #endregion

      
        #region 成功方法
        public void successManager(Class_Model classModel)
        {
            OUTClass_Model out_base_setting = new OUTClass_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = classModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}