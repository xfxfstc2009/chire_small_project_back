﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Teacher;
using Newtonsoft.Json;

namespace Chire.ChireInter.Class
{
    /// <summary>
    /// Class_List 的摘要说明
    /// </summary>
    public class Class_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }
        #region 验证信息
        public void analysisInfo()
        {

            getAllInfo();
        }
        #endregion

        #region 获取所有课程信息
        public void getAllInfo()
        {
            Class_Action classAction = new Class_Action();
            List<Class_Model> classList = classAction.classGetAllClass();
            successManager(classList);
        }
        #endregion
    
        #region 执行方法
        public void successManager(List<Class_Model> classList)
        {
            OUTClass_List_Model out_base_setting = new OUTClass_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Class_List_Model listModel = new Class_List_Model();
            listModel.list = classList;
            out_base_setting.data = listModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}