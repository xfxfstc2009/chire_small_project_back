﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Chire.ChireInter.Teacher;
using Chire.ChireInter.Event_calendar;
using Chire.ChireInter.Subject;

namespace Chire.ChireInter.Class
{
    public class Class_Action
    {
        #region 根据id删除课程
        public void classDeleteWithId(string id){
          //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update class set deletestatus = 0  where id = '"+id+"'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 获取所有课程
        public List<Class_Model> classGetAllClass()
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from class where deletestatus = 1 order by id desc";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Class_Model> classList = new List<Class_Model>();
            Teacher_Action teacherAction = new Teacher_Action();
            Subject_Action subjectAction = new Subject_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string teacherId = dt.Rows[i]["teacher"].ToString();
                Teacher_Model teacherModel = teacherAction.getTeacherWithId(teacherId);
                string marks = dt.Rows[i]["marks"].ToString();
                string status = dt.Rows[i]["status"].ToString();
                string classtime = dt.Rows[i]["classtime"].ToString();
                string outclasstime = dt.Rows[i]["outclasstime"].ToString();
                string uploads = dt.Rows[i]["uploads"].ToString();
                string week = dt.Rows[i]["week"].ToString();
                string begintime = dt.Rows[i]["begintime"].ToString();
                string endtime = dt.Rows[i]["endtime"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();
                string deletestatus = dt.Rows[i]["deletestatus"].ToString();
                string ablum = dt.Rows[i]["ablum"].ToString();
                string subjectId = dt.Rows[i]["subject"].ToString();

                Subject_Model subjectModel = null;
                if (subjectId.Length > 0) {
                    subjectModel = subjectAction.getSubjectInfoWithId(subjectId);
                }
                
                // 上课时间
                List<Class_Time_Model> classTimeList = getClassTimesListManager(id);

                Class_Model downloadModel = new Class_Model()
                {
                    id = id,
                    name = name,
                    teacher = teacherModel,
                    marks = marks,
                    status = status,
                    classtime = classtime,
                    outclasstime = outclasstime,
                    uploads = uploads,
                    week = week,
                    begintime = begintime,
                    endtime = endtime,
                    datetime = datetime,
                    deletestatus = deletestatus,
                    ablum = ablum,
                    subject = subjectModel,
                    classtimelist = classTimeList,
                };
                classList.Add(downloadModel);
            }
            sqlcon.Close();
            return classList;
        }
        #endregion

        #region 根据课程id获取课程
        public Class_Model getClassInfoWithId(string id)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select top 1 * from class where id = '" + id + "'";
            SqlCommand sqlcmd = new SqlCommand(strselect, sqlcon);
            SqlDataReader dr = sqlcmd.ExecuteReader();
            Class_Model classModel = new Class_Model();
            Teacher_Action teacherAction = new Teacher_Action();
            Subject_Action subjectAction = new Subject_Action();
            if (dr.Read())
            {
                classModel.id = id;
                classModel.name = dr["name"].ToString();
                string teacherId = dr["teacher"].ToString();
                Teacher_Model teacherModel = teacherAction.getTeacherWithId(teacherId);
                classModel.teacher = teacherModel;
                classModel.marks = dr["marks"].ToString();
                classModel.status = dr["status"].ToString();
                classModel.classtime = dr["classtime"].ToString();
                classModel.outclasstime = dr["outclasstime"].ToString();
                classModel.uploads = dr["uploads"].ToString();
                classModel.week = dr["week"].ToString();
                classModel.begintime = dr["begintime"].ToString();
                classModel.endtime = dr["endtime"].ToString();
                classModel.datetime = dr["datetime"].ToString();
                classModel.deletestatus = dr["deletestatus"].ToString();
                classModel.ablum = dr["ablum"].ToString();
                string subjectId = dr["subject"].ToString();
                Subject_Model subjectModel = subjectAction.getSubjectInfoWithId(subjectId);
                classModel.subject = subjectModel;
                classModel.classtimelist = getClassTimesListManager(id);
            }
            sqlcon.Close();
            return classModel;
        }

        #endregion

        #region 获取今天要上课的课程
        public List<Class_Model> getCurrentDayHasInClass()
        { 
            // 1. 获取今天日期
            string currentDayTime=System.DateTime.Now.ToString("yyyy-MM-dd");

            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from event_calendar where type = " + (int)EventType.EventTypeClass + " and [date] = '" + currentDayTime + "' and has_del = '"+0+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Event_Model> eventList = new List<Event_Model>();
            Class_Action classAction = new Class_Action();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string title = dt.Rows[i]["title"].ToString();
                string dates = dt.Rows[i]["date"].ToString();
                string time = dt.Rows[i]["time"].ToString();
                long dateInterval = (long)Convert.ToInt64(dt.Rows[i]["dateInterval"].ToString());
                long timeInterval = (long)Convert.ToInt64(dt.Rows[i]["timeInterval"].ToString());
                int has_del = Convert.ToInt32(dt.Rows[i]["has_del"].ToString());
                string class_id = dt.Rows[i]["class_id"].ToString();
                Class_Model classModel = classAction.getClassInfoWithId(class_id);
                Event_Model eventModel = new Event_Model() { classModel = classModel, id = id, title = title, date = dates, time = time, dateInterval = dateInterval, timeInterval = timeInterval, has_del = has_del, class_id = class_id };
                eventList.Add(eventModel);
            }
            sqlcon.Close();
            
            // 2. 进行判断事件
            List<Class_Model> classModelList = new List<Class_Model>();
            for (int i = 0; i < eventList.Count; i++) {
                Event_Model eventModel = eventList[i];
                classModelList.Add(eventModel.classModel);
            }

            return classModelList;
        }
        #endregion

        #region 修改课程的图片
        public void classImgUpdateManager(string classid, string imgs) {
            string[] singleImg = imgs.Split(',');
            string ablum = "";
            if (singleImg.Length > 0) {
                ablum = singleImg[0];
            }
            
            //连接数据库
            SqlConnection sqlcon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon1.Open();
            //修改数据信息
            string strSqls = "update class set uploads = '" + imgs + "',ablum = '"+ablum+"'  where id ='" + classid + "'";

            SqlCommand cmd = new SqlCommand(strSqls, sqlcon1);
            //添加参数并且设置参数值
            cmd.ExecuteNonQuery();
            sqlcon1.Close();
        }
        #endregion

        #region 获取开课时间列表
        public List<Class_Time_Model> getClassTimesListManager(string linkId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from class_time where linkId = '"+linkId+"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Class_Time_Model> classTimeModelList = new List<Class_Time_Model>();

            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string week = dt.Rows[i]["week"].ToString();
                string begintime = dt.Rows[i]["begintime"].ToString();
                string endtime = dt.Rows[i]["endtime"].ToString();
                string create_time = dt.Rows[i]["create_time"].ToString();

                Class_Time_Model classtimeSingleModel = new Class_Time_Model()
                {
                    id = id,
                   week = week,
                   begintime = begintime,
                   endtime = endtime,
                   createtime = create_time

                };
                classTimeModelList.Add(classtimeSingleModel);
            }
            sqlcon.Close();
            return classTimeModelList;
        }

        #endregion
    }
}