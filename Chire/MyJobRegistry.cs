﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Task;
using Chire.wechat;
using FluentScheduler;

namespace Chire
{
    public class MyJobRegistry : Registry
    {
        public  MyJobRegistry()
        {
            // 1.每周日晚上5点进行下周的排课
            Schedule<ClassWeekEvaluate>().ToRunEvery(1).Weeks().On(DayOfWeek.Monday).At(17, 00);
            // 2.当天上课的早上7点进行推送今天有课 
            Schedule<EveryDayReminderClassEvaluate>().ToRunEvery(1).Days().At(7, 00);
            // 3. 每天上课前1小时的开课提醒
            Schedule<EveryClassOneHourEvaluate>().ToRunNow().AndEvery(10).Minutes();
            // 4. 每周5晚上进行判断上周是否有没有签退的。
            Schedule<BeforeWeekNoSignOut>().ToRunEvery(5).Weeks().On(DayOfWeek.Monday).At(17, 30);

      
       
        }
    }


    #region 1.每周日晚上5点进行下周的排课
    internal class ClassWeekEvaluate : IJob
    {
        public void Execute()
        {
            Task_ClassWeek_EveryWeek taskManager = new Task_ClassWeek_EveryWeek();
            taskManager.mainManager();
        }
    }
    #endregion

    #region 2.当天上课的早上7点进行推送今天有课
    internal class EveryDayReminderClassEvaluate : IJob
    {
        public void Execute()
        {
            Task_ReminderClass taskManager = new Task_ReminderClass();
            taskManager.mainManager();
        }
    }
    #endregion

    #region 3.每天上课前1小时的开课提醒
    internal class EveryClassOneHourEvaluate : IJob
    {
        public void Execute()
        {
            Task_ClassBefore_Tixing taskManager = new Task_ClassBefore_Tixing();
            taskManager.mainManager();
        }
    }
    #endregion



     #region 4.每天上课了还没有签到的进行提醒
    internal class BeforeWeekNoSignOut : IJob
    {
        public void Execute()
        {
            Task_BeforeNoSignOut taskManager = new Task_BeforeNoSignOut();
            taskManager.mainManager();
        }
    }
     #endregion

     #region 1.每天晚上7天进行当天事件的更新
     internal class EveryEventEvaluate : IJob
     {
         public void Execute()
         {
             //Task_Event_EveryDay taskManager = new Task_Event_EveryDay();
             //taskManager.mainManager();
         }
     }
     #endregion
    internal class UpdateEvaluate : IJob  //此处实现IJob接口
    {
        public void Execute()//这个方法是IJob接口的方法，必须实现。这个方法也是上面所示的</pre> //Schedule<UpdateEvaluate>()...的入口方法<br>  
        { //此处添加自己的代码段 
            

        }
    }
}