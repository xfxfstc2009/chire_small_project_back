﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Chire.wechat
{
    public class Constance
    {
        public const string smallProjectAppId = "wxe181f5967e679b7c";
        public const string smallProjectSecret = "04e8dc9688f86436d09bc2b2b808d375";
        public const string smallProjectGrant_type = "authorization_code";


        public const string AliOSSaccess = "LTAIonSsqNjkQH2K";
        public const string AliOSSaccessSecret = "LBRbirJ1tZ5REmFJfhR7tSrq5QoVsF";
        public const string AliOSSEndPoint = "https://chire.oss-cn-hangzhou.aliyuncs.com";
        public const string AliOSSBucketName = "chire";

        public const string wechatAppID = "wx0efd04edbbba705e";
        public const string wechatAccessSecret = "a31cd815dc718f9c0fa78d99de6b8447";
        public const string wechatToken = "o26jbg6B39zZjT11ZxKzK3TCFKl23Ns1";

        // 高德
        public const string amapKey = "7237698724944d5ad70cbe797b9d0954";

        // 腾讯地图
        public const string tcentMap = "FTRBZ-HFFA4-E4RUI-XOFZE-N36GQ-4VBOV";

        public const string smallProjectId = "wxe181f5967e679b7c";
        public const string smallProjectAccessSecret = "04e8dc9688f86436d09bc2b2b808d375";
        public const string smallProjectToken = "o26jbg6B39zZjT11ZxKzK3TCFKl23Ns1";


        // imgBase
        public const string imgBaseUrl = "https://chire.oss-cn-hangzhou.aliyuncs.com";
        private static Constance _instance;
        public static Constance Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Constance();
                }
                return _instance;
            }
        }


        // 推送信息
        // 1. 微信签到
        public const string pushWechatSignIn = "--ZO8hgyUkPXV5Qmb0DM-bUXimLuiyKiCYeyAxcUnBs";
        // 2. 微信签退
        public const string pushWechatSignOut = "-jjOpt6L7zysoIAcnbZGhV_3GHd_XyP0ehiWA255bSE";
        // 3. 进行签到
        public const string pushWechatMemberJoin = "0kh_Gkm_mKyHBL1u0Xl0tV2pW1zHB6V-DlSf5OSkqHA";
        // 4. 课程开课
        public const string pushWechatClassWillStart = "jgo5wWGNbqFvDnXM-5MfigeE8ouQEPLwns3eoq9ub4w";
        // 4. 课程结束
        public const string pushWechatClassWillEnd = "elG4jUa-1fkfd4nl1x23g1uqx2h1IDVmCEObLsegbd4";
        // 5. 课程结束后 但是没签退
        public const string pushWechatClassEndNotSignOut = "6hLGauGki5KP6HyU7Lpk8RlatKLzrhT0vhoD0whrJDY";
        // 6. 课程已经开课10分钟，但是没签到
        public const string pushWechatClassStartNotSignIn = "LfNx1yZVwr7IzFdjM-XPGi9u72jTotWhb44rGb1WGDU";
        // 7. 作业推送
        public const string pushWorkInfo = "udve09y6n0W_lA5hn1MfVzv63oLDo_k-RPIfa9BN_nw";
        // 8. 课程信息
        public const string classEvent = "OK9oVaVUFqm37Zpx7IZl9e6HkJ7nCJ0vcg46bWvEWvM";
        // 9. 家长绑定成功
        public const string bindingSuccess = "qDGG64fdGY0RsM8Uo1nRku7jlpP2KUwytXeW1wClDL8";



        #region 进行生成复杂字符串
        public string getRandomStr(int n)//b：是否有复杂字符，n：生成的字符串长度
        {
            string str = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

            StringBuilder SB = new StringBuilder();
            Random rd = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0; i < n; i++)
            {
                SB.Append(str.Substring(rd.Next(0, str.Length), 1));
            }
            return SB.ToString();
        }


        #endregion

        #region 时间戳转换
        /// <summary>
        /// 时间戳转换
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public long ConvertDateTimeInt(System.DateTime time)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
            TimeSpan ts = (time - startTime);

            return (long)ts.TotalSeconds;
        }

        public static DateTime ConvertLongToDateTime(long time)
        {
            TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            DateTime datetime = DateTime.MinValue;
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

            datetime = startTime.AddSeconds(time);

            return datetime;
        }

        public static DateTime WeekStartTime
        {
            get
            {
                DateTime dt = DateTime.Now;
                int dayOfWeek = -1 * (int)dt.Date.DayOfWeek;
                //Sunday = 0,Monday = 1,Tuesday = 2,Wednesday = 3,Thursday = 4,Friday = 5,Saturday = 6,

                DateTime weekStartTime = dt.AddDays(dayOfWeek);//取本周一
                if (dayOfWeek == 0) //如果今天是周日，则开始时间是上周一
                {
                    weekStartTime = weekStartTime.AddDays(-7);
                }

                return weekStartTime.Date;
            }
        }

        public static DateTime WeekEndTime
        {
            get
            {
                return WeekStartTime.AddDays(7);
            }
        }
        #endregion


    }
}