﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.wechat
{

    public class Xiaochengxu_Action
    {
        // 小程序
        // 1. 小程序 发推送
        public const string wechat_push_work_add = "YoXlVo7khe_Czjx2lbhCewZQPGx4dYVratveJODOHZU";
        // 家长绑定学生索取号码
        public const string xiaochengxu_push_identity_binding = "r9UmJUJBHJLhYlDqjr41gYsMfT95GSX79O3tlswEF7A";
        // 2.家长成功绑定给学生和家长发送成功消息
        public const string xiaochengxu_push_identity_bindingSuccess = "r9UmJUJBHJLhYlDqjr41gYsMfT95GSX79O3tlswEF7A";
        // 3. 发送推送消息
        public const string xiaochengxu_push_msg = "SaYc5JSVjcAsctAIcmWpVXmuTOD8onfRz5xDEIQVAcQ";
        // 4. 发送早上有课提醒
        public const string xiaochengxu_morning_tixing = "9rO6VxDMjQxEW46sGq8S0Y0TswvVKuJ9w9ZuS-liM_Q";
        // 5. 发送打卡提醒
        public const string xiaochengxu_daka_tixing = "TOe48jXKxVGVZRIu2DuNa3dWU_tyJXoqXE3QFDcezgM";
        // 6. 预计要签到但是没签到进行通知
        public const string xiaochengxu_noSign = "dJo1DsrU3-NYu7ScU_rtEo1_pIaCX-wQ1aEPf1ILvRM";

        #region 小程序
        #region 获取AccessToken
        public string getXiaochengxuAccessToken()
        {
            Wechat_AccessToken wechatAction = new Wechat_AccessToken();
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + Constance.smallProjectId + "&secret=" + Constance.smallProjectAccessSecret;
            string info = wechatAction.HttpGet(url, "");

            JObject jo = (JObject)JsonConvert.DeserializeObject(info);
            string access_token = jo["access_token"].ToString();

            return access_token;
        }
        #endregion

        #region Main小程序进行发送消息
        public string smallProjectSendMsg(string openId, SmallProjectModelDataList list, string pushTemp, string url)
        {
            string accessToken = getXiaochengxuAccessToken();
            string nUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken;

            string template_id = pushTemp;
            string appid = Constance.smallProjectId;

            SmallProjectModel model = new SmallProjectModel();
            model.appid = appid;
            model.data = list;
            model.template_id = template_id;
            model.touser = openId;
            if (url.Length > 0)
            {
                model.url = url;
            }


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), model);
            string data = sw.GetStringBuilder().ToString();

            Wechat_AccessToken wechatAction = new Wechat_AccessToken();
            string callback = wechatAction.HttpPost(nUrl, data);
            return callback;
        }
        #endregion



        #region 1.家长绑定学生，给学生下发验证码
        public string smallproject_identity_binding_sendSmsCode(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, xiaochengxu_push_identity_binding, "");
            return callback;
        }
        #endregion

        #region 2.家长绑定学生 绑定成功后进行告知
        public string smallproject_identity_binding_success(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, xiaochengxu_push_identity_binding, "");
            return callback;
        }
        #endregion

        #region 3.进行消息推送
        public string smallproject_push_msg(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, xiaochengxu_push_msg, "");
            return callback;
        }
        #endregion

        #region 4.早上进行提醒课表
        public string smallproject_morningclass_tixing(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, xiaochengxu_morning_tixing, "");
            return callback;
        }
        #endregion

        #region 5.课前1小时&&结束提醒
             public string smallproject_keqiankehou_tixing(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, xiaochengxu_daka_tixing, "");
            return callback;
        }
        
        #endregion


        #region 4.作业下发
        public string smallproject_work_add(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, wechat_push_work_add, "");
            return callback;
        }
      
        #endregion

        #region 6.忘记签到
        public string smallproject_noSign(string openId, SmallProjectModelDataList list)
        {
            string callback = smallProjectSendMsg(openId, list, xiaochengxu_noSign, "");
            return callback;
        }
        #endregion
        #endregion

    }
}