﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Chire.wechat.Menu
{
    public class Wechat_Menu_Action
    {
        public List<Menu_Model> wechatMenuAddActionManager(string parentId)
        {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "";
            if (parentId.Length > 0)
            {
                strselect = "select * from wechat_menu where parent = '" + parentId + "'";
            }
            else
            {
                strselect = "select * from wechat_menu where parent = '0'";
            }

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Menu_Model> classList = new List<Menu_Model>();

            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string name = dt.Rows[i]["name"].ToString();
                string type = dt.Rows[i]["type"].ToString();
                string info = dt.Rows[i]["info"].ToString();
                string parent = dt.Rows[i]["parent"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();
                string newType = "";

                if (type.Equals("1") == true)
                {
                    newType = "view";
                }
                else if (type.Equals("2") == true)
                {
                    newType = "click";
                }
                else if (type.Equals("3") == true)
                {
                    newType = "miniprogram";
                }
                List<Menu_Model> subList = new List<Menu_Model>();
                if (parentId.Length == 0)
                {
                    subList = wechatMenuAddActionManager(id);
                }

                Menu_Model downloadModel = new Menu_Model()
                {
                    name = name,
                    type = newType,
                    url = info,
                    sub_button = subList
                };
                classList.Add(downloadModel);
            }
            sqlcon.Close();

            return classList;
        }
    }
}