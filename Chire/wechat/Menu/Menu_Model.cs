﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.wechat.Menu
{
    public class Menu_Model
    {
        public List<Menu_Model> sub_button { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string url { get; set; }
        public string media_id { get; set; }
        public string appid { get; set; }
        public string pagepath { get; set; }
    }

    public class Menu_Button_Model {
        public List<Menu_Model> button { get; set; }
    }
}