﻿using Chire.ChireInter.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.wechat
{
    public class WechatModel
    {

        public string touser { get; set; }
        public string template_id { get; set; }
        public string url { get; set; }
        public string appid { get; set; }
        public WechatModelDataList data { get; set; }
    }

    public class WechatModelData
    {
        public string value { get; set; }
        public string color { get; set; }
    }

    public class WechatModelDataList
    {
        public WechatModelData first { get; set; }
        public WechatModelData userName { get; set; }
        public WechatModelData courseName { get; set; }
        public WechatModelData date { get; set; }
        public WechatModelData keyword1 { get; set; }
        public WechatModelData keyword2 { get; set; }
        public WechatModelData keyword3 { get; set; }
        public WechatModelData keyword4 { get; set; }
        public WechatModelData keyword5 { get; set; }
        public WechatModelData remark { get; set; }
    }

    public class WechatNoticeModel
    {
        public string id { get; set; }
        public string info { get; set; }
        public string name { get; set; }
        public string openid { get; set; }
        public string datetime { get; set; }
        public Student_Model student { get; set; }
    }

    public class WechatNoticeListModel
    {
        public List<WechatNoticeModel> list { get; set; }
    }

    public class OUTWechatNoticeListModel
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public WechatNoticeListModel data { get; set; }
    }






    public class SmallProjectModelDataList
    {
        public SmallProjectModelData name1 { get; set; }
        public SmallProjectModelData name6 { get; set; }
        public SmallProjectModelData thing1 { get; set; }
        public SmallProjectModelData thing2 { get; set; }
        public SmallProjectModelData thing3 { get; set; }
        public SmallProjectModelData thing4 { get; set; }
        public SmallProjectModelData thing5 { get; set; }

        public SmallProjectModelData thing6 { get; set; }
        public SmallProjectModelData thing7 { get; set; }
        public SmallProjectModelData thing8 { get; set; }
        public SmallProjectModelData thing9 { get; set; }
        public SmallProjectModelData thing10 { get; set; }
        public SmallProjectModelData date1 { get; set; }
        public SmallProjectModelData date2 { get; set; }
        public SmallProjectModelData date3 { get; set; }
        public SmallProjectModelData date4 { get; set; }

        public SmallProjectModelData time1 { get; set; }
        public SmallProjectModelData time2 { get; set; }
        public SmallProjectModelData time3 { get; set; }

        
    }

    public class SmallProjectModelData
    {
        public string value { get; set; }
    }

    public class SmallProjectModel
    {

        public string touser { get; set; }
        public string template_id { get; set; }
        public string url { get; set; }
        public string appid { get; set; }
        public SmallProjectModelDataList data { get; set; }
    }

}