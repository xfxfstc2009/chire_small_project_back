﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Task;

namespace Chire.wechat
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
            // 1. 每周的事件进行下发
            //Task_EveryEventWeekDay sma = new Task_EveryEventWeekDay();
            //sma.mainManager();

            //// 2. 
            //Task_EveryDay taskManager = new Task_EveryDay();
            //taskManager.mainManager();

            //Wechat_AccessToken aes = new Wechat_AccessToken();
            //aes.sucaiManager();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}