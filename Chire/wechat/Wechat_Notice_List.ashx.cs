﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Xml;
using Chire.ChireInter.Students;

namespace Chire.wechat
{
    /// <summary>
    /// Wechat_Notice_List 的摘要说明
    /// </summary>
    public class Wechat_Notice_List : IHttpHandler
    {
        HttpContext contextWithBase;
        int page = 1;
        int size = 10;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();

        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("page"))
            {
                page = Convert.ToInt32(contextWithBase.Request.Form["page"]);
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("size"))
            {
                size = Convert.ToInt32(contextWithBase.Request.Form["size"]);
            }
            getNoticeList();
        }
        #endregion

        #region 获取当前的列表
        public void getNoticeList() { 
         SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            int pageWithIndex = ((page - 1) * size) + 1;
            int maxPageIndex = pageWithIndex + size - 1;
            string strselect = "select a.* from(select row_number() over (order by id desc) as rn,* from wechatEvent) a where rn between '" + pageWithIndex + "' and '" + maxPageIndex + "'";

            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;
            
            List<WechatNoticeModel> noticeListModel = new List<WechatNoticeModel>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString().Trim();
                string info = dt.Rows[i]["info"].ToString().Trim();
                string name = dt.Rows[i]["name"].ToString().Trim();
                string openid = dt.Rows[i]["openid"].ToString().Trim();
                string datetime = dt.Rows[i]["datetime"].ToString().Trim();
                Student_Model studentModel = jiexiXML(name);
                WechatNoticeModel wechatNoticeModel = new WechatNoticeModel() { 
                    id = id,
                    info = info,
                    openid = openid,
                    datetime = datetime,
                    student = studentModel,

                };
                    noticeListModel.Add(wechatNoticeModel);
            }
            sqlcon.Close();
            successManager(noticeListModel);
        }
        
        
        #endregion

        #region 进行解析
        public Student_Model jiexiXML(string info) {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(info);

            XmlNode rootNode = doc.SelectSingleNode("xml");
            XmlNodeList xn1 = rootNode.ChildNodes;

            // 1. 获取发送人信息
            string FromUserName = xn1.Item(1).InnerText;
                
            // 2.
            Student_Action studentAction = new Student_Action();
            Student_Model studentModle = studentAction.getStudentInfoWithStudentOpenId(FromUserName);

            return studentModle;
        }
        #endregion

        #region 执行方法
        public void successManager(List<WechatNoticeModel> noticeList)
        {
            OUTWechatNoticeListModel out_base_setting = new OUTWechatNoticeListModel();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            WechatNoticeListModel workListModel = new WechatNoticeListModel();
            workListModel.list = noticeList;
            out_base_setting.data = workListModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}